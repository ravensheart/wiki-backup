---
title: Breath of the Dragon Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Breath of the Dragon Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Breath of the Dragon Form
-------------------------

Cost

\-

Type

Form

Requirements

Breath 2, Scales 2

Prerequisites

[Fire Breath](/legends/fire-breath), [Acid Breath](/legends/acid-breath), [Cold Breath](/legends/cold-breath), [Lightning Stare](/legends/lightning-stare)

> You breathe deeply and take a low stance. Your eyes track your opponent mercilessly, and smoke trails out of your nostrils while acid drips from your lips. While this form is active, none of the prerequisite Jutsu cost anything to activate. Breath of the Dragon has no form Skills.
    