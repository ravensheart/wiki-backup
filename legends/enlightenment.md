---
title: Enlightenment
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Fine Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Academic](/legends/academic)
*   [Investigator](/legends/investigator)
*   [Mystic](/legends/mystic)
*   [Philosopher](/legends/philosopher)
    