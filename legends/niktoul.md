---
title: Niktoul
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dark God
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Niktoul
=======

The dark one, the stranger. The Assassin.

### Consecration or Dedication

To each penitent is whispered a name. To each a task. To each, a life to be taken, in service to the agenda of Niktoul. The life must be taken in such a way that the penitent cannot be held accountable for it as a crime. Those who Dedicate to Niktoul gain grey hair after they bathe in the oils derived from the blood of their sacrifice.

### Covenant

Niktoul admonishes that one must never underestimate the danger of an enemy, and so must never leave one alive willingly.

### Attunement

Niktoul grants his Unholy Bonus to all rolls to acquire and maintain social stealth and disguise.

### Gift

Niktoul grants the power of Pearl Mastery.

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.
    