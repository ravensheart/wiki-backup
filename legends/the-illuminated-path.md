---
title: The Illuminated Path
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Mythril Sphere
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Illuminated Path is the path for those who seek a better world. Any Martial Artist who has the Higher Purpose: Make the World a Better Place and gains no marks against it for a full month gains access to this path and may learn it in any order. They lose the ability to learn new things from these styles if they ever violate that Higher Purpose, but only until they have gone another month without doing so.

*   [Flaming Palm](/legends/flaming-palm)

The Illuminated Path Master Jutsu
=================================
    