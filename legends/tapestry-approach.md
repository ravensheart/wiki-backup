---
title: Tapestry Approach
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosophy Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Tapestry Approach (Intellect, -8)_: You have studied the Tapestry approach to philosophy. You can analyze things from the perspective of the mundane and transmundane, and tell if something is good for the world in a quiet, long term way. You understand the minds of Titans and Elementals, as well as plants and animals, at least in an abstract way.
    