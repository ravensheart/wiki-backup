---
title: The Onyx Sphere
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Martial Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

*   [Anthropotransmogrify](/legends/anthropotransmogrify)
*   [The Black Spiral](/legends/the-black-spiral)
*   [The Dark Song](/legends/the-dark-song)
*   [The Twisted Path](/legends/the-twisted-path)
    