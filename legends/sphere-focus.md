---
title: Sphere Focus
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Connoisseur Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Sphere Focus (Varies, -2):_ The Connoisseur has spent time focusing on a particular sphere within a particular art. Each of the 16 spheres is its own Technique, and the penalty for this Technique stacks with that of Cardinality Focus; both always apply if Sphere applies. Success on this technique gives the Connoisseur an idea of what talent governed the skill used to make the thing he observes, as well as some clue as to the utility and cost of the thing; how to operate a gun, or what kind of bonus (generally) an Enchanted sword gives, what sort of energy a spell used, that kind of thing.
    