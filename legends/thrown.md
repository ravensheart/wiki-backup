---
title: Thrown
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Combat
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Thrown is the combat skill that covers attacks with weapons that leave your hand when you attack with them.
    