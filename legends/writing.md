---
title: Writing
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Academic
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Writing is the skill of creating meaningful text, both documentation of the world and fantastic new worlds.
    