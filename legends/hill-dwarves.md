---
title: Hill Dwarves
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dwarves
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Hill Dwarves nearly universally have the [Civilized God Virtues](/legends/civilized-god-virtues).

Hill Dwarf Knacks
-----------------
    