---
title: Gnomes
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Browen
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Gnomes frequently have the virtues associated with the [Many Facets of Truth](/legends/many-facets-of-truth), though many have the virtues of the Civilized Gods instead.

Gnome Knacks
------------

Innovative
----------

Cost

16 experience

Prerequisites

> Gnomes are renown for their new ideas, and are continually pushing the envelope in all areas they get involved in. This Knack grants the Gnome an automatic success on all rolls to create something genuinely new; this can be anything from a new Spellform to a new Rude Arts invention to a new Jutsu. Even enchanting a Rune Magic item that has a genuinely novel function. This does not apply to refining old ideas or changing things incrementally, only for brand new ideas. This Knack can be purchased more than once, up to the lower of the character’s Hun and Po, and its effects stack.

Sharp Nose
----------

Cost

4 experience

Prerequisites

none

> Some species have a sense of smell that a bloodhound would envy. Each level of this Knack grants the character an automatic success on all smell and taste related Perception checks, and can be taken up to as many times as the character’s Perception.
    