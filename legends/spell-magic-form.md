---
title: Spell Magic Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Form of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Spell Magic:_ Spell Magic uses Spellforms and requires that the Mage learn each spell in order, learning prerequisites before learning the next spell. Normally, Spell Magic users must find the spell somewhere in the world to learn it. Each Spell is similar to a Technique, but Spell Magic users have an advantage over others in that they can train specific Spells to the point where there is a bonus, rather than a penalty. This bonus cannot be greater than the spellcaster’s Hun, and training any given spell counts as a Specialty. Unlike Techniques, Spells cannot be cast without first putting points into the Spell, and one cannot put points into a Spell until one has points in all prerequisite Spell.
    