---
title: Time Powered
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Time:_ Some magics, primarily magics that can only be used on enchanting, allow the character to put in only time and produce magic. Such magics produce 100 energy per hour, but again, can usually only be used to permanently enchant things. Some such magics can produce single-use items faster, usually at the rate of 1 energy per minute.
    