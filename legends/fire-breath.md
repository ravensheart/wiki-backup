---
title: Fire Breath
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Breath of the Dragon Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Fire Breath
-----------

Cost

2 w

Type

Normal Instant

Requirements

Breath 2, Scales 2

Prerequisites

[Shocking Glance](/legends/shocking-glance)

> Your first real taste of the Breath of the Dragon, you may breathe fire… though, it is weak and short-ranged. You may use this Jutsu to make an Agility + Thrown attack with a range increment equal to your Path Talent. This attack has no accuracy or damage bonus, but it does do lethal Fire Hazard damage. It is also likely to come as a surprise to many opponents, preventing them from defending well (GM’s call).
    