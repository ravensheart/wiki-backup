---
title: Tracking
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Beastmaster
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

This is your ability to track and find creatures, including humans and beasts. It works in all environments, but is better suited to natural settings.

_City Tracking (Perception, -4)_: This technique allows you to track prey, to include humans, through any urban environment.

_Sky Tracking (Perception -10)_: This technique allows you to track flying things through the air.
    