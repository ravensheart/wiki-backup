---
title: Karate
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Combat
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Karate is the skill that governs all formalized unarmed combat. It covers kicking, punching, head-butting, and various other techniques employed in formal combat.

_Deceptive Attack (Agility, -2 or more):_ This is a deceptive attack, helping you bypass your opponent's defenses. It reduces your opponent's Dodge, Parry or Block against this attack, only, by 1 for every -2 to skill you take.

_Lethal Strike (Agility, -6)_: Normally, unarmed attacks only do non-lethal damage. With the proper training, however, a skilled unarmed combatant can do real damage with their bare hands.

_Multi-Strike (Agility, -3 per extra attack)_: This technique allows the Artisan to make multiple attacks in a single action. Every attack made is at the same penalty, which is -3 per attack beyond the first. As normal, this technique can be trained up as a Specialty, but no more levels in the specialty than the character’s skill level. Further techniques can be used on these strikes, but the same techniques must be used on every strike, and the penalty stacks. Unlike most techniques, Multi-Strike takes a lot of concentration; all of the penalties for using this Technique apply not only to the character’s attack pool, but also to his defensive pools, until his next action.
    