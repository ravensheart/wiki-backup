---
title: Deep Dwarves
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dwarves
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Most Deep Dwarves have the Twisted Virtues.

Deep Dwarf Knacks
-----------------

Void Resistant
--------------

Cost

8 Experience

Prerequisites

> Some creatures are just better at resisting the Void than others. Each purchase of this Knack gives the character an extra die for the purpose of resisting Void effects, including most poisons and toxins and all Rude Arts. It also subtracts one from the die pools of any Rude Artist attempting to target the character directly with his arts. This can be purchased multiple times, but no more than the character’s Khat. Its effects stack.
    