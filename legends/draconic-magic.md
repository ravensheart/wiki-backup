---
title: Draconic Magic
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  core, Core, ethereal, magic arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

**Draconic Magic is a Will Based Willpower Powered Core Realm Magic**. There are five realms in Draconic Magic: Arcane, Elemental, Etherial, Vital and Divine. Each realm has 5 circles, which are, roughly, Sense, Control, Transform, Create and Mastery. All of the modifiers cost energy, but higher levels in some realms expand the utility of this. Each circle in each realm costs 20 points. Casting time is reduced by skill, energy is not. The Arcane realm controls magic itself. With the second circle, the character can manipulate existing spells and enchantments created with lesser magic. The third circle allows the character to permanently alter the enchantments of lesser magic items, grant someone magery, and similar. The fourth circle allows him to increase the ambient mana level, grant targets Mana Enhancer, and, when included with other magic effects, allows him to use the Duration Effect Modifier to calculate the increased fatigue cost instead of paying maintenance costs. Mastery allows him to create enchantments and truly permanent effects by spending 12 extra fatigue to enhance the duration. Enchanting can also be accomplished through traditional means by including Arcane 2 in the effect. The Elemental realm gives control over all things material and non-living. Besides the obvious, the Elemental realm is also quite good at doing damage. Elemental damaging effects do (Realm Level) squared dice of damage per multiple of base cost. Level 4 allows for the creation of essential elements, while Level 5 allows for the creation of custom elements. Etherial governs the mind. 1st circle allows sensing minds, 2nd circle allows communication and influence, 3rd allows outright control, including instilling permanent disadvantages, 4th allows the creation of new minds (frequently used to make magic items sapient) and 5th allows full transcendence of normal limitations of the mind. Etherial also governs space and time, allowing portals to be created or altering the flow of time. Vital allows healing at the same rate as Elemental does damage. It can also drain health, at level 3 or higher, at 1d per base cost. Adding new advantages requires Vital 3, making them pass on to future generations requires Vital 4. Creating entirely new species from one’s imagination requires Vital 5. The Divine realm deals with gods, spirits and such beings. The Dragon can use this realm to summon divine avatars, damage gods directly, bind spirits to his will, and the like.

Time:
-----

Draconic magic takes (Highest realm used +2) seconds to cast, reduced for high skill based on the skill level in said highest realm.

Cost:
-----

Base Cost is twice the highest realm used, plus the sum of the other realms. For example, an effect that used Arcane 4, Elemental 3 and Vital 2 would have a base cost of (4\*2+3+2) 13 fatigue.

Range:
------

+1 energy per 2 full yards of range. Including Etherial 2 allows the use of Long-Distance modifier instead. Etherial 3 allows the use of sympathetic magic.

Range in Time:
--------------

see Long-Distance modifiers

Duration:
---------

Base Cost per minute for normal, “lasting” effects. Effects that are normally instant can be made lasting at -2 to skill; this allows them to be maintained at a rate of Base Cost per 10 seconds. Including Arcane 4 allows the Dragon to spend energy to climb the effect duration table in Thaum. Including Arcane 5 allows the Dragon Mage to make such effects permanent by spending 12 energy.

Area of Effect:
---------------

Base Cost is multiplied by the Radius in yards of the effect.

Damage:
-------

Base Cost is multiplied by number of dice of damage done (more for Elemental).

Healing:
--------

Base Cost is multiplied by multiple of Base Healing done.

Personal Abilities:
-------------------

Multiply Base Cost by (sum of absolute values of traits added)/10. Weight Affected, Multiple Targets and Multiple Elements: use tables, page 243 in Thaumotology

Overcoming Resistance:
----------------------

Dragons can include Arcane 4 in order to bypass Magic Resistance. Arcane 5 allows spells to be cast in No Mana zones at twice the normal cost. Including Vital 5 allows one to reduce the target’s effective HT for the purposes of resisting this single effect by 1 per 10 extra fatigue spent. Same can be done with Will and Etherial 5. Arcane 5 can be included in order to reduce the resistance to dispelling granted to one spell by another spell. These can also be flipped, allowing the Dragon to make his own spells harder to dispel or aiding in his resistance to the magic of others.
    