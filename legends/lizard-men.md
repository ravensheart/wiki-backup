---
title: Lizard Men
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dragonkin
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The big, burly cousin of the Kobald, Lizard Men are big and mean.

Lizard Man Knacks
-----------------

Brutal
------

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.

Lizard Tail Recovery
--------------------

Cost

16 experience

Prerequisites

> Even the most serious injury, if it does not kill you, can be overcome. Any Crippling injury heals once all of the Hit Point loss associated with its infliction is healed. Amputations require more potent solutions.

Tough
-----

Cost

16 experience

Prerequisites

> Some creatures are just tough. The character gains a natural AR of 1. This increases by 1 for every extra time this Knack is purchased, no more times than his Khat.
    