---
title: One Handed Chop Sword
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  weapon
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

A single edged sword, often shorter than cut & thrust sword.

**Time Cost**

**Damage**

**Damage Type**

**Accuracy**

**Defense**

**Reach**

6

+3

Lethal

0

+1

1
    