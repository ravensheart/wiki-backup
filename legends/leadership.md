---
title: Leadership
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Tactician
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is your ability to lead others from the front, giving comfort and discipline in appropriate measure and inspiring loyalty in others.
    