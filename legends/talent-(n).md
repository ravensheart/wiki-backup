---
title: Talent (n)
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Glossary
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

A talent is a 3 quatrine trait. It governs 4 skills, and there are 4 talents to a sphere. Every Cardinaltiy has 16 talents. Talents in Magic Arts are called Magery, Talents in Fine Arts are called Callings, Talents in Rude Arts are called Carriers, and Talents in Martial Arts are called Paths.
    