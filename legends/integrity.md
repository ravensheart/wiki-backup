---
title: Integrity
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Empath
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This skill represents one's ability to be internally consistent and not be swayed by factious arguments.
    