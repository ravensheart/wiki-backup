---
title: Cross-Trump Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Suit Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cross-Trump Attrition allows a player to Trump from a secondary suit. For example, if the Challenge is in the suit of Wood and someone has already played a Metal card, the player can play a Fire card to trump the Metal, but only if he has no Wood and no Metal cards in his hand. This can theoretically go all the way around to Earth being the final trump. Crystal Cross-Trump also exists, and functions in the same way.
    