---
title: Gardening
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Steward
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of maintaining a garden, a small plot of land wherein vegetables and fruits are grown, usually for personal use.
    