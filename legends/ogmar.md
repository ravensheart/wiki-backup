---
title: Ogmar
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dark God
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Ogmar
=====

Ogmar is the scholar of the Dark Gods. He values all knowledge, even that which is hidden or forbidden. Ogmar seeks to understand everything, even that which would spell his own doom. One of the strangest gods, Ogmar regularly ventures into the Void to seek hidden or lost understanding.

### Consecration or Dedication

The Dedication of Ogmar involves the crafting of a copy of the Tome of Five Shadows. This requires that the penitent kill a Person and use their body in an arcane ritual. Completion of this ritual requires Rituals of the Dark Gods at 16 and Dark God Investiture at 4, and results in a [Tome of Five Shadows](/legends/the-tome-of-five-shadows) crafted from the flesh of the victim. This book is enchanted to be quite durable; it has an AR of 10 and 100 hit points, and heals 1 hit point per day. The penitent must then gift this book to someone who seeks the knowledge therein. Afterwards, the penitent must bathe in an oil prepared from the blood of the sacrifice. This oil turns the penitent's hair pitch black.

### Covenant

The Covenant of Ogmar is to never destroy knowledge, which includes the knowledge of skillful individuals and the knowledge stored in books.

### Attunement

Ogmar grants the priest an Unholy bonus to all spellcasting checks and all Intellect checks.

### Gift

The Gifts of Ogmar include access to several Knacks that are useful to spellcasters, as well as access to [Arcane Mysteries](/legends/arcane-mysteries).

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.
    