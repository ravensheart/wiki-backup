---
title: Nephendus
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Black Magic
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The Nephendi are dark, terrible beings that empower themselves by consuming the flesh of others; Nephendus is a Will-Based, Blood-Powered Empowering Magic. The ritual hunt is the center of the power of the Nephendi. First, the Nephendus must choose a victim. It must be someone capable of fighting back: they cannot be physically or socially restrained against defending themselves from the Nephendus. It must also be a Person; animals give the Eater nothing. The Nephendus must then hunt his victim. The victim must know he is being hunted, and the hunt must last at least one hour. The Nephendus must then capture his victim alive and take them to a pre-prepared place of sacrifice (which must be, amongst other things, out of the sun). He then sacrifices (and eats) his victim, making his Nephendus roll then. The character’s appetite is supernatural, and it only takes approximately one minute per hit point to eat a person using this magic. The Nephendus never gets sick from this consumption so long as he succeeds on the above roll, and may hunt down and eat as many people per day as there are hours; he never gets full. Each successful hunt provides the Eater with all of the food and water he needs for a full day.

Should the Nephendus succeed in all of this, he gains a number of character points equal to his Magery times the lower of the number of successes on his roll or the sum of the Victim's souls. If the character point value of the sacrifice is greater than that of the Nephendus himself, this reward is doubled.

These character points can be spent immediately, or banked. They cannot be spent on any skill that neither the Nephendus nor his victim knew (with some exceptions, below), nor can they be spent on any form of Celestial magic or Knacks (Power Investiture, Blessed, etc; there is an exception to this rule). Banked experience can only be spent on traits that cost less than the character’s Nephendus \* Black Magery per level. Explicitly allowed are all forms of Abyssal Magic (even ones the character would otherwise not have access to), any of the four Abyssal Mageries, and all Magic Knacks. Other forms of magery and other magical skills can be purchased using points acquired from eating a mage (assuming they had those skills), or with banked experience if the character already has access to that form of magic. Banked points can also be spent on Cooking, Shadowing, Tracking, and Stealth as these help directly in the hunt. There are also a number of Nephendus-specific Knacks that are available.

Experience banked from Nephendus can be spent to increase the character's Po, Alma, or Khat, but only up to his Hun. It may be used to increase his Hun without limit, though like all traits this can only be done if the character's Nephendus \* Blakc Magery is 32 or higher. Increasing the character's Khat in this way bypasses training time, but increasing his Alma does not. Only those Artisan talents the character already has access to may be purchased, with the exception of Steward, Combat and Beastmaster. Same with Rude Arts, with the exception there being Shadow. Any Void Path Talent may be purchased with banked experience, but experience may only be spent on Jutsu (Martial or Rude) if the character already has access to them. Techniques for any skill the character knows may be improved with banked experience, though some specialized techniques might require further justification.

Nephendus Knacks
================
    