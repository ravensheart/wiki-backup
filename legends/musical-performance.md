---
title: Musical Performance
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Performer
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill that covers playing of any musical instrument.

_Specific Instrument (Charisma, -2 to -6)_: This technique represents training in the use of a specific musical instrument. Some instruments are harder to master than others.
    