---
title: The Arts
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  TableOfContents
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Skill tree is broken down by Art. Ever Art is goverened by a respective Soul.

*   [Fine Arts](/legends/fine-arts)
*   [Magic Arts](/legends/magic-arts)
*   [Martial Arts](/legends/martial-arts)
*   [Rude Arts](/legends/rude-arts)
    