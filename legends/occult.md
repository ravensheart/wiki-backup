---
title: Occult
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Cunning Folk
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Occult is the skill of dealing with spirits and gods by invoking their rules and rites. It is knowledge of the supernatural in a eminently practical sense.

_Last Rites (Charisma, -2)_: You know how to put the dead to rest. Success on this technique allows you to weaken the Po of the deceased by your Cunning Folk talent level, usually preventing it from coming back and haunting the land.
    