---
title: Innovative
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Gnome Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

16 experience

Prerequisites

> Gnomes are renown for their new ideas, and are continually pushing the envelope in all areas they get involved in. This Knack grants the Gnome an automatic success on all rolls to create something genuinely new; this can be anything from a new Spellform to a new Rude Arts invention to a new Jutsu. Even enchanting a Rune Magic item that has a genuinely novel function. This does not apply to refining old ideas or changing things incrementally, only for brand new ideas. This Knack can be purchased more than once, up to the lower of the character’s Hun and Po, and its effects stack.
    