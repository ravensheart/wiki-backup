---
title: Thaumotology
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Connoisseur Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Thaumotology (Varies, -4):_ This is the study of magic. A successful roll when analyzing magic gives more than the usual information… usually, just which sphere it came from, and the general purpose or intent behind the object or event. The Primordial Skills are inscrutable beyond this point, simply revealing that it was Wyld Magic.
    