---
title: Double Trump Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Suit Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

In this variant, each suit has two trump suits. Metal trumps Wood and Earth, Earth trumps Fire and Water, Fire trumps Wood and Metal, Wood trumps Water and Earth, Water trumps Metal and Fire. If two trump suits are both played on the same play, one will trump the other, and that one wins the Challenge.
    