---
title: Dark
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  ethereal, Ethereal, magic arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Dark Magic [opposes](/legends/opposing-magics) [Crystalline Becoming](/legends/crystalline-becoming)
    