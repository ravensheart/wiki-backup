---
title: Sturdy
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dark Elf Knacks, Dwarf Knacks, Ogre Knacks, Orc Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

16 experience

Prerequisites

> Some races are tough to kill. Each level in this Knacks adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Khat.
    