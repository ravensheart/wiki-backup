---
title: The Boy Who Became a Mountain
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Library, Work In Progress
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Once, There was a boy who became a mountain...
----------------------------------------------

One of this worlds forgotten children. He was born of the City's forges, and quenched in Garden's waters.
    