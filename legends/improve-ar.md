---
title: Improve AR
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Craft: Tapestry Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Improve AR (Intellect, -4 per AR)_: This technique allows you to improve the AR of armor beyond the normal limits. The penalty for this technique is equal to the difference between the desired final AR and the mundane AR of the armor (not counting enchantments). Improving the AR of a piece of armor has difficulties and times similar to that of making the armor in the first place.
    