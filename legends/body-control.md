---
title: Body Control
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Mystic
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Body Control is the skill of consciously affecting your body through your Will.
    