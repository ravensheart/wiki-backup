---
title: Fel Evocation
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Celestial
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Fel Evocation [opposes](/legends/opposing-magics) [Black Magic](/legends/black-magic)
    