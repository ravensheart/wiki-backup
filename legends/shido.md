---
title: Shido
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Martial Arts Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Shido represent the character's personal code that he has grown to consider inviolate. Shido are, for the most part, identical to Higher Purposes, with the exception that a character's Shido are used to calculate his [Chi Bonus](/legends/chi-bonus). The drawback to this is that the character loses access to his Chi Bonus if he gains any marks against his Shido, until he repents. This can be devastating mid-combat.
    