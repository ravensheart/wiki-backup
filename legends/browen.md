---
title: Browen
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Gnomes
======

Gnomes frequently have the virtues associated with the [Many Facets of Truth](/legends/many-facets-of-truth), though many have the virtues of the Civilized Gods instead.

Gnome Knacks
------------

Innovative
----------

Cost

16 experience

Prerequisites

> Gnomes are renown for their new ideas, and are continually pushing the envelope in all areas they get involved in. This Knack grants the Gnome an automatic success on all rolls to create something genuinely new; this can be anything from a new Spellform to a new Rude Arts invention to a new Jutsu. Even enchanting a Rune Magic item that has a genuinely novel function. This does not apply to refining old ideas or changing things incrementally, only for brand new ideas. This Knack can be purchased more than once, up to the lower of the character’s Hun and Po, and its effects stack.

Sharp Nose
----------

Cost

4 experience

Prerequisites

none

> Some species have a sense of smell that a bloodhound would envy. Each level of this Knack grants the character an automatic success on all smell and taste related Perception checks, and can be taken up to as many times as the character’s Perception.

* * *

Kender
======

Kender usually have very low virtues, and don’t pay them much mind in general. Nearly any virtue system is appropriate for a Kender, in that they are all equally inappropriate.

Kender Knacks
-------------

Foolhardy
---------

Cost

16 experience

Prerequisites

> Kender are either foolishly brave or bravely foolish. Either way, fear just doesn’t seem to be an idea that they really understand. Each level in this Knack gives the character a bonus success to resist Fear. It can be taken multiple times, up to the character’s Alma, and stacks.

Impressive Vocabulary
---------------------

Cost

8 experience

Prerequisites

> Kender are well known for their creative use of words, especially when angry. The Kender may roll their Wits + Fast Talk at a difficulty of their target’s SDR; this roll must be stunted, and the Knack does not function unless the player gets at least one stunt die. If they succeed, their target must spend a Certainty or attack them next round. Using this Knack in combat is a reflexive action, and if the target is already engaged in physical combat, he does not have the option to spend Certainty.

No invitation
-------------

Cost

8 experience

Prerequisites

> A locked door is an invitation, at least to a Kender. This Knack grants one automatic success on all rolls related to opening a locked portal, no matter the means. It can be purchased more than once, up to the character’s Po, and its effects stack.

* * *

Nelwin
======

Nelwin have the virtues of the Court of Heaven much of the time, though a surprisingly large minority has [The Silent Virtues](/legends/the-silent-virtues).

Nelwin Knacks
-------------

Small and Quiet
---------------

Cost

16 Experience

Prerequisites

> The smaller races have a knack for hiding in the shadows. Each level of this Knack gives the character an automatic success on all rolls to stay hidden or to prevent someone from tracking them. It can be taken no more times than the character’s Po.

The Importance of Family
------------------------

Cost

16 experience

Prerequisites

> Nelwin find the people around them are the most important part of their lives. Whenever a Nelwin with this knack turns in 5 marks from a Deep Connection for experience, he gains 10 experience, rather than 5.

The Little Things
-----------------

Cost

16 experience

Prerequisites

> Other races care about grand things. Castles and armies and great magics. Nelwin love the little things. Afternoon tea. The breeze across their porch. Enjoying a drink and a song at the local tavern. It is these things that the Nelwin values, and it is these things that keep them going. The character gains an automatic success to resist Despair for every level in this Knack, and can have as many levels in this Knack as his Alma. He need only remember the taste of bread.

* * *

General Browen Knacks
=====================
    