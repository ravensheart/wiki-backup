---
title: Reduce Mobility Penalty
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Craft: Tapestry Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Reduce Mobility Penalty (Intellect, -4 per MP)_: This technique allows you to reduce the Mobility Penalty of a piece of armor. It is at -4 per difference between the natural MP and the desired MP, not counting enchantments. The difficulty of this roll is the difference in MP, and the number of successes needed is five times that. Each roll takes an hour, and the character's Artificier talent multiplies the successes before applying them to the number needed (no talent means the successes are multiplied by 1/2).
    