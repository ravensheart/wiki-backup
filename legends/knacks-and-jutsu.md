---
title: Knacks and Jutsu
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

In addition to all of the skills, talents, and specialties, characters will frequently have access to Jutsu and Knacks. Jutsu have a fixed experience cost, and are usually arranged in Cascades, where all the Jutsu other than the first require that the character learn some or all of the previous Jutsu first. Jutsu have a fixed experience cost (8?), and are usually something that one activates, though some provide static bonuses. All Martial Arts Styles have Jutsu cascades that must be learned to master them. Knacks, on the other hand, are usually static in nature, rarely have prerequisites other than certain skill and talent levels, and can cost anything from 2 to 100 experience.
    