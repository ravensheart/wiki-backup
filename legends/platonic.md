---
title: Platonic
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  ethereal, Ethereal, magic arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Platonic Magic [opposes](/legends/opposing-magics) [Void Magic](/legends/void-magic)

*   [Pearl](/legends/pearl)
*   [Phantasmagoria](/legends/phantasmagoria)
*   [Rune](/legends/rune)
*   [Tattoo](/legends/tattoo)
    