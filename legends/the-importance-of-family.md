---
title: The Importance of Family
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Nelwin Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Cost

16 experience

Prerequisites

> Nelwin find the people around them are the most important part of their lives. Whenever a Nelwin with this knack turns in 5 marks from a Deep Connection for experience, he gains 10 experience, rather than 5.
    