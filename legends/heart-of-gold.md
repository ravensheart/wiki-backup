---
title: Heart of Gold
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Artisan Knack
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

20 experience

Prerequisites

Master of Philosophy, Psychology, and Integrity. No Fatal Flaws or Vices. Virtues (not Urges)

> You have a heart so pure and clean that temptation and vice have no effect on you. You are immune to all Ordeals not caused by the use of Magic, and even those cannot cause you to gain Marks against your virtues. You cannot gain Fatal Flaws or Vices, and are totally immune to any Rude Arts effect that would force you to do so. Your heart is also now literally made of Gold, and molten gold flows in your veins. Anyone who damages you in melee combat automatically takes a level of Fire damage (Armor protects) as they are splashed with your blood. This blood of gold is extremely useful to blood mages, and is worth 5 times the normal energy.
> 
> You lose all effects of this Knack if you ever somehow gain a Fatal Flaw, or fall to the Dark Urges.
    