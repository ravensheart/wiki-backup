---
title: Strife
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Embers Virtue, Hun Virtue, Virtue Resists Fear
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Life is pain. No pain, no gain.
    