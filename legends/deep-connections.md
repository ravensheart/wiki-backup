---
title: Deep Connections
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Characterizations
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Deep Connections are the individuals that a character cares about. These are always people, and always have some sort of emotional context. The emotional context can be love or hate, and should have an adjective to specify why that emotion is evoked. Brotherly Love for a comrade in arms, for example, or Ideological Hatred for a politician. A character can have no more Deep Connections than his Khat.
    