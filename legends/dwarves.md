---
title: Dwarves
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Deep Dwarves
============

Most Deep Dwarves have the Twisted Virtues.

Deep Dwarf Knacks
-----------------

Void Resistant
--------------

Cost

8 Experience

Prerequisites

> Some creatures are just better at resisting the Void than others. Each purchase of this Knack gives the character an extra die for the purpose of resisting Void effects, including most poisons and toxins and all Rude Arts. It also subtracts one from the die pools of any Rude Artist attempting to target the character directly with his arts. This can be purchased multiple times, but no more than the character’s Khat. Its effects stack.

* * *

Hill Dwarves
============

Hill Dwarves nearly universally have the [Civilized God Virtues](/legends/civilized-god-virtues).

Hill Dwarf Knacks
-----------------

* * *

Mountain Dwarves
================

Mountain Dwarves frequently have the [Virtues of the Old Gods](/legends/virtues-of-the-old-gods) or the [Virtues of the Many Facets of Truth](/legends/virtues-of-the-many-facets-of-truth).

Mountain Dwarf Knacks
---------------------

* * *

General Dwarf Knacks
====================

Starlight Vision
----------------

Cost

4 experience

Prerequisites

> You are at home in dim light, such as that provided by the stars or a dim torch. You can see five times as far using a single light source as a normal human, and see by starlight just as well as a human does in full daylight. A character with both this Knack and Home in Darkness suffers no penalties is full daylight.

Sturdy
------

Cost

16 experience

Prerequisites

> Some races are tough to kill. Each level in this Knacks adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Khat.

Tough
-----

Cost

16 experience

Prerequisites

> Some creatures are just tough. The character gains a natural AR of 1. This increases by 1 for every extra time this Knack is purchased, no more times than his Khat.
    