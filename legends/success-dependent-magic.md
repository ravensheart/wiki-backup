---
title: Success Dependent Magic
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Magics that use increased costs to govern their parameters show a great deal of control for those parameters. So long as the casting roll is successful at all, the parameters paid for with increased cost take exactly the form and magnitude specified at the time of casting.

Magnitude:

0: Sensory effects only  
1: 1 die of damage, 10 kilos, 8 character points  
2: 2 dice of damage, 20 kilos, 16 character points  
etc.  

Scope ([AoE](/legends/aoe)):

0: Self only  
1: 1 target Missile Spell  
2: 2 Targets  
3: 3 Targets  
4: 4 Targets  
5: 5 Targets...  
6: Area of Effect  

Range:

0: Self, or touch range  
1: 10 meter range for non-sensory, 10 meters for sensory  
2: 20 meter range for non-sensory, 100 meters for sensory  
3: 30 meter range for non-sensory, 1,000 meters for sensory  
Etc.  
...  
10: Maladiction  

Range in Time:

0: Now  
1: 1 minute for non-sensory, 10 minutes for sensory  
2: 10 mtes for non-sensory, 1 hour for sensory  
3: 30 minutes for non-sensory, 12 hours for sensory  
4: 1 hour for non-sensory, 1 day for sensory  
5: 2 hours for non-sensory, 1 week for sensory  
6: 3 hours for non-sensory, 1 month for sensory  
7: 4 hours for non-sensory, one season for sensory  
8: 5 hours for non-sensory, one year for sensory  
9: 6 hours for non-sensory, one decade for sensory  
10: 7 hours for non-sensory, one century for sensory  
+1 hour per level for non-sensory, x10 per level for sensory  

Duration:

0: Instant 1: 1 minute for non-sensory, 10 minutes for sensory  
2: 10 minutes for non-sensory, 1 hour for sensory  
3: 30 minutes for non-sensory, 12 hours for sensory  
4: 1 hour for non-sensory, 1 day for sensory  
5: 2 hours for non-sensory, 1 week for sensory  
6: 3 hours for non-sensory, 1 month for sensory  
7: 4 hours for non-sensory, one season for sensory  
8: 5 hours for non-sensory, one year for sensory  
9: 6 hours for non-sensory, one decade for sensory  
10: 7 hours for non-sensory, one century for sensory  
+1 hour per level for non-sensory, x10 per level for sensory
    