---
title: Ritual Magic Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Form of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Ritual Magic:_ Ritual Magic functions like Spell Magic, except that the Spells function exactly like Techniques of other cardinalities. Spell Techniques of Ritual magic have a penalty equal to their prerequisite count, and can be trained as Specialties just like with Artisan.
    