---
title: Wrath
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Dark God Virtue, Po Virtue, Virtue Resists Fear
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Wrath is the Po virtue of the Dark Gods. It represents the character's capacity for anger and hatred, and tends to represent a more long-term anger than similar virtues. Wrath resists Fear.
    