---
title: Trolls
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Twisted Folk
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Trolls usually have the [Dark God Virtues](/legends/dark-god-virtues).

**Biggun**  
Cost: 8 experience  
Some creatures just grow big. Each level of this knack reduces the training time needed to increase the character’s Khat by half. This Knack can be taken multiple times, up to once per level of the character’s Hun.

Cost

16 experience

Prerequisites

> Even the most serious injury, if it does not kill you, can be overcome. Any Crippling injury heals once all of the Hit Point loss associated with its infliction is healed. Amputations require more potent solutions.
    