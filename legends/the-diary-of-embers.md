---
title: The Diary of Embers
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Holy Texts, Library
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

This text is the record of the great traveler and polymath Theocritus, Who has made it his duty to record and document all of the great works of knowledge in the world.

An Age of Embers
================

We are all of us a mixture of the truths we choose to breath in and the lies we choose the breathe out. Sometimes it is the other way around, and sometimes it is even true that lies tell us more than truth ever could. In the world there are many eyes to see through, Many challenges to conquer, Many wrongs to right, and many beauties to behold. Life would be trite and incomplete without each of these, and especially without the ability to drink deeply from the well of understanding that bears each of them to us. With each of these enthymemes the Council of Embers embodied and possessed to us a great journey. That will never cease until we consecrate ourselves to each and every one of their teachings, or better yet. Seek out even greater Truths, Dangers or Challenges. This diary of the ember siblings is recorded by a traveller who shall for the sake of the knowledge that this tome bears, remain anonymous.

The Chronicle of Clockwork Ember
================================

There Once was a little girl who thought the world a pitiable mess. She grew up in a small village of 25, where each of her mothers and father possessed talents for the arts and the great crafts of the realm. She never thought she could paint like her sister, or teach like her middle brother. Or speak as well as her eldest brother, but she never failed to find a solution to every little problem that fell in her way.

When she was 10 she constructed a little sawmill that made timber faster than all the lumberers in town, When she was 15 she made a machine that could soar through the sky and mastered its every quivering tremble like an animal. And when she was 23 years of age, a rival nation, the Whoevers came down from the mountainside near the edge of the island where her siblings dwelt. Her two brothers and sisters fought valiantly but for her part Qellektria knew that the weapons of the enemy were too great and numerous to fight. So she took it upon herself to bolster the defenses with great machines of war, and various inventions that could slay men. She took no pleasure in it, and lost many nights of sleep to the screams of dying soldier outside her home. But she would build her engines of destruction and terror with efficiency, with the hope that their effectiveness would strike awe into the hearts of her enemies. And lo it was so…that the ember of Clockwork burned bright to light the destiny of Qellektria and her siblings. And so once the Whoevers were defeated Qellektria was disquiet with with the grief of so many lives lost. So she kept her defenses active and the guns never left the walls of her city. As a quiet reminder of the fury of her intellect, and the depths of her love for her family and home.

The Chronicle of Scarlett Ember
===============================

There Once was a sister to clockwork who never saw beauty in function. Never felt at home where anything was predictable. Who only loved the world when it vexed and amused, lied and shifted. Like a small child who has more confidence than a towering warrior, But less the knowledge of all things in the world. In this there is a wisdom only known to those who relinquish the deathgrip that we all have to coherence and security.

She never needed a reason to create anything, instead she relished in letting what beauty came to her in the act of creation take her to whatever meaning it might have to herself or others in the wider world. Her creations always led those who experienced them to have some higher understanding of purpose. The meaning behind the work, the message it told, to moral of the story, or the emotions the music evoked were wholly secondary to the miracle of the art itself.

Even on her deathbed she couldn't have told you what mystery her life had solved. Even though the entire length of it was spent unravelling a single mystery that she admitted she failed to comprehend its entirety, and those closest to her attested that she died more fulfilled than anyone they had ever known.

Such was the life of Kharmine of Scarlet Ember, so shrouded in the mystery of creation and beauty that she vanished into it.

She vowed to never take credit for the beauty she created and she credited any morsel of success she had to her love of the craft of creation. In her last breaths she uttered these words to her followers:

“There is beauty in pain and suffering that allow you to savor it like a fine wine. The dust of the field and the breath of the winds have as many succulent flavors as the finest wines. If only you learn to taste of them.”

The Chronicle of Dauntless Ember
================================

Some say it’s like a disease...Like a poison of the blood that you just can't get out. The need for danger, the hunger for more speed, The need for more danger like a drug.

Telia never went a day in her life without her blood slamming hard against the walls of her heart.

Truly I think Talia never thought of where she was or why she did what she did. The only thoughts she had for anyone were her four brothers and sisters who grounded her and cared for her. She was a screeching tyrant of daring, hopelessly impulsive and needlessly risky.

As she grew older her thirst for danger increased, Flying into hurricanes with flimsily constructed mock ups of her eldest sisters designs. Hanging or leaping from cliff sides with gadgets hastily fashioned from her sisters stolen designs.

When the Whoevers arrived at the shores and the need was dire, it was Talia who vanished into the night to meet them and without fail found herself impossibly outnumbered by adversaries, or tasked with rescuing countless civilians from a cliff face, Or even single handedly steering an unstoppable siege engine of the enemy into the sea before it violently exploded. No one ever asked for these tasks, Her siblings merely braced themselves for the ultimate sacrifice right before Talia would swoop in with salvation in the form of some impossible stunt.

She wished for a scream in her teeth. a roar of hellish intensity made loud upon her silent visage. A phantom of terror cast in her eyes and a face as a Dauntless Ember. Proof that she had been taken by some unearthly thing. All wound up into the simple smirk on her face as she flung herself headlong into the howling wind.

To seek Talia of Dauntless Embers is to court such an elemental mistress, as power, terror and ecstasy itself. You will hear her whispers at the edge of cliffs, at the depths of the oceans,racing through the meadows. free falling through the sky.

The Chronicle of Ephemeral Ember
================================

The eldest of the ember siblings was a philosopher who grounded and advised the lot of his siblings. At trying times in their lives each of them came to the eldest sibling for comfort and guidance, and through his life he was known only as Axumander.

Every follower of the Axumander has never written down his philosophies, preferring instead to have them passed down purely through oratorical means. But it is these itinerant scholars who hold the keys to many secrets that elude the most studious of magical scholars. I have elected to mark down what the monks of ephemeral embers I have met in my travels have called the greatest of Axumander’s secrets, the secret the now bears the name of the pantheon itself.

“Fire consumes the wood that it burns, Essential Metal destroys Wood, All the primal elements have related in the earliest days in only this way. Such is the way of things utterly bound to their power to change. Fire can burn bright and reach high at the expense of the wood, Fire is quenched by the Essential water completely. There is no room in their essence for anything but their consuming features. But there is something that only happens when the mixing of essential and mundane things occurs. In a world where there are things that are “non” essential, made of some other element beyond what these elements contain to consume one another.

As the world grows old, the fires bite dies and so rises a new order of things. A world where embers can rise from fire. Embers that rise higher and burn brighter than the Fire that consumed the wood. Embers of Wood in fire are made seemingly of both elements simultaneously. They embody the strengths of each of their parents.

In this world there is a natural order. A way of things that resembles this consuming relationship of these elements. But as time goes on, the greatest and most powerful of magics has either faded or sequestered itself away to parts of the world where its power can be undimmed.

The truth is that these five elements that we know as the bearers of nature are solely the domain of magic. Which is one of four elusive primordial elements. All the elements that we celebrate as the essence of the world and nature are borne of the sun as is all magic. This magic flows through the stars and into the land and our hearts. But people are not merely the domain of magic. And are in fact composed of four elements more fundamental than that, People are where all four of these primal essences mix and relate in the same consuming fashion that the 5 magical elements do, they each hold another in the palm of their hands. With complete dominion over one of them with the power to uplift or destroy what lies in their grasp, in the same way that fire can consume wood.

But an ember is something beyond all of these, For what is an ember but a beautiful analogy of the contradiction of this natural order. An Ember is what occurs when wood burns fire, When metal rusts the water. Such things are improbable but possible and precisely what characterizes some of the most intriguing and powerful people. Because people are the only ones capable of discovering and harnessing the power of the four primal cardinal elements. People are the brightest Embers.”

The Chronicle of Sedulous Ember
===============================

A powerful master of martial arts was among these siblings. He defended with ease his siblings when they faced physical danger. Rejoiced at the prospect of physical conflict and never shrunk from a chance to befriend or train under an opponent who he defeated or who defeated him. His excellency in the martial arts was forced upon him by times of conflict against the Whoevers. But like a diamond that is forged deep in the earth and cut to a brilliant diadem. So the skill of Tekua of Sedulous ember rose. He never claimed mastery and never pursued any form of recognition for his teachings or bravura, Save for the the pluck of his students whom he each cherished as a father does his own children. Tekua spent most of his days on the mats of his dojo. Teaching his craft with his own hands and leaving his unique fingerprint upon each of his students, each of whom bears this signature mark.

When thier master passed. each bearer of Tekua's Fingerprints created part of a kata that would serve as a bond to those who were true to their masters teachings. This kata is was a combative complex that required great skill to execute. The Fingerprint Kata and Mark to this day marks the authenticity of those who have learned from a true master.
    