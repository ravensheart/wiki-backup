---
title: Literature
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Academic
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Literature is the study of old writings, usually fiction but not always. It is the study of the art that has gone by.
    