---
title: In-Style Rolls
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Martial Arts Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

An in-style skill is a skill that is core to the use of a Martial Arts Style. Whenever any roll is called for by a Martial Arts Jutsu, the roll is considered In-Style. For all in-style rolls, the character adds his appropriate Virtue to the Attribute being used, and his Path Talent and Style Skill to the Talent and Skill named in the Jutsu. When a Form jutsu is active, all rolls on In-Style Skills are considered in-style rolls and thus benefit as above. Further, a Style Master always adds his [Chi Bonus](/legends/chi-bonus) to all In-Style Rolls.
    