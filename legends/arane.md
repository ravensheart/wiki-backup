---
title: Arane
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Dark God
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Arane
=====

The Spider Goddess, Arane was Lilith's familiar and loyal servant. She was the last of the Dark Gods to Ascend, as she spent much time in the mortal realm, experimenting on those who interested her.

### Consecration or Dedication

The Priest must kill and eat someone of their own species. The Priest must eat every part of the victim. Priests who have dedicated themselves primarily to Arane have stark white hair.

### Covenant

Always spend time to study an enemy before engaging.

### Attunement

Arane grants automatic success on all hunting (not combat; ambush yes) rolls and all Nephendus rolls.

### Gift

The gift of Arane is the magic of [Nephendus](/legends/nephendus), as well as a selection of Knacks that involve sucking the vitality of others.

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.
    