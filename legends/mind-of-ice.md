---
title: Mind of Ice
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Intellect Style, Melee Style, Method of Assimilation, Perception Style
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Mind of Ice is an armed Martial Art that uses a number of weapons commonly associated with fencing: Rapier, Saber, Smallsword, and Knife (Main-Gauche). Any character who has mastered all of these weapons may start learning the Jutsu of this style without further instruction. Mind of Ice is a Melee style that enhances Intellect and Wits. Students of this style frequently study Multi-Strike and Disabling Strike. Mind of Ice cannot be practiced in any armor that has an Encumbrance Penalty.

Mind of Ice Core Cascade
========================

Quick Nick of the Tip
---------------------

Quick Nick of the Tip

Cost

1 w

Type

Quick Persistent

Requirements

Method 1, Mind 1

Prerequisites

none

> You flash the tip of your weapon forward almost faster than the eye can follow, nicking your opponent where it will continue to bleed. Make a normal attack roll, but your post-Armor damage is reduced by 2 hit points. If you do less than 2 hit points of damage after Armor, this Jutsu fails and you do no damage to your opponent. If you succeed, your opponent bleeds for 1 point of fatigue every action they take until the end of the scene or they die; armor does not protect. If they are knocked unconscious, they continue to bleed out, losing hit points. Magical healing will close one bleeding wound, but only one per spell or effect, no matter how powerful. Wounds can also be bandaged, but doing so is a First Aid roll at a difficulty equal to your path rating. This Jutsu has, of course, no effect on creatures that have no blood.

Mind of Ice Form
----------------

Mind of Ice Form

Cost

\-

Type

Quick Form

Requirements

Method 2, Mind 2

Prerequisites

placeholder

> You raise the tip of your weapon, turning side-face to your opponent, light on your feet. While this Form is active, you may make a number of additional attacks with in-style weapons equal to your Path rating every time you act, even if your action did not include an attack with an in-style weapon. Additionally, every attack you make with an in-style weapon benefits from Quick Nip of the Tip. These additional attacks can benefit from any Artisan Melee Techniques you might wish to employ, including Multi-Strike, and the penalties for each attack are resolved as though you had made them on separate actions. In Form skills include Alacrity, Awareness, Melee and Fast Talk.

Breath of the Dragon Master Jutsu
=================================

Spirited
--------

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.
    