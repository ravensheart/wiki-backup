---
title: Body Language
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Investigator
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Body Language is the art of reading the intentions of others through the position and movement of their body; in some circles it is known as "cold reading".
    