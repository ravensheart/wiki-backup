---
title: Items
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  TableOfContents
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Armor](/legends/armor)
*   [Poisons](/legends/poisons)
*   [Weapons](/legends/weapons)
    