---
title: Circle of Gifts
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Pantheon
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Circle of Gifts
===============

Aeons ago, eight persons, known and the Motgo, perfected the spirit of community and ascended together, hand in hand, to assume the position of Dog constellation. Their ascension was timed perfectly as it patched the gap in heavens torn at the Celestial Wolf Pack's divorce and reassured Creation that there was still a constellation of loyalty and society eager to support peoples from every crevice across the uneasy Tapestry. Legend states that the Motgo, before ascension, were deep dwarves whose colony had slowly died around them millennia ago, leaving only eight to strain against the ever encroaching hunger of Void. Strain knit closeness and unity, however, and against the Great Hunger arose an unbreakable circle and a new pantheon through the recognition and utilization of gifts unique to each Motgo but essential to social integrity and survival. Masters of the Gifts Omnipresent, Ministers of Terrestrial Glory, Circle of Community, Gods of the Welcoming Commune, and countless other pompous titles have graced these ascended beings, but

Motgo Physician, Motgo Councellor
=================================

placeholder

### Consecration or Dedication

have 6 levels in each psychology, first aid, and esoteric medicine

### Covenant

give aid to all who seek it, offer aid freely to any who appear to need it

### Attunement

holy bonus on healing/medical skills

### Gift

gain access to Excision

### Atonement

placeholder

Motgo General, Motgo Statesman
==============================

placeholder

### Consecration or Dedication

One year of civil service.

### Covenant

placeholder

### Attunement

Holy bonus to all skills governed by Tactician Calling (Leadership. War, Drill, intelligence)

### Gift

True Divination

### Atonement

placeholder

Motgo Executioner, Motgo Gardener, Motgo Assassin
=================================================

placeholder

### Consecration or Dedication

Find someone who needs to die, kill them. Justify their death and understand all positive and negative ramifications of their death out some number of years, minimum 1.

### Covenant

never take life of Person without understanding ramifications

### Attunement

holy bonus to convince others you must complete your mission and bonus to avoid detection when on mission

### Gift

gain access to Silent Road: Thousand Voice Silence

### Atonement

placeholder

Motgo Heir, Motgo Eccentric
===========================

placeholder

### Consecration or Dedication

Use highest die pool only in service of others for 1 year

### Covenant

Only use holy bonus in service to others

### Attunement

Holy bonus to highest die pool. if tie, all tied pools gain bonus

### Gift

Gem Mastery

### Atonement

placeholder

Motgo Jester, Motgo Soldier, Motgo Fool
=======================================

placeholder

### Consecration or Dedication

1 year mandatory military service

### Covenant

placeholder

### Attunement

Holy bonus to Charisma

### Gift

access to Heart of Stone

### Atonement

placeholder

Motgo Hermit, Motgo Monk
========================

placeholder

### Consecration or Dedication

spend 6mo in solitude, speak only to those who initiate the connection, vegetarian diet, chastity, no mild-altering substances (classic ascetic behaviors). Then, spend 6mo in the city with the same restrictions. Buy off all Temptation Fatal Flaws in this time

### Covenant

gain no further Fatal Flaws (Temptation), live life of modesty

### Attunement

holy bonus against Temptation Ordeals

### Gift

Oden Knacks

### Atonement

placeholder

Motgo Maven, Motgo Magi
=======================

placeholder

### Consecration or Dedication

have 2 levels in 8 different fine arts

### Covenant

placeholder

### Attunement

holy bonus to long-term planning

### Gift

Connection Magic

### Atonement

placeholder

Motgo Caretaker, Motgo Staward, Motgo Parent
============================================

placeholder

### Consecration or Dedication

1 year as caretaker (of population of persons or individual person)

### Covenant

instruct others in self-sufficiency

### Attunement

Holy bonus applied to immediate roll to benefit another (not over time, not abstract, not kill orcs to save village but rather parry orc attack against child).

### Gift

Can crucible in Steward, Combat, Natural Athlete, and Academic Callings.

### Atonement

placeholder

Related to Circle of Gifts
==========================
    