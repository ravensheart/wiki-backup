---
title: Core
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  ethereal, Ethereal, magic arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Core magic [opposes](/legends/opposing-magics) [World Pulse](/legends/world-pulse).

*   [Draconic Magic](/legends/draconic-magic)
*   [High Magic](/legends/high-magic)
*   [Low Magic](/legends/low-magic)
*   [Wizard Magic](/legends/wizard-magic)
    