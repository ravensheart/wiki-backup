---
title: The Book of Niktoul
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Dark Gods, Holy Texts, The Tome of Five Shadows
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

There once was a stranger. Nobody knew his name, or his face. His history can never be known. His desires are hidden in shadow. Those that stand against him die. Those who aid him are given gifts. They call him Niktoul.
    