---
title: Broker
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Enhancement Art, Love
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Money is a lie, but interest is not, and the right kind of mind can figure out how to charge interest on things that really matter. In order to be a Broker, the character must have at least one level in Love and some skill in Broker. Brokering is an Enhancement Art.

A Broker loans character points. The total number of character points a Broker has loaned out cannot exceed the product of his Po, Love, and Skill in Broker. In order to loan someone character points, the Broker must loan them some form of currency, with the understanding that the other person needs to pay it back… the Broker cannot be deceptive on this point, or else the contract is invalid.

With the currency, the Broker also gives the other character any number of character points, up to the limit of his ability to loan, or the actual number of character points he currently has free. The Broker decides how these points are spent when his mark accepts the deal.

Thereafter, every month, the mark has to pay the Broker one character point for every 5 he was loaned. This is automatic, and happens without the mark’s consent and, usually, without his knowledge. The missing character points can become lowered attributes, new Fatal Flaws or Debilities, lost Knacks, anything the GM feels is appropriate. This continues to happen indefinitely, until the Broker is paid back.

In order to pay back the Broker, the mark has to find the original currency he was loaned and return it, personally, physically, to the Broker, as well as paying off all of the loaned character points. He must have the full value of the loan, both in currency and in unspent character points, in order to actually pay off the loan. Anything less does not work.

Beings reduced to a total character point value of 0 or less become Wretches, and gain total and absolute loyalty to the Broker. They are, metaphysically, his property. He no longer gains character points from them, and gets his original investment back, but that person is now a tool for the Broker, and will follow all of his instruction without hesitation or question. Usually, at this point, a Broker uses some other Art to make his Wretches useful again.
    