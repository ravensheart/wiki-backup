---
title: Terrible Hunger
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Greatfolk Knacks, Twisted Folk Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

**Terrible Hunger**  
Cost: 16 experience  
Twisted Folk are notorious in many ways. One of those ways is that they are known is that they are known to eat the flesh of others. A character with this Knack may consume the flesh of a person, and that flesh will sustain them for a full day for every level that person had in Khat. Characters with this knack never get sick from eating the flesh of people. Freshness does not matter, though a corpse that has decayed to the point at which it has no flesh cannot be eaten.
    