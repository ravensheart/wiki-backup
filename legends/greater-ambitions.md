---
title: Greater Ambitions
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Characterizations
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Greater Ambitions are things that the character aspires to; something that involves personal growth or changing the world. Something about the world that is not true that the character wants to see become true. Greater Ambitions never involve individuals other than the character himself; desire to kill someone is never a Greater Ambition, even if that person is a king and you want a more just society. Then your Greater Ambition could be “Make the society more just.”, and killing the king in an incidental. A character can have no more Greater Ambitions than his Hun.
    