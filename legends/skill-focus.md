---
title: Skill Focus
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Connoisseur Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Skill Focus (Varies, -8):_ As above, but each of the 256 skills is its own Technique. Success on this roll allows the Connoisseur to know EXACTLY what he just witnessed. He may read the full Spell, Jutsu, Knack, or Technique description. He knows the exact ratings of all relevant traits used by the individual who is responsible for what he is observing. This can function as an Identify spell for magic items or Artifacts, or something similar for Rude Arts… any object examined with the correct Skill Focus reveals all of its secrets, including use, maintenance, and destruction. Some Arts allow the creator to conceal such things; bypassing such concealment requires a contested roll of the Connoisseur’s Skill Focus in the concealing skill against the creator’s concealing skill.
    