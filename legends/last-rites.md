---
title: Last Rites
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Occult Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Last Rites (Charisma, -2)_: You know how to put the dead to rest. Success on this technique allows you to weaken the Po of the deceased by your Cunning Folk talent level, usually preventing it from coming back and haunting the land.
    