---
title: The Voice of Iwo
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Concepts
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The mingling of Destiny and the Illuminated Path manifests itself as the voice of the great being that is known as [Iwo](/legends/iwo)
    