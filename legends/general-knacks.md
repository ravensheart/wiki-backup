---
title: General Knacks
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Character Creation
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Assertion
---------

Cost

8 experience

Prerequisites

Alma 4 or Po 4

> Sometimes, the world gets in the way of what is right, and instead of bowing, you have to assert yourself. This Knack allows the character to spend a point of Certainty in order to add a Virtue to a die roll. This requires that the die roll be in line with the Virtue; one cannot Assert Fury when attempting a First Aid roll. The dice added by this Knack stack with all other dice and successes, including the general bonus to attributes enjoyed by Style Masters. One cannot Assert a Virtue in order to deceive; rolls based on Rude Arts skills, or using Po attributes, cannot be subjected to Assertion. However, a character who has this Knack and the Dark Urges can Assert those Urges with such skills… but they cannot pair such Urges with Khat attributes or any Fine Arts skills other than Combat. A character with Dark Urges can qualify for this Knack based on his Po, rather than his Alma… but he still spends Certainty to activate it.

Attractive
----------

Cost

4 experience

Prerequisites

Khat 2+

> You are physically attractive. You reduce the SDR of anyone you engage in social combat by 1 per level of this Knack, which can be purchased once for every two full levels of Khat you have. Against those who have a Fatal Flaw relating to being attracted to people like you, their SDR is reduced by 2 for every level of this Knack.

Luck
----

Cost

4 experience

Prerequisites

Alma 2+

> Fortune seems to favor some people. For every level of this knack, the character can either reduce any die-pool penalty he is experiencing by 2 or reduce the difficulty of a roll by 1, to a minimum penalty of -0 and a minimum difficulty of 1. This applies to all rolls, but won’t help if there is no difficulty or penalties associated with the roll. This also does not function against active opposition; technically, the opponent’s Dodge, Armor, Parry, and SRD all act in a manner similar to a difficulty on a roll, but none of these can be reduced by Luck. Levels in Luck can help the character pull off hard Techniques that he has not yet learned completely, but he still cannot attempt techniques whose base penalties are higher than his skill, and Luck doesn’t help with avoiding Spellshear. It can help mitigate penalties that accrue due to the use of magic, however, such as those suffered by Linguists and Spellmasters. Luck cannot be taken more than once for every two points of the character’s Alma.

Resolve
-------

Cost

4 experience

Prerequisites

Hun 2+

> When you want to get something done, you can get it done. You can, on any roll, spend a Willpower to gain a number of automatic successes equal to the number of times you have taken this Knack. You cannot take this knack more than once for every two full levels of Hun you have.

Wealth
------

Cost

4 experience

Prerequisites

Po 2+

> You have money. How do you have money? It doesn't matter, and really, it isn’t anybody’s business but your own. The point is, that you always have at least a little pocket change, or a few valuable knicknacks, or even just your name to lean on. Regardless of how it is explained in play (the specifics of which can contribute to a Stunt for various rolls involving money), you never need to worry about purchases below a certain threshold. The first purchase of this knack allows you to simply pay for any expense less than 1 silver. You have some extra coin on you, or you sign an IOU and pay it off later, or whatever. Regardless, the purchase does not impact the actual amount of money recorded on your character sheet, ever. You can do this a number of times per scene equal to your Convolution; this is a soft limit, and the GM can allow you to do it more than that if he feels you aren’t attempting to exploit the Knack. This Knack can be purchased more than once, up to once for every two levels of Po you have. Each purchase increases by a factor of 10 the amount of money you can handwave. With two purchases, you can buy up to ten silver worth of stuff without worrying. Three, and a gold piece. Four, and you can spend ten gold. Five purchases and you can spend a platinum without worrying, and can make any number of silver piece purchases in a scene. Six, and you can spend ten platinum without marking it down, and make any number of purchases at 10 silver or less per scene without censure. And so on.
    