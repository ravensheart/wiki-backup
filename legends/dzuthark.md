---
title: Dzuthark
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Motgo
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Motgo Heir, Motgo Eccentric
===========================

placeholder

### Consecration or Dedication

Use highest die pool only in service of others for 1 year

### Covenant

Only use holy bonus in service to others

### Attunement

Holy bonus to highest die pool. if tie, all tied pools gain bonus

### Gift

Gem Mastery

### Atonement

placeholder
    