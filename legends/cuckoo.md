---
title: Cuckoo
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Intellect Style, Karate Style, Melee Style, The Black Spiral, Thrown Style, Will Style
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Default

Cuckoo Core Cascade
===================

Cuckoo Master Jutsu
===================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.

Reservoir of Will
-----------------

Cost

\-

Type

Stackable Permanent

Requirements

Master of a Will Style

Prerequisites

none

> Your Will has greater depths than most men. Add one to your effective Hun and Will for the purposes of calculating Willpower, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Rating; Mana Reservoir and Reservoir of Will do not stack.
    