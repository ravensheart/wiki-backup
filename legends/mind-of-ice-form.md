---
title: Mind of Ice Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Mind of Ice Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Mind of Ice Form

Cost

\-

Type

Quick Form

Requirements

Method 2, Mind 2

Prerequisites

placeholder

> You raise the tip of your weapon, turning side-face to your opponent, light on your feet. While this Form is active, you may make a number of additional attacks with in-style weapons equal to your Path rating every time you act, even if your action did not include an attack with an in-style weapon. Additionally, every attack you make with an in-style weapon benefits from Quick Nip of the Tip. These additional attacks can benefit from any Artisan Melee Techniques you might wish to employ, including Multi-Strike, and the penalties for each attack are resolved as though you had made them on separate actions. In Form skills include Alacrity, Awareness, Melee and Fast Talk.
    