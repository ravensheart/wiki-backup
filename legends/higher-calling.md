---
title: Higher Calling
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Characterizations
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Higher Callings are deeply held moral beliefs or vows that the character sticks to as a matter of principle. They are not things that are ever done, but rather constant and continual habits that shape his life. They are never specific to a person or a place, but rather represent universal ideals that the character upholds. Vows of Poverty, Vows against using Bladed Weapons, Vows of Chastity, and the like all are Higher Callings. Higher Callings can become [Shido](/legends/shido) or [Covenants](/legends/covenants).
    