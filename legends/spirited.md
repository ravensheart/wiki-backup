---
title: Spirited
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Style Master Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.
    