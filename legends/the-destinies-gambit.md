---
title: The Destinies Gambit
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Concepts, Lore
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Gods take possession of a soul and guard it zealously, or haphazardly depending on thier magnitude. The [Hun](/legends/soul) and [Alma](/legends/soul) of the great servants of heaven are traded in what some scholars claim to be a game of chance and wagers in heaven held in heaven on the full moon.

The final score, or the outcome of this game in heaven for the souls of people is known as Destiny.
    