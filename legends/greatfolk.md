---
title: Greatfolk
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cyclopses
=========

Hecaconshere
============

Ogres
=====

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.

Cost

16 experience

Prerequisites

> Some races are tough to kill. Each level in this Knacks adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Khat.

Cost

16 experience

Prerequisites

> Some creatures are just tough. The character gains a natural AR of 1. This increases by 1 for every extra time this Knack is purchased, no more times than his Khat.

General Greatfolk Knacks
========================

**Biggun**  
Cost: 8 experience  
Some creatures just grow big. Each level of this knack reduces the training time needed to increase the character’s Khat by half. This Knack can be taken multiple times, up to once per level of the character’s Hun.

**Guardians of This World...And All Others**  
Cost: 8 Experience  
The Greatfolk have walked between the gardens for lengths of time unknowable, Oaths that were sworn in a strange place now lost to memory are remembered in the bloodlines of the guardians of the place that the greatfolk (and all the children of those who swore this oath) once came from. Each level of this knack grants the character an automatic success to defend against influence that would debilitate or hinder them in the lands that lie between gardens.

Cost

16 experience

Prerequisites

> Even the most serious injury, if it does not kill you, can be overcome. Any Crippling injury heals once all of the Hit Point loss associated with its infliction is healed. Amputations require more potent solutions.

**Terrible Hunger**  
Cost: 16 experience  
Twisted Folk are notorious in many ways. One of those ways is that they are known is that they are known to eat the flesh of others. A character with this Knack may consume the flesh of a person, and that flesh will sustain them for a full day for every level that person had in Khat. Characters with this knack never get sick from eating the flesh of people. Freshness does not matter, though a corpse that has decayed to the point at which it has no flesh cannot be eaten.

Cost

8 Experience

Prerequisites

> Some creatures are just better at resisting the Void than others. Each purchase of this Knack gives the character an extra die for the purpose of resisting Void effects, including most poisons and toxins and all Rude Arts. It also subtracts one from the die pools of any Rude Artist attempting to target the character directly with his arts. This can be purchased multiple times, but no more than the character’s Khat. Its effects stack.
    