---
title: Ordeal Powered
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Ordeal:_ The Virtuous can, with some forms of magic, turn their virtue into power. Ordeal based magics provoke a Virtue roll, with the amount of energy provided equal to twice the difficulty of the roll. The magic in question will specify which virtue must be rolled. Failure on this roll always has significant, permanent consequences for the character, as specified in the particular magic.
    