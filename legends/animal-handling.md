---
title: Animal Handling
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Beastmaster
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

This skill represents your ability to tame and train animals, as well as deal with wild animals who have no training.
    