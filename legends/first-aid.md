---
title: First Aid
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Steward
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of providing simple medical aid to those in need.
    