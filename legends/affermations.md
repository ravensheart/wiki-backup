---
title: Affermations
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Sometimes, life gives you just what you need. An Affirmation refreshes your belief in yourself and your place in the world. This can happen due to your own actions, if the GM rules that you have acted particularly in accordance with your virtue. It can also happen due to the use of Fine Arts, and frequently does.

An Affirmation removes one mark against a virtue, or adds one mark in favor of a virtue. For characters with Dark Urges, an Affirmation can remove a mark in favor of an Urge, or add a mark against an Urge.
    