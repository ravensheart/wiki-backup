---
title: Encumbrance and Mobility
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Armor usually has some Mobility Penalty. Further, a character who is carrying too much can be encumbered. A character can carry up to Strength + Potence kg without penalty. After that, for every full multiple of the above, the character suffers a -1 Encumbrance Penalty. Mobility Penalty and Encumbrance Penalty stack and subtract directly from all Agility + Alacrity static values. Twice the total penalty is also subtracted from the character’s Agility + Alacrity die pool. Characters with an Encumbrance Penalty greater than their Health + Resistance static value lose one fatigue per hour of activity from exhaustion.
    