---
title: Brutal
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Daikini Knacks, Lizard Man Knacks, Ogre Knacks, Orc Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.
    