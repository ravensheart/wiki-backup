---
title: Fine Arts (n)
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Glossary
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The Fine Arts are the Arts of the Tapestry. They are more "mundane" than other arts, but no less powerful. Fine Arts normally use techniques, and so Artisans (those who practice Fine Arts) are frequently very specialized. The Embodiment of Fine Arts is a Titan.
    