---
title: Elves
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Dark Elves
==========

Dark Elves frequently have either [The Twisted Virtues](/legends/the-twisted-virtues) or the [Virtues of the Dark Gods](/legends/virtues-of-the-dark-gods).

Dark Elf Knacks
---------------

Sturdy
------

Cost

16 experience

Prerequisites

> Some races are tough to kill. Each level in this Knacks adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Khat.

* * *

Sky Elves
=========

Sky Elves frequently have the [Illuminated Virtues](/legends/illuminated-virtues).

* * *

Wood Elves
==========

Nearly all Wood Elves have [The Taoist Virtues](/legends/the-taoist-virtues).

* * *

General Elf Knacks
==================

Starlight Vision
----------------

Cost

4 experience

Prerequisites

> You are at home in dim light, such as that provided by the stars or a dim torch. You can see five times as far using a single light source as a normal human, and see by starlight just as well as a human does in full daylight. A character with both this Knack and Home in Darkness suffers no penalties is full daylight.
    