---
title: Glossary
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  TableOfContents
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

**Artisan (n)**

> An artisan is a practitioner of the Fine Arts.

**Fine Arts (n)**

> The Fine Arts are the Arts of the Tapestry. They are more "mundane" than other arts, but no less powerful. Fine Arts normally use techniques, and so Artisans (those who practice Fine Arts) are frequently very specialized. The Embodiment of Fine Arts is a Titan.

**Jiffy (n)**

> A Jiffy is 15 trice, somewhere between 15 and 75 seconds. It is the standard measure of time in social or mass combat.

**Jutsu (n)**

> A Jutsu is a well-defined series of actions that results in some supernatural effect. Most Jutsu are in Martial Arts, but some are also in Rude Arts. All Jutsu have some requirement for skill and talent, and most also have other Jutsu that one must learn first before progressing. A set of Jutsu that must be learned in a specific order are known as a Jutsu Cascade. All Martial Arts Styles have a Primary Jutsu Cascade, which is a list of Jutsu that must be mastered before the Style is mastered.

**Talent (n)**

> A talent is a 3 quatrine trait. It governs 4 skills, and there are 4 talents to a sphere. Every Cardinaltiy has 16 talents. Talents in Magic Arts are called Magery, Talents in Fine Arts are called Callings, Talents in Rude Arts are called Carriers, and Talents in Martial Arts are called Paths.

**Trice (n)**

> A trice is 1 to 5 seconds, and is the standard measure of time in combat. There are 15 trice to a jiffy.
    