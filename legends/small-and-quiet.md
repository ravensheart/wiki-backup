---
title: Small and Quiet
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Goblin Knacks, Kobald Knacks, Nelwin Knacks, Racial Knacks, Wildling Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

16 Experience

Prerequisites

> The smaller races have a knack for hiding in the shadows. Each level of this Knack gives the character an automatic success on all rolls to stay hidden or to prevent someone from tracking them. It can be taken no more times than the character’s Po.
    