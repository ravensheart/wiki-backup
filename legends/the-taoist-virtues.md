---
title: The Taoist Virtues
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Virtue System
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

**De**

De represents the character's dedication to doing what is right and to not harming those he comes across without good reason. A character can always channel De when attempting to bring Creatures of Darkness to justice, or to re-integrate them into creation. He can also channel De when defending the weak or punishing those who prey on people weaker than themselves. A high De character must fail a De roll in order to take advantage of another for personal gain, to abuse or allow someone else to abuse the weak, to engage in mindless destruction or slaughter, or to stand by while injustice prevails. De resists Fear.

**Piti**

Piti is the character's ability to enjoy life. It is his existential joy in being, the immortal nectar that flows from his heart and into the world at the wonder of it all. The character can always channel Piti when he is having fun. This virtue resists Despair.

**Pu**

Pu is one's connection to the Tao, his understanding of Wu Wei. A character can always channel Pu when attempting to know when the right moment is to act, when attempting to accomplish an action with the minimal amount of effort, and when trying to discern the best course of action in a complex situation or follow their intuition to discern the best action for all. This includes all Prayer rolls for guidance. A high Pu character must fail a virtue roll in order to act rashly or against his better judgment. Pu is used to resist Callousness.

**Sati**

This is the character's freedom from distraction and separation from material desires. Sati can always be channeled to resist those who would bribe, cajole or threaten the character away from the way. A high Sati character must fail a virtue roll in order to spend time pursuing material gain, extremism or petty pursuits. A high Sati character must fail a virtue roll in order to totally abstain from sex as much as he must fail a virtue roll in order to spend a week doing nothing but. Balance is the key of Sati. Sati is used to resist Temptation.
    