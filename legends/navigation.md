---
title: Navigation
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Explorer
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is your ability to navigate unknown lands using just your knowledge of nature and ability to remember landmarks.
    