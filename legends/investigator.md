---
title: Investigator
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Enlightenment
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Archeology](/legends/archeology)
*   [Body Language](/legends/body-language)
*   [Investigation](/legends/investigation)
    