---
title: The Cult of the Dark Gods
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Pantheon
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Cult of the Dark Gods
=====================

All Dark Gods require a Dedication. This is a ritual that dedicates the Priest’s body, soul and flesh to the god in question. The particulars of the Dedication vary, but after completing the specified rituals, the Priest must then bathe in specially prepared oils that require at least a skill of 8 in the Dark God magic skill to prepare. After the first bath, the Priest’s hair color changes to match his primary Oath, however subsequent Dedications for the same Priest do not change their hair color. Note that each of these Dedications require the Priest to kill someone. He cannot double-up, or use any of these deaths to power any sort of other ritual. Each Dedication requires a fresh sacrifice. As normal, a Priest cannot Dedicate himself to more gods than his Alma.

Arane
=====

The Spider Goddess, Arane was Lilith's familiar and loyal servant. She was the last of the Dark Gods to Ascend, as she spent much time in the mortal realm, experimenting on those who interested her.

### Consecration or Dedication

The Priest must kill and eat someone of their own species. The Priest must eat every part of the victim. Priests who have dedicated themselves primarily to Arane have stark white hair.

### Covenant

Always spend time to study an enemy before engaging.

### Attunement

Arane grants automatic success on all hunting (not combat; ambush yes) rolls and all Nephendus rolls.

### Gift

The gift of Arane is the magic of [Nephendus](/legends/nephendus), as well as a selection of Knacks that involve sucking the vitality of others.

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.

Brutoth
=======

Brutal general of the Dark Gods, and lover to Lilith.

### Consecration or Dedication

The Dedication of Brutoth is to beat a person to death with your bare hands. You must kill them using no supernatural traits, and using only non-lethal damage. Those who dedicate themselves to Brutoth first gain dark red hair.

### Covenant

Brutoth does not have patience for those who cast themselves as victims. Any who lay down and die rather than fight back deserve their fate, and get no sympathy or aid from Brutoth or his followers. It is a violation of his Covenant to aid those who do nothing in their own defense.

### Attunement

Brutoth adds his Unholy Bonus to all combat actions and tactical evaluations. This only functions in actual combat; Brutoth does not give bonuses to training rolls.

### Gift

Brutoth gives the gift of Excision.

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.

Lilith
======

The dark-haired defiant mistress of darkness, Lilith is the first Dark God.

### Consecration or Dedication

The Dedication of Lilith is to find an oathbreaker and to ritually kill them. Oathbreakers who have repented directly to Lilith do not qualify. The blood of the victim is used in the ritual bath, and the Dedication of Lilith leaves the priest with dark blue hair if it is their first.

### Covenant

Lilith's Covenant is to never break one's oath. Even the most casual word must be held sacred, and the breaking of any other Covenant is also considered the breaking of Lilith's Covenant; both must be atoned for.

### Attunement

Servants of Lilith get their holy bonus on all social rolls where the other party can see them or hear their voice and may be interested in sex with the character (ie is attracted to their gender and species).

### Gift

Lilith grants her followers access to Herb Mastery, as well as a small number of Knacks. One basic trait is that the character no longer acquires scars, and all scars they previously had heal.

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.

Niktoul
=======

The dark one, the stranger. The Assassin.

### Consecration or Dedication

To each penitent is whispered a name. To each a task. To each, a life to be taken, in service to the agenda of Niktoul. The life must be taken in such a way that the penitent cannot be held accountable for it as a crime. Those who Dedicate to Niktoul gain grey hair after they bathe in the oils derived from the blood of their sacrifice.

### Covenant

Niktoul admonishes that one must never underestimate the danger of an enemy, and so must never leave one alive willingly.

### Attunement

Niktoul grants his Unholy Bonus to all rolls to acquire and maintain social stealth and disguise.

### Gift

Niktoul grants the power of Pearl Mastery.

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.

Ogmar
=====

Ogmar is the scholar of the Dark Gods. He values all knowledge, even that which is hidden or forbidden. Ogmar seeks to understand everything, even that which would spell his own doom. One of the strangest gods, Ogmar regularly ventures into the Void to seek hidden or lost understanding.

### Consecration or Dedication

The Dedication of Ogmar involves the crafting of a copy of the Tome of Five Shadows. This requires that the penitent kill a Person and use their body in an arcane ritual. Completion of this ritual requires Rituals of the Dark Gods at 16 and Dark God Investiture at 4, and results in a [Tome of Five Shadows](/legends/the-tome-of-five-shadows) crafted from the flesh of the victim. This book is enchanted to be quite durable; it has an AR of 10 and 100 hit points, and heals 1 hit point per day. The penitent must then gift this book to someone who seeks the knowledge therein. Afterwards, the penitent must bathe in an oil prepared from the blood of the sacrifice. This oil turns the penitent's hair pitch black.

### Covenant

The Covenant of Ogmar is to never destroy knowledge, which includes the knowledge of skillful individuals and the knowledge stored in books.

### Attunement

Ogmar grants the priest an Unholy bonus to all spellcasting checks and all Intellect checks.

### Gift

The Gifts of Ogmar include access to several Knacks that are useful to spellcasters, as well as access to [Arcane Mysteries](/legends/arcane-mysteries).

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.

Related to Cult of the Dark Gods
================================

*   [The Book of Aranea](/legends/the-book-of-aranea)
*   [The Book of Brutoth](/legends/the-book-of-brutoth)
*   [The Book of Lilith](/legends/the-book-of-lilith)
*   [The Book of Niktoul](/legends/the-book-of-niktoul)
*   [The Book of Ogmar](/legends/the-book-of-ogmar)
*   [The Tome of Five Shadows](/legends/the-tome-of-five-shadows)
*   [Virtues of the Dark Gods](/legends/virtues-of-the-dark-gods)
    