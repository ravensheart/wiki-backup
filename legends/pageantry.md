---
title: Pageantry
description: 
published: true
date: 2021-06-09T22:05:22.583Z
tags: legends,  card game
editor: markdown
dateCreated: 2021-06-09T21:10:08.872Z
---

History
=======

Pageantry is a card game of considerable length depending upon which circles it is played in. Dating back to time immemorial. It is a game where one plays as some royal household defending their family from any number of opponents over many seasons. The five seasons are played through in each variant and each variant has its own rules for win conditions and what cards are permitted. It would behoove those who come to a table with an elf playing pageantry to know what rules his household upholds for pageantry.

Variants and Game Composition
=============================

Proper games of Pageantry are played where each person has their own deck of cards, Human variants include a normal deck of 4 suits, 2 red and 2 black. 4 of each Ace through King and two Jokers, low and high.

Human variants of Pageantry are played where each person has their own deck of cards or the deck can be split for a much shorter game into two decks of 26. Human variants include a normal deck of 4 suits, 2 red and 2 black. 4 of each Ace through King and two Jokers, low and high.

Simple Elven variants of Pageantry are played with a deck of cards whose total value cannot exceed 469 points (The sum of the number of points in a normal 54 card deck with two jokers), and persist for as many seasons as there are cards that still remain on the table.

Card Point Values
-----------------

The Low Joker is worth as much as a Jack a Queen and a King (36 points)

The High Joker is worth as much as a Low Joker and an Ace. (51 points)

The Ace is worth 15 points

The King is worth 13 points

The Queen is worth 12 points

The Jack is worth 11 points

All numbered cards are worth their displayed value.

Beginning the Game
==================

Making the Court
----------------

After shuffling each deck to the opponents satisfaction

Each player takes the top card off their deck and places it face down in the middle of the table simultaneously until the first 3 face cards are drawn. This will serve as the players court and it determines the size of the battlefield (and in the human variant) how many cards are in your hand.

Determining the Battlefield
---------------------------

A King gives you 3 Battlefronts, +1 Cards to your hand.

A Queen gives you 2 Battlefronts, +2 Cards to your hand.

A Jack gives you 1 Battlefront, +3 cards to your hand.

A normal court consists of a Jack, Queen and a King which places 6 Battlefronts between you and your opponent and 7 cards in your hand. if you have fewer cards in your deck because of a different court than you gain the benefit of having more battlefronts. Any number of battlefronts more than six removes the number of cards in your hand granted by your court. Lower value face cards give cards to your hand in exchange for battlefronts.

For example a Court of Three Jacks would give you three battlefronts and 11 cards in your hand. A court of two queens and a king would give you 5 cards in your hand and 7 battlefronts.

Playing through the Seasons
===========================

In Human Pageantry, there are five rounds of play after which each player assesses their position on the board. based upon number of battlefronts available and cards in their hand. Elven variants continue until one player has won the entirety of their opponents deck and court.

One person will be decided to go first, for elves it is usually the younger person, for human variants is usually tossed.

Each player takes turns playing the cards in their hand against the cards on the battlegrounds of their opponent. They may also play their cards on their own battlegrounds to reinforce them. After each player is out of cards. Each player chooses a battle to resolve. The winner of the battle is decided by who simply has a higher value of cards on that battlefront. The winner keeps all the cards from the victory and shuffles them back into his deck. If the battle was won on friendly soil, the player chooses a single card from the battle to remain on the battlefront. If the losing player loses the battlefront in enemy territory. They lose the battlefront and all the cards they played in the fight to their opponent.

This process continues through five rounds in the human variant, each time a hand is depleted signals the end of a "season."

In the human variant of the game, cards are reshuffled and game continues unto a new year with the same court but with fresh battlefronts assigned refreshed with forces from the new year.

Winning
=======

All battlegrounds must be exhausted until their are less than 3 in front of a players court - at which point the defending characters court is in jeopardy. The offending character may choose to attack the court directly. If a season ends with the death of a member of the court, the game ends and the player who struck the blow against their opponents court wins the game. In the human variant of the game, cards are reshuffled and game continues unto a new year with the same court but with fresh battlefronts assigned

Elven variants continue until all cards have been gathered by a single player.
    