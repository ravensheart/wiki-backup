---
title: Timeless Body
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Assimilation Path Master Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Timeless Body
-------------

Cost

\-

Type

Permanent

Requirements

none

Prerequisites

Master Assimilation Path

> You no longer age against your will. You are totally immune to all Crippling effects based on aging your body. Your apparent age can drift at the same speed people normally age, but can go in either direction and never causes you any inconvenience beyond social inconvenience. You don’t have conscious control of this; your body ages towards whatever your current self-image is.
    