---
title: Warp Powered
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Warp:_ Warp magics are powerful and dangerous. A character accumulates Warp whenever they cast a spell using Warp magic. They can safely handle an amount of Warp equal to their Warp Edge; anything less than that and they suffer no consequences. Unlike Fatigue, Willpower and Hit Points, Warp recovers only slowly; a character reduces his Warp by his Recovery once per day, at a time determined by the kind of magic he knows. There are several other rules connected to Warp; see the section dedicated to discussing this topic.
    