---
title: Area Knowledge
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Cultivator
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This skill represents your knowledge about a specific area of the world.
    