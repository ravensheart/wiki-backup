---
title: Perception
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Khat Attributes
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Perception is being able to tell when a tomato is rotten.
    