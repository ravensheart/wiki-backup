---
title: Theology
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Academic
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Theology is the Academic study of the gods and their ways, as well as religion and the practices of worship.

_Religion Focus (varies, -4):_ Specific questions about particular religions require that the character have an understanding of this technique. Since there are nearly countless religions, there are countless variations of this technique.
    