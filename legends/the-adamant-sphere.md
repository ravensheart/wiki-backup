---
title: The Adamant Sphere
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Martial Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

*   [The Flesh of the World Tree](/legends/the-flesh-of-the-world-tree)
*   [The Heart of the World](/legends/the-heart-of-the-world)
*   [The Method of Assimilation](/legends/the-method-of-assimilation)
*   [The Verdant Way](/legends/the-verdant-way)
    