---
title: Craft: Crystal
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Artificier
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The Craft of Crystal is the craft that moves people. Roads, walls, churches and town halls. Castles and signs. Any durable item that influences the movement of people passively is Craft: Crystal.
    