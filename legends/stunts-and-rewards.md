---
title: Stunts and Rewards
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Whenever an action is dramatically important, a character can Stunt. By definition, this is always and only when the GM or rules explicitly call for the player to make a roll; if the GM says you don’t need to roll, you can’t stunt. A Stunt is a description of the actions your character takes to make them more interesting and cool. Stunts are explicitly not about what would work better, but what kind of dramatic flair your character puts on the action. Any description beyond “I hit him with my sword” or “I study the book” should garner at least one stunt die; decent descriptions should get 2, and amazing descriptions that the player acts out should get 3. NPCs can stunt, but the GM has to describe their actions in at least as much detail as he would require of a player. In addition to adding dice to a roll, Stunts also give the character an opportunity to recover some of their resources. Whenever the GM grants Stunt dice, the player can choose to recover a total number of Willpower, Hit Points, Fatigue, and Certainty equal to the number of dice rewarded. These rewards can be mixed and matched; he can recover 2 Willpower and one Fatigue, for example. Injuries don’t magically disappear; the character still looks injured, but they have “fought through the pain” or whatever, allowing them to continue fighting longer. On a 3 die stunt, the player can choose to instead gain an experience.
    