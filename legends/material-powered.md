---
title: Material Powered
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Material:_ Material magics take costly and rare materials and use them to produce magical effects. In general, every 50 copper worth of appropriate materials is equivalent to 1 energy.
    