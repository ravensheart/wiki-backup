---
title: Quick Nick of the Tip
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Mind of Ice Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Quick Nick of the Tip

Cost

1 w

Type

Quick Persistent

Requirements

Method 1, Mind 1

Prerequisites

none

> You flash the tip of your weapon forward almost faster than the eye can follow, nicking your opponent where it will continue to bleed. Make a normal attack roll, but your post-Armor damage is reduced by 2 hit points. If you do less than 2 hit points of damage after Armor, this Jutsu fails and you do no damage to your opponent. If you succeed, your opponent bleeds for 1 point of fatigue every action they take until the end of the scene or they die; armor does not protect. If they are knocked unconscious, they continue to bleed out, losing hit points. Magical healing will close one bleeding wound, but only one per spell or effect, no matter how powerful. Wounds can also be bandaged, but doing so is a First Aid roll at a difficulty equal to your path rating. This Jutsu has, of course, no effect on creatures that have no blood.
    