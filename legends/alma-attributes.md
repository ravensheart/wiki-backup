---
title: Alma Attributes
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Attributes, Virtues
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Alma Attributes are known as Virtues. There are a number of Virtue systems, and a few Urge systems, which are almost functionally identical. Virtues help the character resist Emotion Ordeals, and can be very useful to Martial Artists.

*   [Council of Embers Virtues](/legends/council-of-embers-virtues)
*   [The Taoist Virtues](/legends/the-taoist-virtues)
*   [Virtues of the Dark Gods](/legends/virtues-of-the-dark-gods)
    