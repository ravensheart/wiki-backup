---
title: Jutsu (n)
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Glossary
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

A Jutsu is a well-defined series of actions that results in some supernatural effect. Most Jutsu are in Martial Arts, but some are also in Rude Arts. All Jutsu have some requirement for skill and talent, and most also have other Jutsu that one must learn first before progressing. A set of Jutsu that must be learned in a specific order are known as a Jutsu Cascade. All Martial Arts Styles have a Primary Jutsu Cascade, which is a list of Jutsu that must be mastered before the Style is mastered.
    