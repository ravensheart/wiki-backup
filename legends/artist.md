---
title: Artist
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Creation
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Jeweling](/legends/jeweling)
*   [Musical Composition](/legends/musical-composition)
*   [Painting](/legends/painting)
*   [Sculpting](/legends/sculpting)
    