---
title: Scream of a Thousand Voices
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Thousand Voiced Silence Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Scream of a Thousand Voices
---------------------------

Cost

1 w

Type

Free Instant

Requirements

Road 1, 1k Silence 1

Prerequisites

[Binding Parry](/legends/binding-parry)

> Your enemies attempt to change your way, but you cannot hear them. This Jutsu can only be used in response to a social attack, and must be used before the character knows what the content of that attack will be. Instead of listening to the voice of his attacker, he opens himself up to the voices of the gods. Their screams and whispers, solicitations and condemnations fill your mind for a brief moment, drowning out any other attempt at influence. This Jutsu perfectly defends against the social attack, and may, at the GM's discretion, also give the character insight into the future or the [Destiny](/legends/destiny) of those around him.
    