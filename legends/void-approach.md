---
title: Void Approach
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Philosophy Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Void Approach (Intellect, -8)_: You have studied the Void approach to philosophy. You may use this technique to analyze something from the perspective of the Void Beyond, a Thing, or the inevitable decay and destruction of all things.
    