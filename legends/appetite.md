---
title: Appetite
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Dark God Virtue, Hun Virtue, Virtue Resists Despair
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Appetite is the Hun virtue of the Dark Gods. It represents the character's desire for growth and power. It can also represent the character's actual hunger, especially for those who have [Nephendus](/legends/nephendus). Appetite resists Despair.
    