---
title: Martial Arts
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The Primordial Martial Arts skill is [Brawling](/legends/brawling).

*   [The Adamant Sphere](/legends/the-adamant-sphere)
*   [The Celestium Sphere](/legends/the-celestium-sphere)
*   [The Mythril Sphere](/legends/the-mythril-sphere)
*   [The Onyx Sphere](/legends/the-onyx-sphere)
    