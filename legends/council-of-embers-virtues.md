---
title: Council of Embers Virtues
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Virtue System
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Abrigation
==========

Jog on!

Masks
=====

Everyone in my head agrees.

Strife
======

Life is pain. No pain, no gain.

Vanity
======

I feel pretty, and witty, and bright!
    