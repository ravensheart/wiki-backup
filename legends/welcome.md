---
title: Welcome
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  welcome
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Hey guys!
---------

Here are some resources for how to work with the wiki.

Main website for the wiki platform.

*   [https://tiddlywiki.com/](https-//tiddlywiki.com/)

Also, these youtube videos are super helpful

*   [https://www.youtube.com/watch?v=ZMGpAW0z\_Bo&list=PLzZCajspPU\_UjFn0uy-J9URz0LP4zhxRK](https-//www.youtube.com/watch?v=zmgpaw0z_bo&list=plzzcajsppu_ujfn0uy-j9urz0lp4zhxrk)

And of course hit me up on discord and I can help. I hope this is helpful and everyone enjoys the new wiki.
    