---
title: Rude Arts
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The primordial Rude Arts skill is [Serialist](/legends/serialist)

*   [Education](/legends/education)
*   [Eugenics](/legends/eugenics)
*   [Industry](/legends/industry)
*   [Politics](/legends/politics)
    