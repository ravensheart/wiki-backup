---
title: Marksman
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Combat
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Marksman is the skill that governs attacks with weapons that use projectiles and do not use Magic or Rude Arts. This includes bows, crossbows, slings, and the like.
    