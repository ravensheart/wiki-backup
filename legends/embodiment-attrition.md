---
title: Embodiment Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Suit Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Some decks have four additional cards: The True Dragon, The Titan, The Immortal, and The Anathema. These serve as ultimate trump cards, always winning a Challenge except when facing one another. The True Dragon wins against the Titan, who wins against the Anathema, who wins against the Immortal, who wins against the True Dragon. Should a cross-pair show up in a single Challenge, it is known as a Reckoning, and all cards in the Challenge are discarded, and the winner of the previous Challenge initiates again.
    