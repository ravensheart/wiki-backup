---
title: Diligence
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dark God Virtue, Khat Virtue, Virtue Resists Temptation
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Diligence is the Khat virtue of the Dark Gods. It represents the character's dedication of purpose and ability to set themselves to a task completely. It resists Temptation.
    