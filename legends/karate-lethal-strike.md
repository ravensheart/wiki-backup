---
title: Karate Lethal Strike
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Karate Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Lethal Strike (Agility, -6)_: Normally, unarmed attacks only do non-lethal damage. With the proper training, however, a skilled unarmed combatant can do real damage with their bare hands.
    