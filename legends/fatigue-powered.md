---
title: Fatigue Powered
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Fatigue:_ Some magics also function off of Fatigue, but such magics are usually inefficient. Fatigue only provides 1 energy per point.
    