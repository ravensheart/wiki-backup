---
title: Attributes and Souls
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Each character has four “Souls”, which function a lot like Essence in Exalted. They are the Hun, Khat, Po, and Alma. Each soul governs four Attributes and sixteen Talents. Each Talent has four Skills, and some Skill have further Specialties.

The attributes are:

*   Hun: Intellect, Wits, Charisma, Will
*   Khat: Strength, Agility, Health, Perception
*   Po: Manipulation, Guile, Convolution, Defiance
*   Alma: see Virtues and Urges

Attributes are limited to 4 or the rating of the governing soul, whichever is higher. Talent levels are limited to the rating of the governing soul. Skills in Po, Khat, or Alma are limited to 4x Khat or the governing soul, whichever is higher. Skills in Hun are limited to 4x Hun. A character has a number of Hit Points equal to his Khat times his Health, and amount of Willpower equal to his Hun times his Will, an amount of Fatigue equal to his Defiance times his Po, and an amount of Certainty equal to his Khat Virtue times his Alma.

Every morning, after a full night’s sleep, a character may roll his Hun Virtue and recover the successes in Willpower, and his Alma Virtue and recover his successes in Certainty. A character who rests for a full day, with plenty of food and water, can roll his Health and, on a success, recover a Hit Point. With proper medical care, he can recover one Hit Point per success. For every hour of rest, a character can roll his Health and recover the successes in Fatigue.
    