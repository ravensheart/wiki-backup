---
title: Religion Focus
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Theology Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Religion Focus (varies, -4):_ Specific questions about particular religions require that the character have an understanding of this technique. Since there are nearly countless religions, there are countless variations of this technique.
    