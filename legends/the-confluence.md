---
title: The Confluence
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Concepts
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Confluence is the natural phenomenon that relates the four cardinal elements of the setting. Light, Tapestry, Crystal and Void each have a role to play in the Plan of Iwo. Fundamentally the four elements of the universe interact as:

Light producing potential and embodying possibility, Tapestry receiving the change and cherishing and developing it, Void removed and cleansing the waste and useless, Crystal embodying the notion of perfection and flawlessness.

These four cardinalities reinforce and connect the world together when they work in this manner. If there roles are reversed, or if they are used to undermine one another they produce nothing.

Crystal can be used to focus Light, harnessing the panoply of pure possibility into form, It is in this manner that the Gods speak their will and guide the weather.

Light can be used to breathe diversity and potential into Tapestry that cherishes and nurtures it to life. It is in this manner that the oceans and forests of the worlds teem with life birthed from the Sun.

Tapestry can be used to bring grounding and purpose to those who must use the Void to do what is right. It is in this manner that those who must pursue true justice must act.

Void can be used in turn to ensure that even the most selfless and pure of Crystals still turn an eye to their own needs, It is in this manner that people may be relentless in their pursuit of virtue yet retain their identity.

A bearer of the powers that resides within one of the cardinalities holds in their hands the ability to either reinforce, or subvert the very essence of reality, Rendering the world ever stronger, or ever weaker, for everyone around them.

Crystal can be used to destroy the potential and possibility of Light, bringing cold Fate. Light can be used to corrupt and strip away the resilience of the Tapestry, bringing Chaos. Tapestry can be used to strip away the Void's ability to cull things from the world resulting in Glut and Excess. Void can be used to shred and undermine the resilience of Crystal, Making the world Soulless.

This relationship is the harmony that exists in nearly every facet of the universe.
    