---
title: Dreaming
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Mystic
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Dreaming is the art of consciously controlling your dreams and nightmares. At high levels, it can be used to control the dreams of others.
    