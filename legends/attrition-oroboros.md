---
title: Attrition Oroboros
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Play Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

In this variant, the Snake isn't the weakest card all the time, and it is known as Oroboros. Within the same suit, Oroboros beats Ox, Tiger and Dragon (11, 12, and 13; Jack, Queen, and King). Unless one of those cards are in play, Oroboros is still beaten by all other cards. However, if one of those other cards (of the same suit) are in the Challenge, then The Round card beats them and can only be beaten by a Cat, Ferret, or Rooster. For example, if an Oroboros, a Monkey (7), and a Hores (9) were in play, the Horse would win. If the next player played an Ox (11), the Oroboros would be winning. If the next player played a Rooster, that would then be the winning card (assuming everything is in suit).  
When this variant is used with the Face Point Value, frequently the Oroboros is worth more than the 1 point that it would be normally. Common values are 10, 13 and 14 points. It is frequently called the "little dragon" in such games.
    