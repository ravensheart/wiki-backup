---
title: Research
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Academic
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of finding the correct information when given access to a sea of text. It is your ability to find the relevant knowledge when given more knowledge than you can fully assimilate.
    