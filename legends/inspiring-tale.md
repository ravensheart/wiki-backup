---
title: Inspiring Tale
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Storytelling Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Inspiring Tale (Charisma, -2 or more):_ You tell a tale of inspiring deeds. On a successful roll, those who listen to your tale (and consent) gain a +1 to the attribute(s) of your choice. This technique is at a -2 per +1 granted, and the total bonus to all attributes cannot exceed your level in the Performer Calling. Inspiring Tales cannot improve Hun attributes. Bonuses can increase a character's attributes beyond their normal cap, but cannot increase them beyond twice their cap. All bonuses fade at the end of the scene.
    