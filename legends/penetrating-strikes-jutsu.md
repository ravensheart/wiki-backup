---
title: Penetrating Strikes Jutsu
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Strength Style Master Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Penetrating Strikes
-------------------

Cost

\-

Type

Stackable Permanent

Requirements

Strength Style Master

Prerequisites

none

> Your fists and style weapons bite deep into the flesh of your enemies. You ignore one level of Armor your opponent has per level in this Jutsu when making attacks with in-style weapons. You can have multiple levels of this Jutsu, but no more than your talent level in the Path the style belongs to.
    