---
title: Meheliz
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Motgo
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Motgo Caretaker, Motgo Staward, Motgo Parent
============================================

placeholder

### Consecration or Dedication

1 year as caretaker (of population of persons or individual person)

### Covenant

instruct others in self-sufficiency

### Attunement

Holy bonus applied to immediate roll to benefit another (not over time, not abstract, not kill orcs to save village but rather parry orc attack against child).

### Gift

Can crucible in Steward, Combat, Natural Athlete, and Academic Callings.

### Atonement

placeholder
    