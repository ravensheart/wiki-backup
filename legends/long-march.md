---
title: Long March
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Drill Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Long March (Health, -2)_: You have learned to endure long marches without arriving tired. This technique can be used instead of the character's Health + Resistance pool to endure carrying heavy weights during long marches.
    