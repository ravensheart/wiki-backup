---
title: The Chronicle of Clockwork Embers
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Diary of Embers
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Chronicle of Clockwork Ember
================================

There Once was a little girl who thought the world a pitiable mess. She grew up in a small village of 25, where each of her mothers and father possessed talents for the arts and the great crafts of the realm. She never thought she could paint like her sister, or teach like her middle brother. Or speak as well as her eldest brother, but she never failed to find a solution to every little problem that fell in her way.

When she was 10 she constructed a little sawmill that made timber faster than all the lumberers in town, When she was 15 she made a machine that could soar through the sky and mastered its every quivering tremble like an animal. And when she was 23 years of age, a rival nation, the Whoevers came down from the mountainside near the edge of the island where her siblings dwelt. Her two brothers and sisters fought valiantly but for her part Qellektria knew that the weapons of the enemy were too great and numerous to fight. So she took it upon herself to bolster the defenses with great machines of war, and various inventions that could slay men. She took no pleasure in it, and lost many nights of sleep to the screams of dying soldier outside her home. But she would build her engines of destruction and terror with efficiency, with the hope that their effectiveness would strike awe into the hearts of her enemies. And lo it was so…that the ember of Clockwork burned bright to light the destiny of Qellektria and her siblings. And so once the Whoevers were defeated Qellektria was disquiet with with the grief of so many lives lost. So she kept her defenses active and the guns never left the walls of her city. As a quiet reminder of the fury of her intellect, and the depths of her love for her family and home.
    