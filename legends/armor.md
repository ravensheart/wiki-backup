---
title: Armor
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Items
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Armor has three relevant traits: Cost, Armor Rating (AR), and Mobility Penalty (MP). Cost is how much the armor costs, in copper. Armor Rating is how much protection it offers; the number of successes on a damage roll that are subtracted before applying the damage. The Mobility Penalty applies to the character’s Dodge pool, as well as to actions that require agility, like stealth or climbing. Mobility Penalty does not usually apply to Parry pools, unless specified otherwise in some Martial Art.

Mundane armor comes in three weights: Light, Medium and Heavy. Light Armor has an AR of 1, Medium Armor has an AR of 2, and Heavy Armor has an AR of 3. For all mundane armors, MP = AR, but cost can vary. Mundane armor can be improved by various supernatural skills, but doing so makes it no longer Mundane.
    