---
title: Taoist Approach
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosophy Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Taoist Approach (Intellect, -10)_: You have studied the Taoist approach to philosophy. The synthesis and conjoining of all things to unify in a beautiful ballet of harmony and purpose. You may use this technique to understand [The Way](/legends/the-way), and to understand those who follow the [Taoist Virtues](/legends/taoist-virtues).
    