---
title: Investigation
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Investigator
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of finding the truth of a mystery by looking at the facts. It is also the skill of understanding another person and finding what they are trying to hide.
    