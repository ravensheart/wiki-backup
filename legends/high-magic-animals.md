---
title: High Magic Animals
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  High Magic Animals
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

13: Dragon  
12: Tiger  
11: Ox  
10: Allicorn  
9: Horse  
8: Wild Boar  
7: Monkey  
6: Crane  
5: Dog  
4: Rooster  
3: Ferret  
2: Cat  
1: Snake
    