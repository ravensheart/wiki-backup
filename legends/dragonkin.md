---
title: Dragonkin
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Dragonkin includes all direct descendants of True Dragons. They are all lizard-like in appearance, many of them have wings, and only some of them qualify as People. Only two Dragonkin races are playable. Much of the lore in this document is not widely known.

The child of two True Dragons is always a Greater Dragon. Greater Dragons are born with a rating of 32 in all four souls; they are powerful. They are also large, and when young frequently require a lot of food.

The child of two Greater Dragons is only a Greater Dragon if at least one of its grandparents was a True Dragon. Otherwise, it is a Lesser Dragon. Lesser Dragons are born with an 16 in all four souls, and are ferocious and dangerous, though not as much so as their Greater kin. The child of two Lesser Dragons is a Lesser Dragon only if there is a Greater Dragon in four generations (at least one of its Great-Great-Grandparents was a Greater Dragon). Otherwise, it is a Drake. Drakes are born with a 8 in all four souls, and are still quite potent.

And so it goes, with the number of stable generations doubling with each new kind of Dragonkin… Drakes beget Wyverns, Wyverns beget Lizard Men, Lizard Men beget Thunder Lizards, and Thunder Lizards beget Kobalds. Kobalds are stable, and may breed indefinitely without further degrading; they are as degraded as Dragonkin get. Wyverns and Thunder Lizards are beasts and not People, and how their children can be people is a deep mystery, but Lizard Men and Kobalds are both People.

It is worth noting that any Dragonkin can breed with any other Dragonkin, but the children are always of the lesser breed; a Greater Dragon who breeds with a Wyvern will sire Wyvern, every time.

Most Dragonkin have the virtues of the Dragon Gods

Dragons
=======

Dragon Knacks are only available to True Dragons, Greater Dragons, Lesser Dragons, and Drakes.

Dragon Knacks
-------------

* * *

Kobalds
=======

Kobalds are short, mean, lizard-like people who are considered vermin in most places.

Kobald Knacks
-------------

Light Fingers
-------------

Cost

8 Experience

Prerequisites

none

> Some smaller races are adept at sleight-of-hand and mundane trickery. Each purchase of this Knack adds one to the difficulty of all Perception rolls to notice the Koobald performing an illegal or nefarious act in plain sight: slipping poison into someone’s drink, filching an apple from a fruit stall, etc. This Knack can be purchased more than once, but no more times than the character’s Po.

Sharp Nose
----------

Cost

4 experience

Prerequisites

none

> Some species have a sense of smell that a bloodhound would envy. Each level of this Knack grants the character an automatic success on all smell and taste related Perception checks, and can be taken up to as many times as the character’s Perception.

Small and Quiet
---------------

Cost

16 Experience

Prerequisites

> The smaller races have a knack for hiding in the shadows. Each level of this Knack gives the character an automatic success on all rolls to stay hidden or to prevent someone from tracking them. It can be taken no more times than the character’s Po.

* * *

Lizard Men
==========

The big, burly cousin of the Kobald, Lizard Men are big and mean.

Lizard Man Knacks
-----------------

Brutal
------

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.

Lizard Tail Recovery
--------------------

Cost

16 experience

Prerequisites

> Even the most serious injury, if it does not kill you, can be overcome. Any Crippling injury heals once all of the Hit Point loss associated with its infliction is healed. Amputations require more potent solutions.

Tough
-----

Cost

16 experience

Prerequisites

> Some creatures are just tough. The character gains a natural AR of 1. This increases by 1 for every extra time this Knack is purchased, no more times than his Khat.

* * *

General Dragonkin Knacks
========================

Starlight Vision
----------------

Cost

4 experience

Prerequisites

> You are at home in dim light, such as that provided by the stars or a dim torch. You can see five times as far using a single light source as a normal human, and see by starlight just as well as a human does in full daylight. A character with both this Knack and Home in Darkness suffers no penalties is full daylight.
    