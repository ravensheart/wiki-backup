---
title: Talent Focus
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Connoisseur Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Talent Focus (Varies, -4):_ As above, but the focus is far more narrow: each of the 64 Talents has its own Technique for Connoisseur. Success on this roll gives the Connoisseur a very good description of the use of the thing, though not particular numbers or prerequisites. It tells him exactly which Skill was used to make it, as well as when and with what intent. It also gives him information of the object’s or act’s history, or the history of similar objects or acts. 5 or more successes gives some insight into the mind of the being that made it, as well, including whether any Virtues or Characterizations came into play when they were doing so.
    