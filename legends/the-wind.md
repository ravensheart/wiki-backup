---
title: The Wind
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Agility Style, Health Style, The Path of Fate, Thrown Style
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Fate connects all things, and a student of this style studies how his weapon might connect with his foe. The Wind is a thrown style that can use nearly anything as a weapon at higher levels. To begin, the practitioner usually practices with throwing knives or shuriken. Unlike must Martial Arts styles, The Wind can only be used with physical throwing weapons; it cannot be used armed, or in close combat.

The Wind Core Cascade
=====================

Conspiracy of Meaningless Fates
-------------------------------

Cost

1 w

Type

Quick Instant

Requirements

Fate 1, The Wind 1

Prerequisites

none

> The first thing to learn is how to turn anything into a weapon. This Jutsu allows the character to make a normal Thrown attack with any item he has on hand that is light enough to lift with one hand. This normal Agility + Thrown attack has the normal range for all such attacks, but suffers no weapon familiarity penalty for what item the character is using. The item has at least an Accuracy bonus of +0 and a damage bonus of at least +2 nonlethal or +1 lethal.

Blade Seeks Blood Understanding
-------------------------------

Cost

2+ w

Type

Quick Stackable Instant

Requirements

[Conspiracy of Meaningless Fates](/legends/conspiracy-of-meaningless-fates)

Prerequisites

Fate 1, Wind 2

> The blood of mortals calls to its ultimate fate, yearning to be freed from the fragile shell containing it. Your blade is merely the path to enable this fate. This Jutsu represents an attack with a normal thrown weapon. This attack does double damage after armor for 2 willpower. Every additional 2 willpower adds one to this multiplier, though the multiplier cannot be higher than the Martial Artist's Path of Fate Talent level + 1. This stacks multiplicitively with the Vital Strike Thrown technique.

Disabusement of Ephemeral Prophylactics
---------------------------------------

Cost

2+w

Type

Quick Stackable Instant

Requirements

[Conspiracy of Meaningless Fates](/legends/conspiracy-of-meaningless-fates)

Prerequisites

Fate 1, Wind 2

> The protection that armor provides is an illusion; no mere metal or skin can protect one from fate. This Jutsu allows the Martial Artist to ignore one level of his opponent's AR for every 2 willpower spent, up to his Path level in AR.

The Wind Master Jutsu
=====================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.

Sturdy
------

Cost

\-

Type

Stackable Permanent

Requirements

Master of a Health Style

Prerequisites

none

> Some kinds of training make you very tough. Each level in this Jutsu adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Path Talent. This Jutsu does not stack with the Knack of the same name.
    