---
title: Dark Elves
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Elves
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Dark Elves frequently have either [The Twisted Virtues](/legends/the-twisted-virtues) or the [Virtues of the Dark Gods](/legends/virtues-of-the-dark-gods).

Dark Elf Knacks
---------------

Sturdy
------

Cost

16 experience

Prerequisites

> Some races are tough to kill. Each level in this Knacks adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Khat.
    