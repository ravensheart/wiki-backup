---
title: Exhale and Strike
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Breath Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Exhale and Strike
-----------------

Cost

1 w

Type

Quick Instant

Requirements

Breath 1, Fate 1

Prerequisites

none

> In the space between one breath and the next, you strike. This Jutsu allows you to make a single attack before the first action in combat. You must activate this Jutsu as you roll Initiative. Doing so allows you to make a single attack with a style weapon before anyone else gets to act. Enemy defensive values do not apply to this attack so long as they are not using an effect with a Talent level higher than your level in Fate, or a Rude Arts effect. Using this Jutsu delays your first action in the combat by a number of Trice equal to the Time Cost of the Jutsu (3).
    