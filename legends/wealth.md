---
title: Wealth
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  General Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Cost

4 experience

Prerequisites

Po 2+

> You have money. How do you have money? It doesn't matter, and really, it isn’t anybody’s business but your own. The point is, that you always have at least a little pocket change, or a few valuable knicknacks, or even just your name to lean on. Regardless of how it is explained in play (the specifics of which can contribute to a Stunt for various rolls involving money), you never need to worry about purchases below a certain threshold. The first purchase of this knack allows you to simply pay for any expense less than 1 silver. You have some extra coin on you, or you sign an IOU and pay it off later, or whatever. Regardless, the purchase does not impact the actual amount of money recorded on your character sheet, ever. You can do this a number of times per scene equal to your Convolution; this is a soft limit, and the GM can allow you to do it more than that if he feels you aren’t attempting to exploit the Knack. This Knack can be purchased more than once, up to once for every two levels of Po you have. Each purchase increases by a factor of 10 the amount of money you can handwave. With two purchases, you can buy up to ten silver worth of stuff without worrying. Three, and a gold piece. Four, and you can spend ten gold. Five purchases and you can spend a platinum without worrying, and can make any number of silver piece purchases in a scene. Six, and you can spend ten platinum without marking it down, and make any number of purchases at 10 silver or less per scene without censure. And so on.
    