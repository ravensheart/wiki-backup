---
title: Short Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Deal Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

A quicker version, or one for many players, Short Attrition only involves dealing three cards to each player.
    