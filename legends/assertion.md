---
title: Assertion
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  General Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

8 experience

Prerequisites

Alma 4 or Po 4

> Sometimes, the world gets in the way of what is right, and instead of bowing, you have to assert yourself. This Knack allows the character to spend a point of Certainty in order to add a Virtue to a die roll. This requires that the die roll be in line with the Virtue; one cannot Assert Fury when attempting a First Aid roll. The dice added by this Knack stack with all other dice and successes, including the general bonus to attributes enjoyed by Style Masters. One cannot Assert a Virtue in order to deceive; rolls based on Rude Arts skills, or using Po attributes, cannot be subjected to Assertion. However, a character who has this Knack and the Dark Urges can Assert those Urges with such skills… but they cannot pair such Urges with Khat attributes or any Fine Arts skills other than Combat. A character with Dark Urges can qualify for this Knack based on his Po, rather than his Alma… but he still spends Certainty to activate it.
    