---
title: War
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Tactician
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The most practical skill of a Tactician, this skill represents actual tactical and strategic acumen, and your ability to make the best decisions with the information at hand.
    