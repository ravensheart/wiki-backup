---
title: Foolhardy
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Kender Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

16 experience

Prerequisites

> Kender are either foolishly brave or bravely foolish. Either way, fear just doesn’t seem to be an idea that they really understand. Each level in this Knack gives the character a bonus success to resist Fear. It can be taken multiple times, up to the character’s Alma, and stacks.
    