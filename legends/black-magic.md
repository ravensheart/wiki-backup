---
title: Black Magic
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Abyssal
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Black Magic [opposes](/legends/opposing-magics) [Fel Evocation](/legends/fel-evocation)

*   [Nephendus](/legends/nephendus)
    