---
title: Lizard Tail Recovery
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Greatfolk Knacks, Lizard Man Knacks, Troll Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

16 experience

Prerequisites

> Even the most serious injury, if it does not kill you, can be overcome. Any Crippling injury heals once all of the Hit Point loss associated with its infliction is healed. Amputations require more potent solutions.
    