---
title: The Steps of Mortality
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Celestium Sphere
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

As angels and demons look upon mortals, they see the strangeness of their love and their lives. The only way to understand them is to walk in their footsteps. The Steps of Mortality is automatically opened to any being who was once a Style master but became a non-mortal due to celestial magic, including being turned into a Divine Servitor.

The Steps of Mortality Master Jutsu
===================================

Timeless Body
-------------

Cost

\-

Type

Permanent

Requirements

none

Prerequisites

Master Assimilation Path

> You no longer age against your will. You are totally immune to all Crippling effects based on aging your body. Your apparent age can drift at the same speed people normally age, but can go in either direction and never causes you any inconvenience beyond social inconvenience. You don’t have conscious control of this; your body ages towards whatever your current self-image is.
    