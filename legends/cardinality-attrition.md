---
title: Cardinality Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Suit Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

In this Attrition variant, there are four suits, not five. Crystal trumps Light, Light trumps Tapestry, Tapestry trumps Void, and Void trumps Light. It is less common, but still widely played.
    