---
title: Iwo
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Concepts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Iwo is the name that some philosophers give to the collective conscience and thought patterns of every being and god in a given [Shattering](/legends/the-shattering).
    