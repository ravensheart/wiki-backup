---
title: Debate
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosopher
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill used for a reasoned debate with other people acting in good faith. It can be used under other circumstances, but it is less effective.
    