---
title: Hand Betting Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Gambling Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

In this Attrition variant, each player puts up and ante before the cards are dealt. Then another round of betting (similar to Poker) is done after the hands are dealt and everyone sees what cards they have. Players who fold simply discard their cards. The winner of the hand takes the pot.
    