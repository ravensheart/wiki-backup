---
title: Combat Meditation
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Meditation Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Combat Meditation (Will, -10)_: Normally, Meditation isn't usable in combat. However, this technique allows the character to use any other Meditation technique as a Free Action during combat.
    