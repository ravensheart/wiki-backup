---
title: Philosophy
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosopher
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Philosophy is the search for truth, and is practiced by those who seek real truth in the world.
    