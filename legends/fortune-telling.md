---
title: Fortune Telling
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Cunning Folk
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This skill governs the telling of fortunes using props and mood. Mundane uses of this skill involve educated guesses and cold-reading, but a true Artisan Fortune Teller can really see the future, to some degree.

_The Road Ahead (Perception, -8)_: You look into the future and see what paths need to be walked. This technique allows you to give yourself or some other cooperative subject a quest worth no more experience than your Calling level in Cunning Folk. You have no control over the exact nature of this quest other than its magnitude. In general, the quests granted by this technique are good in a Tapestrial sense.
    