---
title: Daikini
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Humans
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Daikini frequently have the [Old God Virtues](/legends/old-god-virtues)

**Biggun**  
Cost: 8 experience  
Some creatures just grow big. Each level of this knack reduces the training time needed to increase the character’s Khat by half. This Knack can be taken multiple times, up to once per level of the character’s Hun.

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.
    