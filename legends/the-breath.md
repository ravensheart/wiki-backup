---
title: The Breath
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Melee Style, Strength Style, The Path of Fate, Will Style
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

A man’s entire life, his whole existence, comes down to one Breath. One breath to determine life or death, one breath to shape fate, one breath… always, possibly, your last.

The Breath is an armed Martial Art based on Melee. Style weapons are all Broadswords, Two-Handed Swords and Shortswords.

The Breath Core Cascade
=======================

Exhale and Strike
-----------------

Cost

1 w

Type

Quick Instant

Requirements

Breath 1, Fate 1

Prerequisites

none

> In the space between one breath and the next, you strike. This Jutsu allows you to make a single attack before the first action in combat. You must activate this Jutsu as you roll Initiative. Doing so allows you to make a single attack with a style weapon before anyone else gets to act. Enemy defensive values do not apply to this attack so long as they are not using an effect with a Talent level higher than your level in Fate, or a Rude Arts effect. Using this Jutsu delays your first action in the combat by a number of Trice equal to the Time Cost of the Jutsu (3).

The Breath Master Jutsu
=======================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.

Penetrating Strikes
-------------------

Cost

\-

Type

Stackable Permanent

Requirements

Strength Style Master

Prerequisites

none

> Your fists and style weapons bite deep into the flesh of your enemies. You ignore one level of Armor your opponent has per level in this Jutsu when making attacks with in-style weapons. You can have multiple levels of this Jutsu, but no more than your talent level in the Path the style belongs to.

Reservoir of Will
-----------------

Cost

\-

Type

Stackable Permanent

Requirements

Master of a Will Style

Prerequisites

none

> Your Will has greater depths than most men. Add one to your effective Hun and Will for the purposes of calculating Willpower, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Rating; Mana Reservoir and Reservoir of Will do not stack.
    