---
title: Intellect
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Hun Attributes
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Intellect is knowing a tomato is a fruit.
    