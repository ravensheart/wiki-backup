---
title: Drill
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Tactician
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is your military discipline. It is your ability to stay composed in difficult situations, and your ability to teach others discipline.
    