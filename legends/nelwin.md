---
title: Nelwin
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Browen
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Nelwin have the virtues of the Court of Heaven much of the time, though a surprisingly large minority has [The Silent Virtues](/legends/the-silent-virtues).

Nelwin Knacks
-------------

Small and Quiet
---------------

Cost

16 Experience

Prerequisites

> The smaller races have a knack for hiding in the shadows. Each level of this Knack gives the character an automatic success on all rolls to stay hidden or to prevent someone from tracking them. It can be taken no more times than the character’s Po.

The Importance of Family
------------------------

Cost

16 experience

Prerequisites

> Nelwin find the people around them are the most important part of their lives. Whenever a Nelwin with this knack turns in 5 marks from a Deep Connection for experience, he gains 10 experience, rather than 5.

The Little Things
-----------------

Cost

16 experience

Prerequisites

> Other races care about grand things. Castles and armies and great magics. Nelwin love the little things. Afternoon tea. The breeze across their porch. Enjoying a drink and a song at the local tavern. It is these things that the Nelwin values, and it is these things that keep them going. The character gains an automatic success to resist Despair for every level in this Knack, and can have as many levels in this Knack as his Alma. He need only remember the taste of bread.
    