---
title: Tough
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Dwarf Knacks, Lizard Man Knacks, Ogre Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Cost

16 experience

Prerequisites

> Some creatures are just tough. The character gains a natural AR of 1. This increases by 1 for every extra time this Knack is purchased, no more times than his Khat.
    