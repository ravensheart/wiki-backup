---
title: Gods
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  TableOfContents
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Many beings are worshipped in Raven's Heart. The two kinds of beings we concern ourselves most with in this document are Gods and Elder Gods. These two are distinct because they are very different in nature. Elder Gods are Things; creatures from beyond the world, who tug at the world in madness or hatred, and are generally destructive. Lol'Cthoth is the most powerful and terrifying of these beings, but an uncountable number of them exist in the Void. For further details, see the descriptions below.

Gods, on the other hand, are shards of Iwo. They are great crystals that float in the heavens and soften the light of the Sun. There are two kinds of Gods, and many would like to think the line between them is a sharp distinction, but that is a lie.

In general, Gods of Light grant Divine Magic, demand no blood sacrifice for their Consecration, and employ Angels as Servitors. In general, Gods of Darkness grant Infernal Blood Magic, demand a blood sacrifice for their Dedication, and employ Demons as Servitors. Gods of Light tend to have brighter stars than Gods of Darkness, and tend to demand less dangerous forms of worship, in general. However, few of these differences are hard and fast rules. Any God can make any of the six kinds of servitors, and most employ some of each type, though it is true that most Gods also favor one kind over others. Stars shine brightly, or not, as a function of the number of followers and the number of souls in their heaven; Gods of Darkness tend to recruit fewer, more fanatical followers, and reward even fewer of their followers with eternal resplendence, and so they tend to shine less brightly in the sky. Most Gods only grant either Infernal or Divine Magic, though some Gods grant access to ways to get around this, and several pantheons have Gods that grant both. While it is true that nearly all Gods that demand a blood sacrifice for their Dedication grant Infernal Blood Magic, this is not a hard-and-fast rule, and it is possible that some obscure God demands blood but does not grant blood magic. The converse is certainly true; Father Sky demands no blood but one's own, but grants blood magic.

Pantheons
=========

*   [Circle of Gifts](/legends/circle-of-gifts)
*   [Council of Embers](/legends/council-of-embers)
*   [The Cult of the Dark Gods](/legends/the-cult-of-the-dark-gods)

Independent Gods
================

Elder Gods
==========
    