---
title: Empath
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Control
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Integrity](/legends/integrity)
*   [Psychology](/legends/psychology)
*   [Socialize](/legends/socialize)
    