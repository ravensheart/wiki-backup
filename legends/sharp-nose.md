---
title: Sharp Nose
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Gnome Knacks, Goblin Knacks, Kobald Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

4 experience

Prerequisites

none

> Some species have a sense of smell that a bloodhound would envy. Each level of this Knack grants the character an automatic success on all smell and taste related Perception checks, and can be taken up to as many times as the character’s Perception.
    