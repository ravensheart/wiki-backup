---
title: Eugenics
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Rude Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Beauty](/legends/beauty)
*   [Innocence](/legends/innocence)
*   [Love](/legends/love)
*   [Purity](/legends/purity)
    