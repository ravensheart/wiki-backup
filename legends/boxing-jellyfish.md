---
title: Boxing Jellyfish
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Charisma Style, Health Style, Karate Style, The Black Spiral
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Default

Boxing Jellyfish Core Cascade
=============================

Boxing Jellyfish Master Jutsu
=============================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.

Sturdy
------

Cost

\-

Type

Stackable Permanent

Requirements

Master of a Health Style

Prerequisites

none

> Some kinds of training make you very tough. Each level in this Jutsu adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Path Talent. This Jutsu does not stack with the Knack of the same name.
    