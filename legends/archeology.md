---
title: Archeology
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Investigator
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

This is the study of bygone civilizations by way of the artifacts they have left behind.
    