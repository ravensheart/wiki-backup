---
title: Connoisseur
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Primordial Skill
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Connoisseur is the skill of appreciation. It is the skill which allows you to appreciate everything, even the darkest or most terrible things. You can see beauty in murder, or in the craftsmanship of the necromancer who turned your dead wife into a zombie, or in the efficiency with which the Rude Artist’s Factory Golem swallows the wilds. Connoisseur is the beautiful tragic skill of seeing the skill in things. It is the Primordial Fine Art, and anyone can put points into it at any time; everyone is capable of appreciating the world, if they just pay attention.

The basic function of Connoisseur is to tell how well-crafted something is. Whether it be a gun, a murder scene, a sword, a spell, or a painting, the Connoisseur can see the marks of a master where others might miss it. A basic Connoisseur roll, without any techniques, can be used to tell, with varying precision, how many successes someone got when making the thing being observed. This is usually Perception based, but can sometimes be based on Convolution or Intellect based on context and amount of time the character has to savor the moment. Regardless, the GM should always make this roll.

If the Connoisseur gets more successes than the craftsman, then they know exactly how many successes the craftsman got when making his work. This is also, usually, disappointing for the Connoisseur… a mediocre effort, one might say. If the craftsman got more successes, then the Connoisseur knows that, as well as whether they got more or less than half again as many. However, if the craftsman gets more than twice as many successes than the Connoisseur, all the Connoisseur knows is that the work is fantastic; a work of genius, a thing of beauty. It follows then, that those with little skill in Connoisseur are easier to please.

If a craftsman was sandbagging and pretending to have less skill, their effort only succeeds if they actually get more than twice the successes on their Convolution or Manipulation + Skill roll than the Connoisseur gets on their roll. In that case, the craftsman can say how many successes he apparently got. Connoisseur rolls are normally free; it does not take an action to appreciate skill.

**Connoisseur Techniques:** As normal, a character cannot attempt a Technique if the total penalty exceeds their skill in Connoisseur.

_Fine Arts Connoisseur (Varies, -2):_ This is the study of the Fine Arts. A successful roll when analyzing a Fine Art gives more than the usual information… usually, just which sphere it came from, and the general purpose or intent behind the object or event.

_Hyoka (Varies, -2):_ This is the study of the Martial Arts. A successful roll when analyzing a Martial Art gives more than the usual information… usually, just which sphere it came from, and the general purpose or intent behind the object or event. Brawling is inscrutable beyond this point, simply revealing that it is Brawling.

_Skill Focus (Varies, -8):_ As above, but each of the 256 skills is its own Technique. Success on this roll allows the Connoisseur to know EXACTLY what he just witnessed. He may read the full Spell, Jutsu, Knack, or Technique description. He knows the exact ratings of all relevant traits used by the individual who is responsible for what he is observing. This can function as an Identify spell for magic items or Artifacts, or something similar for Rude Arts… any object examined with the correct Skill Focus reveals all of its secrets, including use, maintenance, and destruction. Some Arts allow the creator to conceal such things; bypassing such concealment requires a contested roll of the Connoisseur’s Skill Focus in the concealing skill against the creator’s concealing skill.

_Sphere Focus (Varies, -2):_ The Connoisseur has spent time focusing on a particular sphere within a particular art. Each of the 16 spheres is its own Technique, and the penalty for this Technique stacks with that of Cardinality Focus; both always apply if Sphere applies. Success on this technique gives the Connoisseur an idea of what talent governed the skill used to make the thing he observes, as well as some clue as to the utility and cost of the thing; how to operate a gun, or what kind of bonus (generally) an Enchanted sword gives, what sort of energy a spell used, that kind of thing.

_Talent Focus (Varies, -4):_ As above, but the focus is far more narrow: each of the 64 Talents has its own Technique for Connoisseur. Success on this roll gives the Connoisseur a very good description of the use of the thing, though not particular numbers or prerequisites. It tells him exactly which Skill was used to make it, as well as when and with what intent. It also gives him information of the object’s or act’s history, or the history of similar objects or acts. 5 or more successes gives some insight into the mind of the being that made it, as well, including whether any Virtues or Characterizations came into play when they were doing so.

_Thanotology (Varies, -1):_ This is the study of the Rude Arts. A successful roll when analyzing a Rude Art gives more than the usual information… usually, just which sphere it came from, and the general purpose or intent behind the object or event one exception: the work of a Serialist reveals its secrets as though the Connoisseur had Skill Focus, below, without the need of further specialization

_Thaumotology (Varies, -4):_ This is the study of magic. A successful roll when analyzing magic gives more than the usual information… usually, just which sphere it came from, and the general purpose or intent behind the object or event. The Primordial Skills are inscrutable beyond this point, simply revealing that it was Wyld Magic.
    