---
title: Fishing
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Cultivator
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of catching fish in the wild, whether with a pole or spear.
    