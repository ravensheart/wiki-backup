---
title: The Forbidden Tongue
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Void Magic, Work In Progress
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Forbidden Tongue Charisma Based Warp Powered Syntactic Magic and used by the Things Beyond. All Speakers, as they are known, have a base Edge of 10 and a base Discharge of 24. All variables are handled by increasing cost.

Healing using the Forbidden Tongue is not always perfect. Anyone targeted by a Speaker’s healing effect that is not from the Void must roll Health + Resistance. A failure means that they gain a number of points in physical disadvantages equal to the number of hit points healed plus their margin of failure. The GM assigns these to thematically appropriate disadvantages, then compensates the character by adding appropriately-unsettling advantages in proportion. Thus, all comes to serve the Void. Any being with more total points from such disadvantages than their total point value becomes a creature of the Void, and must be healed with appropriate Void magic, rather than normal healing spells. This magic continues to alter them until the Void (the GM) is satisfied with their form. Enchanting with the Void is simple: calculate the total cost of the desired effect, multiply by 100 and that is the total cost of the enchantment. Speakers who are not also Maniacs usually enchant using Slow and Sure methods, but those who know both styles of magic, when they bother to enchant anything, usually use Quick and Dirty.

In addition to the normal methods, Speakers may reduce their Tally by consuming the minds of other sapient beings. The other being must be intelligent (IQ 5 or better) and either unconscious or willing. The speaker touches the head (or wherever the seat of consciousness is) of the being and releases the energies of the Void into them. He may discharge up to 5 points per second in this way. Each point discharged consumes a single point of mental advantage or mental attribute in the other being, chosen by the Speaker. Magery is always a valid target, but Power Investiture is not. Mental skills are also valid. The Speaker cannot leave any advantage, attribute or skill with a non-valid number of points invested (5 points in a skill, say, or 15 in Intelligence); he must consume the trait totally. However, he can do so over several actions. If he should be interrupted while this is occurring, however, the trait he is attempting to consume snaps back to the next largest value, and the Tally snaps back into the Speaker’s pool, causing an immediate Calamity check. For example, should a Speaker sneak into a child’s room and spend seven rounds consuming their Intelligence before being interrupted by a guard, the first point of Intelligence he consumed would be gone forever, but the second point that he had only partially consumed would immediately snap back, and he would gain those 15 points of Tally back and need to immediately make a Calamity check. Traits consumed in this manner are gone forever.

Create cannot be used with the natural elements (Fire, Metal, Wood, Water and Earth), while Destroy cannot be used with the elements from beyond (Ooze, Chitin, Smog, Balefire, Mollusc and, of course, Void).

Word Meaning Cost Time Morkuth Control 4 1 Destroy 2 0 Transform 3 3 Scry 1 5 Move 3 0 Strengthen 2 2 Create 6 4

Vulgaroh Mind 2 1 Magic 4 2 Ooze 1 1 Chitin 1 1 Smog 1 1 Balefire 1 0 Mollusc 1 1 Void 3 2 Fire 2 1 Metal 3 2 Wood 4 1 Water 3 1 Earth 2 2
    