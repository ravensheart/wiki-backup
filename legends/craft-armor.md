---
title: Craft Armor
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Craft: Tapestry Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Craft Armor (Intellect, -2 to -6)_: This technique allows the character to make mundane armor. The difficulty of the technique is -2 per AR of the armor. One roll per hour can be made at a difficulty equal to the AR of the armor. Five times that in successes is required to make a suit, but successes after difficulty are multiplied by the characters Calling level in Artificier (or 1/2, if he has no levels in that calling).
    