---
title: Intelligence
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Tactician
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is your ability to analyze information garnered about an adversary and determine important information about that adversary's weaknesses, goals, strengths, and assets.
    