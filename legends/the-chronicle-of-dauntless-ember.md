---
title: The Chronicle of Dauntless Ember
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Diary of Embers
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Chronicle of Dauntless Ember
================================

Some say it’s like a disease...Like a poison of the blood that you just can't get out. The need for danger, the hunger for more speed, The need for more danger like a drug.

Telia never went a day in her life without her blood slamming hard against the walls of her heart.

Truly I think Talia never thought of where she was or why she did what she did. The only thoughts she had for anyone were her four brothers and sisters who grounded her and cared for her. She was a screeching tyrant of daring, hopelessly impulsive and needlessly risky.

As she grew older her thirst for danger increased, Flying into hurricanes with flimsily constructed mock ups of her eldest sisters designs. Hanging or leaping from cliff sides with gadgets hastily fashioned from her sisters stolen designs.

When the Whoevers arrived at the shores and the need was dire, it was Talia who vanished into the night to meet them and without fail found herself impossibly outnumbered by adversaries, or tasked with rescuing countless civilians from a cliff face, Or even single handedly steering an unstoppable siege engine of the enemy into the sea before it violently exploded. No one ever asked for these tasks, Her siblings merely braced themselves for the ultimate sacrifice right before Talia would swoop in with salvation in the form of some impossible stunt.

She wished for a scream in her teeth. a roar of hellish intensity made loud upon her silent visage. A phantom of terror cast in her eyes and a face as a Dauntless Ember. Proof that she had been taken by some unearthly thing. All wound up into the simple smirk on her face as she flung herself headlong into the howling wind.

To seek Talia of Dauntless Embers is to court such an elemental mistress, as power, terror and ecstasy itself. You will hear her whispers at the edge of cliffs, at the depths of the oceans,racing through the meadows. free falling through the sky.
    