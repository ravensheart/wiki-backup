---
title: Resistance
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Natural Athlete
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of surviving difficult circumstances. It aids in resisting poisons and diseases, as well as magic that affects the body.
    