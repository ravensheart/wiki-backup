---
title: Work With Mythril
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Craft: Tapestry Techniques, Craft: Void Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Work With Mythril (Intellect, -6)_: This technique allows the Artificier to work with one of the most fantastic of metals, Mythril. A base roll on the Technique will allow him to forge normal Platnum into Mythril, generating up to his Calling level in pounds per hour and consuming twice that in raw iron. This technique must also be employed whenever doing anything else with Mythril, potentially making many projects impossible until the character familiarises himself sufficiently with the difficult metal.
    