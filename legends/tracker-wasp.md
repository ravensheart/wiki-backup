---
title: Tracker Wasp
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Agility Style, Melee Style, The Black Spiral, Thrown Style, Wits Style
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Default

Tracker Wasp Core Cascade
=========================

Tracker Wasp Master Jutsu
=========================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.
    