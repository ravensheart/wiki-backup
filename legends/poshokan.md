---
title: Poshokan
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Abyssal Investiture
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

A small spark of the Void exists within the heart of every person. It is the origin of greed, lust, anger and blind ambition; all of the darkest thoughts start there. But the Void is strange; it is nothing, and so has no limitations. Seekers understand this, as do some other scholars of the Void. Poshokan is the art of dedicating oneself to the void within oneself; the art of elevating the demons in one’s heart to gods, and letting them grant you power. Those who practice it, known as Posuhai, are given power directly from their own, inner, darkness.  
Unlike most forms of magic, Poshokan can be discovered spontaneously in several ways. Nearly any Seeker with Seeking 2 or better can learn this magic without further instruction, as could anyone with a sufficiently high skill in Psychology and Philosophy (Void) and a penchant for introspection. Experience earned through the Black Spiral can always be spent on Poshokan and related traits.  
Poshokan is a Will-Based Warp-Powered Granted Magic. Posuhai have access to a very limited number of spells, but ignore prerequisites. A Posuhai has access to a number of spells equal to his Abyssal Investiture squared, which should be chosen with collaboration with the GM and based on the deepest, darkest desires of the character. Since characters evolve, their desires may change, and if the GM feels it is appropriate, he may allow the player to change up to half their allowed spells when they gain a new level of Magery, refunding any points spent on those spells.  
Poshokan is a primal magic, and its abilities are almost a natural extension of the character himself. A Posuhai cannot use external sources to power his spells from this magic, even if he unification; he must use his own Warp, hit points, or fatigue, never blood sacrifices or power stones. Likewise, instilling this magic into an inanimate object is impossible: Poshokan can never be the basis of an enchantment, and the entire Enchantment college is off limits to a Posuhai, with the exception of Ensorcelment. A Poshuhai who has access to Ensorcelment can enchant himself, and only himself, with permanent magical effects. Unification allows other forms of enchantment, though the character is still limited to his personal power reserves.  
However, the intimate connection the Posuhai has to his magic makes it fundamentally easier. The cost to cast and time to cast are reduced for every 2 points of effective skill above 4, rather than every 4.
    