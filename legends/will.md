---
title: Will
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Hun Attributes
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Will is not eating a tomato when you know you have to plant it instead.
    