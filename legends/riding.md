---
title: Riding
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Beastmaster
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is your ability to ride a tame or broken mount, or to break a new mount.
    