---
title: Weather Sense
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Cunning Folk
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

This is the skill of predicting the weather just from observing the sky, sea, and animals of the world.
    