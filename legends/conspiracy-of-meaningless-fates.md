---
title: Conspiracy of Meaningless Fates
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Wind Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Conspiracy of Meaningless Fates
-------------------------------

Cost

1 w

Type

Quick Instant

Requirements

Fate 1, The Wind 1

Prerequisites

none

> The first thing to learn is how to turn anything into a weapon. This Jutsu allows the character to make a normal Thrown attack with any item he has on hand that is light enough to lift with one hand. This normal Agility + Thrown attack has the normal range for all such attacks, but suffers no weapon familiarity penalty for what item the character is using. The item has at least an Accuracy bonus of +0 and a damage bonus of at least +2 nonlethal or +1 lethal.
    