---
title: Ostentia
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Continents, Places
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Ostentia Is a medium sized continent in the northern reaches of Verund's Garden.

**Features of Interest**
========================

Sanctum Syzygy

Isle of Sight

Archtyllium

Grand Guildhall of the Farleague
    