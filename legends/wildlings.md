---
title: Wildlings
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

All Wildlings have a Spirit Animal. Usually, this is the same Spirit Animal their parents have, but not always. It is always an Animal native to the area where the Wildling was born. Wildlings can breed with one another regardless of Spirit Animal; the child of two Wildlings with different Spirit Animals sometimes takes after one of the parents, but more frequently has a different Animal entirely. Spirit Animals are never smaller than a mouse, and are always actual animals, never Beasts or Things or People or Elementals, or any of the other strange things that crawl the world.

A Wildling without any knacks resembles a member of another race, usually well enough that they can pass for a member of that race while amongst other races with just a bit of heavy clothing. Which race they resemble depends on their Spirit Animal; a Bear Wildling might look a bit like a Daikini, while a Fox Wildling might look a lot like a Sky Elf to the casual observer. It is up to the GM and the player to work out these details before play. Nearly all Wildlings pick up Gift of Three Forms early in their career; many are born with it.

Not all Wildling Knacks are available to all Wildlings. A small number of Universal Knacks are, but many others can only be taken by Wildlings with an appropriate Spirit Animal. The GM has the final say as to which Limited Knacks are available for any given Spirit Animal, but he should be generous, in general.

Most Wildlings have the virtues of the Noble Gods.

Universal Wildling Knacks
=========================

Small and Quiet
---------------

Cost

16 Experience

Prerequisites

> The smaller races have a knack for hiding in the shadows. Each level of this Knack gives the character an automatic success on all rolls to stay hidden or to prevent someone from tracking them. It can be taken no more times than the character’s Po.
    