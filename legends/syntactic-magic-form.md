---
title: Syntactic Magic Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Form of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Syntactic Magic:_ Word Magics have a number of words associated with them. Each Word is similar to a Technique with a -4 penalty, with the exception that a Mage that knows Word magic cannot use a Word he does not have a specialty in. Each word has a cost and a time associated with it, which determines the base cost and base time to cast. Usually, Word Magics require more than one word to create an effect; two or three is the minimum, depending on the magic in question. The Technique penalties for each word involved stacks, and if a given word is used more than once, its penalty is applied more than once. If the total penalty is higher than the caster’s skill, he cannot attempt the working. Otherwise, Word Magic functions much like any other flexible magic; further details below.
    