---
title: Earning Experience
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

There are several ways to gain experience. In addition to the Skills dedicated to earning experience, overcoming an adversary grants a number of experience equal to the rating of that being’s highest Soul. An individual can only gain this experience once per adversary per story; defeating someone in debate only to then turn around and kill them grants no extra experience (unless the character has a relevant Skill).

Stunts can grant extra experience. Any 3 die stunt gives the character an option of gaining an Experience rather than other rewards. Greater Ambitions, Deep Connections, Fatal Flaws and Higher Callings all give the character an opportunity to gain experience through roleplaying. Any time a player cracks a joke that gets everyone in the room to laugh, their character gains an experience point. Success beyond expectation also counts: any time the player rolls more successes on the dice than the number of dice rolled, they gain an experience point.
    