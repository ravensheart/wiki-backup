---
title: Crystal Approach
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosophy Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Crystal Approach (Intellect, -8)_: You have studied the Crystal approach to philosophy. You may use this technique to analyze something from the perspective of religion, the gods, or morality.
    