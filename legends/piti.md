---
title: Piti
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Hun Virtue, Taoist Virtue, Virtue Resists Despair
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Piti is the character's ability to enjoy life. It is his existential joy in being, the immortal nectar that flows from his heart and into the world at the wonder of it all. The character can always channel Piti when he is having fun. This virtue resists Despair.
    