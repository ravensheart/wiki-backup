---
title: Add weapons to the wiki
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  todo
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Matt, add the weapons you are making to the wiki. Read the ["how to"](/legends/how-to-make-a-weapon) if you have questions.
    