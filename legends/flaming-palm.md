---
title: Flaming Palm
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Agility Style, Melee Style, Perception Style, The Illuminated Path, Thrown Style
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Flaming Palm is a martial art for Mages. It aids in attacks made by missile spells and effects; it allows a mage to throw blasts of magic energy with deadly precision. It can also be practiced with mundane knives, and all Knives, thrown and wielded in hand, and missile spells and effects are considered in-style weapons. It is incompatible with any armor that carries an Encumbrance Penalty. Practitioners of Flaming Palm frequently train in [Thrown](/legends/thrown) techniques including Vital Strike and Ignore Armor.

Flaming Palm Core Cascade
=========================

Flaming Palm Master Jutsu
=========================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.
    