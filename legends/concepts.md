---
title: Concepts
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  TableOfContents
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Core concepts of the Raven's Heart world.

*   [Fate](/legends/fate)
*   [Iwo](/legends/iwo)
*   [Kismet](/legends/kismet)
*   [Soul](/legends/soul)
*   [The Confluence](/legends/the-confluence)
*   [The Destinies Gambit](/legends/the-destinies-gambit)
*   [The Shattering](/legends/the-shattering)
*   [The Voice of Iwo](/legends/the-voice-of-iwo)
    