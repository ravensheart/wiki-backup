---
title: Mountain Dwarves
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dwarves
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Mountain Dwarves frequently have the [Virtues of the Old Gods](/legends/virtues-of-the-old-gods) or the [Virtues of the Many Facets of Truth](/legends/virtues-of-the-many-facets-of-truth).

Mountain Dwarf Knacks
---------------------
    