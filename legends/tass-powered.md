---
title: Tass Powered
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Tass:_ Some forms of magic rely on the spirits of the world to grant the power to accomplish the task at hand. This usually involves an extended Charisma-based roll, and gathers an amount of energy equal to the margin of success each action, with the difficulty normally based on what, particular, kind of magic is being used and what effect is being generated.
    