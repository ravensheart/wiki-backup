---
title: Granted Magic
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Form of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Granted Magic:_ Granted Magic is similar to Spell Magic in that it uses spellforms, but it differs from both Ritual and Spell in that it ignores prerequisites. However, for all forms of Granted magic, the mage has a sharply limited selection of spells, no greater than his Magery squared. Further, these spells are not selected by the player or the character, but by the GM as a proxy for whatever power granted the Mage this magic.
    