---
title: Thanotology
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Connoisseur Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Thanotology (Varies, -1):_ This is the study of the Rude Arts. A successful roll when analyzing a Rude Art gives more than the usual information… usually, just which sphere it came from, and the general purpose or intent behind the object or event one exception: the work of a Serialist reveals its secrets as though the Connoisseur had Skill Focus, below, without the need of further specialization
    