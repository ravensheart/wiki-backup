---
title: Basic Rules
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Game Mechanics
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Actions and Rolling
===================

Whenever attempting a feat, the player will usually be called upon to roll an attribute plus a skill. He may always add the governing Talent to this roll as well… the sum of these three traits is the total number of 10 sided dice the player rolls. Each die that comes up a 7 or better is a success, and dice that come up 10 count as two. If there are no successes on the dice and at least one 1, then the character has critically failed the task, and suffers negative consequences.

In some cases, a Static Value is used instead of the full dice pool. A Static Value is just the “average” roll that a die pool will achieve, and is used primarily for defenses… for example, the Static Value of Dodge is calculated by adding the character’s Agility + Athletics and dividing by 2. This is the average number of successes if the character had rolled his defense, and functions as the difficulty of attack rolls against which the character dodges.

There are several things that a character can do to improve his die pools and/or total successes. Many Knacks and Jutsu add dice or successes to specific kinds of rolls, or reduce the difficulty or penalties associated with some rolls. Chi, Covenant, and Vice bonuses can almost always help; see the sections on Martial Arts and Rude Arts. Lastly, the player can aid his character by describing the action in an entertaining manner; the better the description, and the more entertaining it is, the more dice are rewarded. This is known as stunting, and rewards stunt dice. Any description past simply saying “I cast the spell” or “I hit him with my sword” should earn at least one stunt die. A good stunt should earn 2 dice, and an amazing stunt should earn 3. Stunting can also earn other rewards; see the section on stunting. On any given stunt, the player can only choose one of the available rewards.

When one of the above is used to supplement a Static Value, any automatic successes are simply added to the Static Value directly, while any dice earned are rolled and successes added to the Static Value. For example, a character with an Agility and an Alacrity of 4 and no levels in Natural Athlete will have a base Dodge of 4. However, if he has one applicable Pact, that goes up to 5. He could also stunt his Dodge by describing it well… if he does a good job with that, he might get 2 stunt dice. On a lucky roll, he might get another 3 successes on those two dice, which would put his Dodge up to 8 for that one attack!

Attributes and Souls
====================

Each character has four “Souls”, which function a lot like Essence in Exalted. They are the Hun, Khat, Po, and Alma. Each soul governs four Attributes and sixteen Talents. Each Talent has four Skills, and some Skill have further Specialties.

The attributes are:

*   Hun: Intellect, Wits, Charisma, Will
*   Khat: Strength, Agility, Health, Perception
*   Po: Manipulation, Guile, Convolution, Defiance
*   Alma: see Virtues and Urges

Attributes are limited to 4 or the rating of the governing soul, whichever is higher. Talent levels are limited to the rating of the governing soul. Skills in Po, Khat, or Alma are limited to 4x Khat or the governing soul, whichever is higher. Skills in Hun are limited to 4x Hun. A character has a number of Hit Points equal to his Khat times his Health, and amount of Willpower equal to his Hun times his Will, an amount of Fatigue equal to his Defiance times his Po, and an amount of Certainty equal to his Khat Virtue times his Alma.

Every morning, after a full night’s sleep, a character may roll his Hun Virtue and recover the successes in Willpower, and his Alma Virtue and recover his successes in Certainty. A character who rests for a full day, with plenty of food and water, can roll his Health and, on a success, recover a Hit Point. With proper medical care, he can recover one Hit Point per success. For every hour of rest, a character can roll his Health and recover the successes in Fatigue.

Characterizations
=================

Characterizations are things that make the character more than just a bunch of numbers on a sheet. They are the things he cares about, the people he loves and hates, the things that he does despite his better self. Your character MUST have at least two Characterizations, and can have as many as his souls can sustain. Each Characterization taken during character creation give the character an extra 5 experience. Then, whenever one comes up in play, but no more than once per session, if the character’s choices are constrained in some way by the presence of the trait, he adds one mark to that particular trait. At 5 marks, the player can choose to rid himself of the trait, or turn them in for 5 experience and keep the trait. He may also turn the marks in for experience and change the nature of the trait, as his personality changes through play. Further, whenever a character earns a Mark on a Characterization, he may roll the Khat virtue the soul that dominates it to recover a resource point per success for that soul. I.e. when a character earns a Mark on a Fatal Flaw, he may roll his Will to regain hit points. A character who has fewer Characterizations than his Souls would allow can, between sessions and not during play, ask the GM permission to add another drive of an appropriate type. If the GM approves the Characterization, the character immediately gains 5 experience that they can spend normally and adds the drive to their character’s sheet. Skills can have enhanced effect when they play off of existing Characterizations, but they cannot add or remove Characterizations from a character; only the player can decide when his character no longer cares about something.

Deep Connections
----------------

Deep Connections are the individuals that a character cares about. These are always people, and always have some sort of emotional context. The emotional context can be love or hate, and should have an adjective to specify why that emotion is evoked. Brotherly Love for a comrade in arms, for example, or Ideological Hatred for a politician. A character can have no more Deep Connections than his Khat.

Fatal Flaws
-----------

Fatal Flaws are vices, moral failings that the character indulges in. They can include gambling, lecherousness, carousing, drinking, drugs, racism, or nearly anything else the GM approves. Mechanically, a Fatal Flaw is a circumstance wherein the character experiences an Emotion Ordeal. He can resist as normal, unless the Fatal Flaw is a Vice, in which case he always fails to resist.

Greater Ambitions
-----------------

Greater Ambitions are things that the character aspires to; something that involves personal growth or changing the world. Something about the world that is not true that the character wants to see become true. Greater Ambitions never involve individuals other than the character himself; desire to kill someone is never a Greater Ambition, even if that person is a king and you want a more just society. Then your Greater Ambition could be “Make the society more just.”, and killing the king in an incidental. A character can have no more Greater Ambitions than his Hun.

Higher Calling
--------------

Higher Callings are deeply held moral beliefs or vows that the character sticks to as a matter of principle. They are not things that are ever done, but rather constant and continual habits that shape his life. They are never specific to a person or a place, but rather represent universal ideals that the character upholds. Vows of Poverty, Vows against using Bladed Weapons, Vows of Chastity, and the like all are Higher Callings. Higher Callings can become [Shido](/legends/shido) or [Covenants](/legends/covenants).

Earning Experience
==================

There are several ways to gain experience. In addition to the Skills dedicated to earning experience, overcoming an adversary grants a number of experience equal to the rating of that being’s highest Soul. An individual can only gain this experience once per adversary per story; defeating someone in debate only to then turn around and kill them grants no extra experience (unless the character has a relevant Skill).

Stunts can grant extra experience. Any 3 die stunt gives the character an option of gaining an Experience rather than other rewards. Greater Ambitions, Deep Connections, Fatal Flaws and Higher Callings all give the character an opportunity to gain experience through roleplaying. Any time a player cracks a joke that gets everyone in the room to laugh, their character gains an experience point. Success beyond expectation also counts: any time the player rolls more successes on the dice than the number of dice rolled, they gain an experience point.

Encumbrance and Mobility
========================

Armor usually has some Mobility Penalty. Further, a character who is carrying too much can be encumbered. A character can carry up to Strength + Potence kg without penalty. After that, for every full multiple of the above, the character suffers a -1 Encumbrance Penalty. Mobility Penalty and Encumbrance Penalty stack and subtract directly from all Agility + Alacrity static values. Twice the total penalty is also subtracted from the character’s Agility + Alacrity die pool. Characters with an Encumbrance Penalty greater than their Health + Resistance static value lose one fatigue per hour of activity from exhaustion.

Knacks and Jutsu
================

In addition to all of the skills, talents, and specialties, characters will frequently have access to Jutsu and Knacks. Jutsu have a fixed experience cost, and are usually arranged in Cascades, where all the Jutsu other than the first require that the character learn some or all of the previous Jutsu first. Jutsu have a fixed experience cost (8?), and are usually something that one activates, though some provide static bonuses. All Martial Arts Styles have Jutsu cascades that must be learned to master them. Knacks, on the other hand, are usually static in nature, rarely have prerequisites other than certain skill and talent levels, and can cost anything from 2 to 100 experience.

Recovering Resources
====================

Beyond recovering resources from stunting, characters recover naturally as well. For every 10 minutes of rest and relaxation, the character can roll his Health and regain the successes in Fatigue. After each full night’s sleep, the character can roll his Hun Virtue and regain Willpower equal to the successes, and his Defiance to recover successes in Certainty. After a full day (24 hours) rest, a character may roll Will and, if the roll succeeds, recover 1 hit point. All of these recovery rates can be affected by supernatural abilities and various Knacks.

Stunts and Rewards
==================

Whenever an action is dramatically important, a character can Stunt. By definition, this is always and only when the GM or rules explicitly call for the player to make a roll; if the GM says you don’t need to roll, you can’t stunt. A Stunt is a description of the actions your character takes to make them more interesting and cool. Stunts are explicitly not about what would work better, but what kind of dramatic flair your character puts on the action. Any description beyond “I hit him with my sword” or “I study the book” should garner at least one stunt die; decent descriptions should get 2, and amazing descriptions that the player acts out should get 3. NPCs can stunt, but the GM has to describe their actions in at least as much detail as he would require of a player. In addition to adding dice to a roll, Stunts also give the character an opportunity to recover some of their resources. Whenever the GM grants Stunt dice, the player can choose to recover a total number of Willpower, Hit Points, Fatigue, and Certainty equal to the number of dice rewarded. These rewards can be mixed and matched; he can recover 2 Willpower and one Fatigue, for example. Injuries don’t magically disappear; the character still looks injured, but they have “fought through the pain” or whatever, allowing them to continue fighting longer. On a 3 die stunt, the player can choose to instead gain an experience.

Techniques, Specialties, and Spells
===================================

Techniques are special tricks that someone with a high skill can attempt. They include the Melee Technique Disarm, the Karate Technique Lethal Strike, and any number of other things. Techniques are the bread and butter of Artisans, though they exist in Magic and Rude Arts as well. All Techniques have a penalty associated with them; this is the number of dice removed from your die pool before you attempt the Technique. You cannot attempt a Technique if the penalty is greater than your Skill. Using a Technique takes the normal amount of time as a normal skill use does, unless noted otherwise in the Technique description. Succeeding in using a Technique usually has some special effect; Lethal Strike does hit point damage, rather than fatigue damage. Disarm causes your opponent to lose his weapon (assuming he loses the contest of Skill; see the actual Technique description for further details).

Specialties are ways to get better at Techniques. You can put points into a Specialty if you have the Skill at a high enough level to attempt the Technique. Each level in a Specialty reduces the penalty associated with the Technique by one. This cannot turn the penalty into a bonus, normally (but see Spells). The final penalty or bonus associated with a Technique after taking into account Specialties is called the Specialty Modifier.

Spells are Technique-like things that exist in Magic. Spells have a number of their own rules, and have their own section elsewhere, but for the purposes of this discussion Spells have three important things that distinguish them from Techniques. First, a Spell cannot be attempted until someone has a Specialty in that Spell. Second, a Spell always has a penalty of -0. Third, the maximum Spell Modifier (a Spell’s version of a Specialty Modifier) is the character’s Hun, not 0. Learning a Spell also requires more than simply having a high enough Skill rating; see the section on Spells. Note that only Granted Magic and Spell Magic treat Spells in this manner; Ritual Magic treats Spells as normal Techniques, with a Technique Penalty equal to the prerequisite count.
    