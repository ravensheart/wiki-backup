---
title: Craft Weapon
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Craft: Void Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Craft Weapon (Intellect, -2 to -6)_: This technique allows you to craft a specific weapon. Each weapon category is a different technique, some harder or easier.
    