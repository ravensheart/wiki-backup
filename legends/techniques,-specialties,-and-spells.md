---
title: Techniques, Specialties, and Spells
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Techniques are special tricks that someone with a high skill can attempt. They include the Melee Technique Disarm, the Karate Technique Lethal Strike, and any number of other things. Techniques are the bread and butter of Artisans, though they exist in Magic and Rude Arts as well. All Techniques have a penalty associated with them; this is the number of dice removed from your die pool before you attempt the Technique. You cannot attempt a Technique if the penalty is greater than your Skill. Using a Technique takes the normal amount of time as a normal skill use does, unless noted otherwise in the Technique description. Succeeding in using a Technique usually has some special effect; Lethal Strike does hit point damage, rather than fatigue damage. Disarm causes your opponent to lose his weapon (assuming he loses the contest of Skill; see the actual Technique description for further details).

Specialties are ways to get better at Techniques. You can put points into a Specialty if you have the Skill at a high enough level to attempt the Technique. Each level in a Specialty reduces the penalty associated with the Technique by one. This cannot turn the penalty into a bonus, normally (but see Spells). The final penalty or bonus associated with a Technique after taking into account Specialties is called the Specialty Modifier.

Spells are Technique-like things that exist in Magic. Spells have a number of their own rules, and have their own section elsewhere, but for the purposes of this discussion Spells have three important things that distinguish them from Techniques. First, a Spell cannot be attempted until someone has a Specialty in that Spell. Second, a Spell always has a penalty of -0. Third, the maximum Spell Modifier (a Spell’s version of a Specialty Modifier) is the character’s Hun, not 0. Learning a Spell also requires more than simply having a high enough Skill rating; see the section on Spells. Note that only Granted Magic and Spell Magic treat Spells in this manner; Ritual Magic treats Spells as normal Techniques, with a Technique Penalty equal to the prerequisite count.
    