---
title: Explorer
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Balance
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Cartography](/legends/cartography)
*   [Navigation](/legends/navigation)
*   [Sail](/legends/sail)
*   [Survival](/legends/survival)
    