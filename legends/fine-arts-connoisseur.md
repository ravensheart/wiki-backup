---
title: Fine Arts Connoisseur
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Connoisseur Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Fine Arts Connoisseur (Varies, -2):_ This is the study of the Fine Arts. A successful roll when analyzing a Fine Art gives more than the usual information… usually, just which sphere it came from, and the general purpose or intent behind the object or event.
    