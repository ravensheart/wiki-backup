---
title: Potence
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Natural Athlete
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of lifting heavy things without injuring yourself. It includes intuitions about leverage and inertia. It figures in when calculating encumbrance, and is useful in grapples as well.
    