---
title: Craft: Void
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Artificier
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Crafts of the Void are those crafts that involve destruction. All weapons are part of Craft: Void, as are poisons, siege weapons, and the like.

_Craft Weapon (Intellect, -2 to -6)_: This technique allows you to craft a specific weapon. Each weapon category is a different technique, some harder or easier.

_Work With Adamant (Intellect, -8)_: This technique allows the Artificier to work with the hardest of metals, Adamant. A base roll on the Technique will allow him to forge normal Iron into Adamant, generating up to his Calling level in pounds per hour and consuming twice that in raw iron. This technique must also be employed whenever doing anything else with Adamant, potentially making many projects impossible until the character familiarises himself sufficiently with the difficult metal.

_Work With Mythril (Intellect, -6)_: This technique allows the Artificier to work with one of the most fantastic of metals, Mythril. A base roll on the Technique will allow him to forge normal Platnum into Mythril, generating up to his Calling level in pounds per hour and consuming twice that in raw iron. This technique must also be employed whenever doing anything else with Mythril, potentially making many projects impossible until the character familiarises himself sufficiently with the difficult metal.
    