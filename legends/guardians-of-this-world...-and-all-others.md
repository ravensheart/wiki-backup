---
title: Guardians of This World... And All Others
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Greatfolk Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

**Guardians of This World...And All Others**  
Cost: 8 Experience  
The Greatfolk have walked between the gardens for lengths of time unknowable, Oaths that were sworn in a strange place now lost to memory are remembered in the bloodlines of the guardians of the place that the greatfolk (and all the children of those who swore this oath) once came from. Each level of this knack grants the character an automatic success to defend against influence that would debilitate or hinder them in the lands that lie between gardens.
    