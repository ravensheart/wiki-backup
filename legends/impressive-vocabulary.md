---
title: Impressive Vocabulary
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Kender Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

8 experience

Prerequisites

> Kender are well known for their creative use of words, especially when angry. The Kender may roll their Wits + Fast Talk at a difficulty of their target’s SDR; this roll must be stunted, and the Knack does not function unless the player gets at least one stunt die. If they succeed, their target must spend a Certainty or attack them next round. Using this Knack in combat is a reflexive action, and if the target is already engaged in physical combat, he does not have the option to spend Certainty.
    