---
title: Kobalds
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dragonkin
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Kobalds are short, mean, lizard-like people who are considered vermin in most places.

Kobald Knacks
-------------

Light Fingers
-------------

Cost

8 Experience

Prerequisites

none

> Some smaller races are adept at sleight-of-hand and mundane trickery. Each purchase of this Knack adds one to the difficulty of all Perception rolls to notice the Koobald performing an illegal or nefarious act in plain sight: slipping poison into someone’s drink, filching an apple from a fruit stall, etc. This Knack can be purchased more than once, but no more times than the character’s Po.

Sharp Nose
----------

Cost

4 experience

Prerequisites

none

> Some species have a sense of smell that a bloodhound would envy. Each level of this Knack grants the character an automatic success on all smell and taste related Perception checks, and can be taken up to as many times as the character’s Perception.

Small and Quiet
---------------

Cost

16 Experience

Prerequisites

> The smaller races have a knack for hiding in the shadows. Each level of this Knack gives the character an automatic success on all rolls to stay hidden or to prevent someone from tracking them. It can be taken no more times than the character’s Po.
    