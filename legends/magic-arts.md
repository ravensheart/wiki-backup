---
title: Magic Arts
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Magic is what it is called when a person evokes the primordial forces of creation to alter the world around them. In general, Magic can use one of eight sources of power, and can take one of eight forms. The primordial magic is [Wyld Magic](/legends/wyld-magic).

**Sources of Power:** All sources of power provide energy for Magic, in different amounts. Spellforms have an energy cost associated with them, as do most forms of flexible magic. Under certain circumstances, one source of energy can be used to power another. For this reason, all of the sources of power, below, have listed an energy equivalence. This does not mean that they are innately fungible, but simply provides an exchange rate when they are.

_Blood:_ Blood is a potent source of magic for those who use blood magic. Each hit point sacrificed is worth 2 energy, and most forms of blood magic allow you to use the hit points of others, so long as you are touching blood that is continuous with their heart. Many forms of magic also allow blood sacrifices, which can provide a LOT of energy for magic. Specifics vary per magic, but in general the amount of energy gained is twice the product of the sacrifice’s souls.

_Fatigue:_ Some magics also function off of Fatigue, but such magics are usually inefficient. Fatigue only provides 1 energy per point.

_Material:_ Material magics take costly and rare materials and use them to produce magical effects. In general, every 50 copper worth of appropriate materials is equivalent to 1 energy.

_Ordeal:_ The Virtuous can, with some forms of magic, turn their virtue into power. Ordeal based magics provoke a Virtue roll, with the amount of energy provided equal to twice the difficulty of the roll. The magic in question will specify which virtue must be rolled. Failure on this roll always has significant, permanent consequences for the character, as specified in the particular magic.

_Tass:_ Some forms of magic rely on the spirits of the world to grant the power to accomplish the task at hand. This usually involves an extended Charisma-based roll, and gathers an amount of energy equal to the margin of success each action, with the difficulty normally based on what, particular, kind of magic is being used and what effect is being generated.

_Time:_ Some magics, primarily magics that can only be used on enchanting, allow the character to put in only time and produce magic. Such magics produce 100 energy per hour, but again, can usually only be used to permanently enchant things. Some such magics can produce single-use items faster, usually at the rate of 1 energy per minute.

_Warp:_ Warp magics are powerful and dangerous. A character accumulates Warp whenever they cast a spell using Warp magic. They can safely handle an amount of Warp equal to their Warp Edge; anything less than that and they suffer no consequences. Unlike Fatigue, Willpower and Hit Points, Warp recovers only slowly; a character reduces his Warp by his Recovery once per day, at a time determined by the kind of magic he knows. There are several other rules connected to Warp; see the section dedicated to discussing this topic.

_Willpower:_ Willpower is the native form of power for a lot of magic. Each point of Willpower provides one point of energy for spellforms and flexible magic, for those kinds of magic that use Willpower.

**Forms of Magic**

_Empowering Magic:_ Empowering Magics give the character new ways to earn experience, and frequently allow him to use that experience to purchase Knacks and traits that he would not normally have access to.

_Granted Magic:_ Granted Magic is similar to Spell Magic in that it uses spellforms, but it differs from both Ritual and Spell in that it ignores prerequisites. However, for all forms of Granted magic, the mage has a sharply limited selection of spells, no greater than his Magery squared. Further, these spells are not selected by the player or the character, but by the GM as a proxy for whatever power granted the Mage this magic.

_Realm Magic:_ Realm Magics are similar to Word Magics, except that there are usually fewer divisions and a mage can usually create an effect with just one Realm. Additionally, the mage must buy levels in Realm Knacks, which allow him to create the magic effects necessary. Innate Magic: Innate Magics allow the character to grant himself and others Knacks at the expense of experience.

_Ritual Magic:_ Ritual Magic functions like Spell Magic, except that the Spells function exactly like Techniques of other cardinalities. Spell Techniques of Ritual magic have a penalty equal to their prerequisite count, and can be trained as Specialties just like with Artisan.

_Spell Magic:_ Spell Magic uses Spellforms and requires that the Mage learn each spell in order, learning prerequisites before learning the next spell. Normally, Spell Magic users must find the spell somewhere in the world to learn it. Each Spell is similar to a Technique, but Spell Magic users have an advantage over others in that they can train specific Spells to the point where there is a bonus, rather than a penalty. This bonus cannot be greater than the spellcaster’s Hun, and training any given spell counts as a Specialty. Unlike Techniques, Spells cannot be cast without first putting points into the Spell, and one cannot put points into a Spell until one has points in all prerequisite Spell.

_Symbolic Magic:_ Almost identical to Syntactic Magic, except that Symbolic Magic uses symbols rather than words. Many magics that are one are also frequently the other, but this is not always true, and as such the distinction can be important. Symbolic Magic must be written in order to be used, while Syntactic must be spoken. This means that what can prevent the character from casting can be different, but the magics function otherwise identically, except as specified for particular magics.

_Syntactic Magic:_ Word Magics have a number of words associated with them. Each Word is similar to a Technique with a -4 penalty, with the exception that a Mage that knows Word magic cannot use a Word he does not have a specialty in. Each word has a cost and a time associated with it, which determines the base cost and base time to cast. Usually, Word Magics require more than one word to create an effect; two or three is the minimum, depending on the magic in question. The Technique penalties for each word involved stacks, and if a given word is used more than once, its penalty is applied more than once. If the total penalty is higher than the caster’s skill, he cannot attempt the working. Otherwise, Word Magic functions much like any other flexible magic; further details below.

Spellforms Spellforms are cosmically-available patterns of magic that are more reliable than the more “raw” flexible magics. Spell Magic, Granted Magic and Ritual Magic all use spellforms. Spellforms generally have a fixed cost or a cost that varies linearly with one or two things (Damage, Area and Duration are common). Full rules will be in a different document. The cost of a spellform is reduced by 1 for every 4 full points of effective Skill in the spell the character has above 4. Effective skill is Skill +/- Technique penalties and Specialization.

Parameters Flexible Magics, which are Symbolic, Syntactic, and Realm Magics, use Parameters to adjust what their magic does, rather than using spellforms. There are 4 Parameters, plus the special Magnitude Parameter. Each can be handled by a given magic system in one of three ways: [Increased Cost](/legends/increased-cost-magic), [Skill Penalty](/legends/skill-penalty-magic), or [Success Dependent](/legends/success-dependent-magic). Other than Magnitude, all Parameters follow the following rules: Increased Cost means that the number (below) is added to the Base Cost of the effect, meaning that it takes more energy to cast. Skill Penalty means that the number associated with the parameter is treated like a Technique penalty, including the ability of a Mage to spend points to buy off that penalty. Success Dependent means that the Mage has no control over the Parameter before casting, and instead looks up his successes on the appropriate table to determine the parameter. Success Dependent casters can specify a maximum parameter before casting, but must do so if they want to control that parameter… with no specificity, the parameters are maxed out.

Magnitude is odd. Instead of adding, it multiplies. When Magnitude is dependent on Cost, multiply the full Base Cost by the Magnitude number. When Magnitude is dependent on Skill Penalty, multiply the sum of all skill penalties suffered by the Magnitude number before accounting for things like Specialties and Luck. When Magnitude is Success Dependent, divide the number of successes by the desired Magnitude before considering other Success Dependent parameters. Again, a Success Dependent caster can, before rolling, specify how she wants the successes parsed; this is especially important when casting damaging spells, because the default assumption is Self-Only, Touch Range, and maximum Magnitude.

Magnitude:

*   Sensory effects only
*   1 die of damage, 10 kilos, 8 character points
*   2 dice of damage, 20 kilos, 16 character points
*   etc.

Scope ([AoE](/legends/aoe)):

*   Self only
*   1 target Missile Spell
*   2 Targets
*   3 Targets
*   4 Targets
*   5 Targets...
*   Area of Effect

Range:

*   Self, or touch range
*   10 meter range for non-sensory, 10 meters for sensory
*   20 meter range for non-sensory, 100 meters for sensory
*   30 meter range for non-sensory, 1,000 meters for sensory
*   Etc.
*   ...
*   Maladiction

Range in Time:

*   Now
*   1 minute for non-sensory, 10 minutes for sensory
*   10 mtes for non-sensory, 1 hour for sensory
*   30 minutes for non-sensory, 12 hours for sensory
*   1 hour for non-sensory, 1 day for sensory
*   2 hours for non-sensory, 1 week for sensory
*   3 hours for non-sensory, 1 month for sensory
*   4 hours for non-sensory, one season for sensory
*   5 hours for non-sensory, one year for sensory
*   6 hours for non-sensory, one decade for sensory
*   7 hours for non-sensory, one century for sensory
*   +1 hour per level for non-sensory, x10 per level for sensory

Duration:

*   Instant
*   1 minute for non-sensory, 10 minutes for sensory
*   10 minutes for non-sensory, 1 hour for sensory
*   30 minutes for non-sensory, 12 hours for sensory
*   1 hour for non-sensory, 1 day for sensory
*   2 hours for non-sensory, 1 week for sensory
*   3 hours for non-sensory, 1 month for sensory
*   4 hours for non-sensory, one season for sensory
*   5 hours for non-sensory, one year for sensory
*   6 hours for non-sensory, one decade for sensory
*   7 hours for non-sensory, one century for sensory
*   +1 hour per level for non-sensory, x10 per level for sensory

*   [Abyssal](/legends/abyssal)
*   [Celestial](/legends/celestial)
*   [Ethereal](/legends/ethereal)
*   [Terrestrial](/legends/terrestrial)
    