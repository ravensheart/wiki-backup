---
title: Naturalist
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Cunning Folk
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the knowledge of the natural world, including what animals and plants are safe to eat, where to find specific herbs or animals, and the like.
    