---
title: Thousand Voiced Silence
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Karate Style, Melee Style, Perception Style, The Silent Road, Wits Style
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Thousand Voiced Silence is a style designed for combating the most vile of opponents, those who have the power to change one's mind and will with mere words. It uses the Sai, Kusari, Staff, and rope, as well as unarmed strikes and grappling. Thousand Voiced Silence enhances Perception and Wits, and cannot be practiced while the character suffers an Encumbrance Penalty.

Thousand Voiced Silence Core Cascade
====================================

Binding Parry
-------------

Cost

2 w

Type

Free Persistant

Requirements

Road 1, 1k Silence 1

Prerequisites

none

> As your opponent strikes, you move to the side and bind his weapon with rope. This jutsu can only be activated if you have rope in hand. Upon activating this Jutsu, roll Agility + Melee at a difficulty equal to your opponent's Agility + Alacrity static value. Success on this roll means you have Disabled the attacking limb or weapon; for weapons, this means you have disarmed your opponent, for limbs, you have bound them with rope. This lasts until the rope is cut loose or untied. Untying takes several minutes.

Blind Strike
------------

Cost

1 w

Type

Quick Instant

Requirements

Road 1, 1k Silence 1

Prerequisites

[Binding Parry](/legends/binding-parry)

> The martial artist closes his eyes and strikes. This strike uses no normal senses, and ignores all penalties to hit based on visibility, noise, or any other sense-based penalty. It functions equally well against invisible opponents and in total darkness. This attack can be made with any in-style weapon.

Scream of a Thousand Voices
---------------------------

Cost

1 w

Type

Free Instant

Requirements

Road 1, 1k Silence 1

Prerequisites

[Binding Parry](/legends/binding-parry)

> Your enemies attempt to change your way, but you cannot hear them. This Jutsu can only be used in response to a social attack, and must be used before the character knows what the content of that attack will be. Instead of listening to the voice of his attacker, he opens himself up to the voices of the gods. Their screams and whispers, solicitations and condemnations fill your mind for a brief moment, drowning out any other attempt at influence. This Jutsu perfectly defends against the social attack, and may, at the GM's discretion, also give the character insight into the future or the [Destiny](/legends/destiny) of those around him.

Thousand Voiced Silence Form
----------------------------

Cost

\-

Type

Normal Form

Requirements

Road 2, 1k Voiced Silence 2

Prerequisites

placeholder

> placeholder

Thousand Voiced Silence Master Jutsu
====================================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.
    