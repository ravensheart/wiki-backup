---
title: Control
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Fine Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Beastmaster](/legends/beastmaster)
*   [Combat](/legends/combat)
*   [Empath](/legends/empath)
*   [Tactician](/legends/tactician)
    