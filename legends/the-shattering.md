---
title: The Shattering
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Concepts
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Shattering refers to the the act of creation of a new universe. Many Artisan philosophers posit that the first event in each universe in the infinite pattern of universes that predate this one begins with an event known only as "the shattering."
    