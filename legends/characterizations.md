---
title: Characterizations
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Characterizations are things that make the character more than just a bunch of numbers on a sheet. They are the things he cares about, the people he loves and hates, the things that he does despite his better self. Your character MUST have at least two Characterizations, and can have as many as his souls can sustain. Each Characterization taken during character creation give the character an extra 5 experience. Then, whenever one comes up in play, but no more than once per session, if the character’s choices are constrained in some way by the presence of the trait, he adds one mark to that particular trait. At 5 marks, the player can choose to rid himself of the trait, or turn them in for 5 experience and keep the trait. He may also turn the marks in for experience and change the nature of the trait, as his personality changes through play. Further, whenever a character earns a Mark on a Characterization, he may roll the Khat virtue the soul that dominates it to recover a resource point per success for that soul. I.e. when a character earns a Mark on a Fatal Flaw, he may roll his Will to regain hit points. A character who has fewer Characterizations than his Souls would allow can, between sessions and not during play, ask the GM permission to add another drive of an appropriate type. If the GM approves the Characterization, the character immediately gains 5 experience that they can spend normally and adds the drive to their character’s sheet. Skills can have enhanced effect when they play off of existing Characterizations, but they cannot add or remove Characterizations from a character; only the player can decide when his character no longer cares about something.

Deep Connections
----------------

Deep Connections are the individuals that a character cares about. These are always people, and always have some sort of emotional context. The emotional context can be love or hate, and should have an adjective to specify why that emotion is evoked. Brotherly Love for a comrade in arms, for example, or Ideological Hatred for a politician. A character can have no more Deep Connections than his Khat.

Fatal Flaws
-----------

Fatal Flaws are vices, moral failings that the character indulges in. They can include gambling, lecherousness, carousing, drinking, drugs, racism, or nearly anything else the GM approves. Mechanically, a Fatal Flaw is a circumstance wherein the character experiences an Emotion Ordeal. He can resist as normal, unless the Fatal Flaw is a Vice, in which case he always fails to resist.

Greater Ambitions
-----------------

Greater Ambitions are things that the character aspires to; something that involves personal growth or changing the world. Something about the world that is not true that the character wants to see become true. Greater Ambitions never involve individuals other than the character himself; desire to kill someone is never a Greater Ambition, even if that person is a king and you want a more just society. Then your Greater Ambition could be “Make the society more just.”, and killing the king in an incidental. A character can have no more Greater Ambitions than his Hun.

Higher Calling
--------------

Higher Callings are deeply held moral beliefs or vows that the character sticks to as a matter of principle. They are not things that are ever done, but rather constant and continual habits that shape his life. They are never specific to a person or a place, but rather represent universal ideals that the character upholds. Vows of Poverty, Vows against using Bladed Weapons, Vows of Chastity, and the like all are Higher Callings. Higher Callings can become [Shido](/legends/shido) or [Covenants](/legends/covenants).
    