---
title: Industry
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Rude Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Chemistry](/legends/chemistry)
*   [Engineering](/legends/engineering)
*   [Medicine](/legends/medicine)
*   [Necrolinguist](/legends/necrolinguist)
    