---
title: Resolve
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  General Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

4 experience

Prerequisites

Hun 2+

> When you want to get something done, you can get it done. You can, on any roll, spend a Willpower to gain a number of automatic successes equal to the number of times you have taken this Knack. You cannot take this knack more than once for every two full levels of Hun you have.
    