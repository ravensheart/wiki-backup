---
title: Pseudomagic
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Abyssal
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Pseudomagic [opposes](/legends/opposing-magics) [Material Magic](/legends/material)
    