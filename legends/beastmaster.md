---
title: Beastmaster
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Control
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Animal Handling](/legends/animal-handling)
*   [Camouflage](/legends/camouflage)
*   [Riding](/legends/riding)
*   [Tracking](/legends/tracking)
    