---
title: Psychology
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Empath
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of understanding others, and helping them get past their own problems.
    