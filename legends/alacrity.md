---
title: Alacrity
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Natural Athlete
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

This is the skill of being quick on one's feet. It figures into the character's Dodge and Full Move, and is extremely important to nearly all forms of movement.
    