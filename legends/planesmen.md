---
title: Planesmen
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Humans
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Planesmen are tall, usually light of skin and hair, with blue or green eyes. Planesmen frequently have the [Court of Heaven Virtues](/legends/court-of-heaven-virtues)
    