---
title: Dezgonesh
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Motgo
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Motgo Executioner, Motgo Gardener, Motgo Assassin
=================================================

placeholder

### Consecration or Dedication

Find someone who needs to die, kill them. Justify their death and understand all positive and negative ramifications of their death out some number of years, minimum 1.

### Covenant

never take life of Person without understanding ramifications

### Attunement

holy bonus to convince others you must complete your mission and bonus to avoid detection when on mission

### Gift

gain access to Silent Road: Thousand Voice Silence

### Atonement

placeholder
    