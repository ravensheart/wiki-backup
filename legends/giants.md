---
title: Giants
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

General Giant Knacks
====================

Biggun
------

**Biggun**  
Cost: 8 experience  
Some creatures just grow big. Each level of this knack reduces the training time needed to increase the character’s Khat by half. This Knack can be taken multiple times, up to once per level of the character’s Hun.
    