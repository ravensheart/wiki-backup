---
title: Veracity
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Alma Virtue, Dark God Virtue, Virtue Resists Callousness
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Veracity is your desire for and adherence to truth, both in your words and in your actions. With a Veracity of 3 or higher, you must fail a Veracity roll in order to tell an outright lie, though omission is fine. Veracity resists Callousness.
    