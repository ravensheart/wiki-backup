---
title: The Black Spiral
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Onyx Sphere
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

*   [Boxing Jellyfish](/legends/boxing-jellyfish)
*   [Castrator Ant](/legends/castrator-ant)
*   [Cuckoo](/legends/cuckoo)
*   [Tracker Wasp](/legends/tracker-wasp)

The Black Spiral Master Jutsu
=============================
    