---
title: Cunning Folk
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Balance
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Fortune Telling](/legends/fortune-telling)
*   [Naturalist](/legends/naturalist)
*   [Occult](/legends/occult)
*   [Weather Sense](/legends/weather-sense)
    