---
title: Starlight Vision
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dragonkin Knacks, Dwarf Knacks, Elf Knacks, Orc Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

4 experience

Prerequisites

> You are at home in dim light, such as that provided by the stars or a dim torch. You can see five times as far using a single light source as a normal human, and see by starlight just as well as a human does in full daylight. A character with both this Knack and Home in Darkness suffers no penalties is full daylight.
    