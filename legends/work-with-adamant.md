---
title: Work With Adamant
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Craft: Tapestry Techniques, Craft: Void Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Work With Adamant (Intellect, -8)_: This technique allows the Artificier to work with the hardest of metals, Adamant. A base roll on the Technique will allow him to forge normal Iron into Adamant, generating up to his Calling level in pounds per hour and consuming twice that in raw iron. This technique must also be employed whenever doing anything else with Adamant, potentially making many projects impossible until the character familiarises himself sufficiently with the difficult metal.
    