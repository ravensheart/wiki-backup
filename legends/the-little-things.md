---
title: The Little Things
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Nelwin Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Cost

16 experience

Prerequisites

> Other races care about grand things. Castles and armies and great magics. Nelwin love the little things. Afternoon tea. The breeze across their porch. Enjoying a drink and a song at the local tavern. It is these things that the Nelwin values, and it is these things that keep them going. The character gains an automatic success to resist Despair for every level in this Knack, and can have as many levels in this Knack as his Alma. He need only remember the taste of bread.
    