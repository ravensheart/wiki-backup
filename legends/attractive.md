---
title: Attractive
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  General Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

4 experience

Prerequisites

Khat 2+

> You are physically attractive. You reduce the SDR of anyone you engage in social combat by 1 per level of this Knack, which can be purchased once for every two full levels of Khat you have. Against those who have a Fatal Flaw relating to being attracted to people like you, their SDR is reduced by 2 for every level of this Knack.
    