---
title: Backend server
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  todo
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The backend server has to be made for the Web app to work.

*   this needs a few things like the database
*   and a reverse proxy server for the subdomains
*   this also involves moving the wiki to a subdomain
*   and designing a REST api.
    