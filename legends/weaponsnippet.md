---
title: weaponSnippet
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  $:/tags/TextEditor/Snippet, template
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

A single edged sword, often shorter than cut & thrust sword.

**Time Cost**

**Damage**

**Damage Type**

**Accuracy**

**Defense**

**Reach**

6

+3

Lethal

0

+1

1
    