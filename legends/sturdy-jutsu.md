---
title: Sturdy Jutsu
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Health Style Master Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Sturdy
------

Cost

\-

Type

Stackable Permanent

Requirements

Master of a Health Style

Prerequisites

none

> Some kinds of training make you very tough. Each level in this Jutsu adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Path Talent. This Jutsu does not stack with the Knack of the same name.
    