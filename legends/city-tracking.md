---
title: City Tracking
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Tracking Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_City Tracking (Perception, -4)_: This technique allows you to track prey, to include humans, through any urban environment.
    