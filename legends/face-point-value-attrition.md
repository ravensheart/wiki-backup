---
title: Face Point Value Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Play Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

In this Attrition Variant, cards are worth more or less depending on their face value, and sometimes suit. Usually, this is juts that each card is worth its face value, but sometimes there is a "gold suit" which is worth twice what the others are.
    