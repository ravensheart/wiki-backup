---
title: Brawling
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Primordial Skill
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Brawling is the Primordial form of combat. It is the martial style that predates all other martial styles, and is the first way in which man attempted to impose his will on man. Brawling has no jutsu and no trainable techniques, and like all Primordial Skills, it can never be mastered. However, it has an infinite number of untrainable techniques. As a Primordial skill, the sum of all Path Talents add to all Brawling rolls.

Any jutsu that exists in any Martial Art, or that could be designed for any Martial Art, or has been part of any Martial Art, is a Brawling technique. The Penalty for these techniques is equal to the product of the Skill and Path Talent requirements for the Jutsu. The cost of activation is spent whether this roll succeeds or not, and jutsu that normally have no cost instead cost one willpower. Should the roll succeed, the jutsu is activated normally, and lasts for its normal duration. For permanent jutsu, they last only for a scene.

Further, all techniques for all Combat skills are also Brawling techniques. In any situation where one could roll Karate, Melee, or Thrown, one could roll Brawling instead. Marksman is too refined for this primordial skill to simulate normally, but a Brawling roll can use any Marksman technique too, at a further -2.

For every level in Brawling the character has, he may apply his Chi bonus to one additional trait. Permissible traits include: All Hun Attributes, all Khat Attributes, and AR. He may select the same Attribute as many times as he wishes. Note that having levels in Brawling does not give you Shido or a Chi Bonus on its own; that must be acquired elsewhere.
    