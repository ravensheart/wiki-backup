---
title: The Road Ahead
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Fortune Telling Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_The Road Ahead (Perception, -8)_: You look into the future and see what paths need to be walked. This technique allows you to give yourself or some other cooperative subject a quest worth no more experience than your Calling level in Cunning Folk. You have no control over the exact nature of this quest other than its magnitude. In general, the quests granted by this technique are good in a Tapestrial sense.
    