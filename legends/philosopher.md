---
title: Philosopher
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Enlightenment
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Debate](/legends/debate)
*   [Deep Lore](/legends/deep-lore)
*   [Mathematics](/legends/mathematics)
*   [Philosophy](/legends/philosophy)
    