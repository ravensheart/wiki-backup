---
title: Ithbar
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Motgo
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Motgo Jester, Motgo Soldier, Motgo Fool
=======================================

placeholder

### Consecration or Dedication

1 year mandatory military service

### Covenant

placeholder

### Attunement

Holy bonus to Charisma

### Gift

access to Heart of Stone

### Atonement

placeholder
    