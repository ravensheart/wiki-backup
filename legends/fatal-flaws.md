---
title: Fatal Flaws
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Characterizations
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Fatal Flaws are vices, moral failings that the character indulges in. They can include gambling, lecherousness, carousing, drinking, drugs, racism, or nearly anything else the GM approves. Mechanically, a Fatal Flaw is a circumstance wherein the character experiences an Emotion Ordeal. He can resist as normal, unless the Fatal Flaw is a Vice, in which case he always fails to resist.
    