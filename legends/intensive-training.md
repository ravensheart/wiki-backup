---
title: Intensive Training
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Drill Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Intensive Training (Will, -16)_: Your ability to train is nearly as intensive as that of a Martial Artist. You can undergo an 8 hour training session and then roll against this technique at a difficulty equal to the number of people you are training, including yourself (if you are training). Success on this roll gives everyone you trained the opportunity to gain an amount of experience up to your Tactician level. To do so, they must succeed on a Health + Resistance (or Drill) roll at a difficulty equal to the amount of experience they would gain. Failure on this roll results in no experience and the loss of all Fatigue the character has. Success grants the promised experience, and reduces the character's Fatigue to half its maximum value. Experience gained through this Technique cannot be spent on Magic, but it can be spent on any Fine or Rude Art the character has access to.
    