---
title: Deceptive Attack Technique
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Karate Techniques, Melee Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Deceptive Attack (Agility, -2 or more):_ This is a deceptive attack, helping you bypass your opponent's defenses. It reduces your opponent's Dodge, Parry or Block against this attack, only, by 1 for every -2 to skill you take.
    