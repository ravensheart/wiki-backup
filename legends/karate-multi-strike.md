---
title: Karate Multi-Strike
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Karate Techniques
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Multi-Strike (Agility, -3 per extra attack)_: This technique allows the Artisan to make multiple attacks in a single action. Every attack made is at the same penalty, which is -3 per attack beyond the first. As normal, this technique can be trained up as a Specialty, but no more levels in the specialty than the character’s skill level. Further techniques can be used on these strikes, but the same techniques must be used on every strike, and the penalty stacks. Unlike most techniques, Multi-Strike takes a lot of concentration; all of the penalties for using this Technique apply not only to the character’s attack pool, but also to his defensive pools, until his next action.
    