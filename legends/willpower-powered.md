---
title: Willpower Powered
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

_Willpower:_ Willpower is the native form of power for a lot of magic. Each point of Willpower provides one point of energy for spellforms and flexible magic, for those kinds of magic that use Willpower.
    