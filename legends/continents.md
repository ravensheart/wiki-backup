---
title: Continents
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Geography, Lore
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Large formations of Tapestry that have been shaped by the will of the Gods and the Primodial Embodiments to form firmament.
    