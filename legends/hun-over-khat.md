---
title: Hun over Khat
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Meditation Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Hun over Khat (Will, -4)_: You may substitute this technique for any Health + Resistance or static value to avoid taking Fatigue Damage. Note that nonlethal combat damage does not normally allow a resistance roll, so this technique does not apply.
    