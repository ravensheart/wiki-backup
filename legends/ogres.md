---
title: Ogres
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Greatfolk
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.

Cost

16 experience

Prerequisites

> Some races are tough to kill. Each level in this Knacks adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Khat.

Cost

16 experience

Prerequisites

> Some creatures are just tough. The character gains a natural AR of 1. This increases by 1 for every extra time this Knack is purchased, no more times than his Khat.
    