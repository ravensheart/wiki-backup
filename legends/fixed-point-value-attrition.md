---
title: Fixed Point Value Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Gambling Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This Attrition Variant is usually only seen in casinos and other organized gambling halls. In it, each player agrees to pay the winner a fixed amount of money per point they win in any hand they win. Frequently, the "house" will have a skilled player participating on their behalf.
    