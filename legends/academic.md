---
title: Academic
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Enlightenment
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

*   [Literature](/legends/literature)
*   [Research](/legends/research)
*   [Theology](/legends/theology)
*   [Writing](/legends/writing)
    