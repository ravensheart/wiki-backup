---
title: Blind Strike
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Thousand Voiced Silence Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Blind Strike
------------

Cost

1 w

Type

Quick Instant

Requirements

Road 1, 1k Silence 1

Prerequisites

[Binding Parry](/legends/binding-parry)

> The martial artist closes his eyes and strikes. This strike uses no normal senses, and ignores all penalties to hit based on visibility, noise, or any other sense-based penalty. It functions equally well against invisible opponents and in total darkness. This attack can be made with any in-style weapon.
    