---
title: No invitation
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Kender Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

8 experience

Prerequisites

> A locked door is an invitation, at least to a Kender. This Knack grants one automatic success on all rolls related to opening a locked portal, no matter the means. It can be purchased more than once, up to the character’s Po, and its effects stack.
    