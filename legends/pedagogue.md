---
title: Pedagogue
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Agents of the State
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Instruction is important to any good society. The Pedagogue understands and exploits this. Pedagogue is an Enhancement Art wherein the character acts as a teacher and mentor to others.

The character can teach up to a number of people equal to his skill in Pedagogue at the same time. For every 20 hours (tracked per student), the Pedagogue gains a number of experience equal to his level in Agents of the State. Every time the Pedagogue gains experience from a student, he may also reallocate one of that student's character points. This can be done by giving the student a new debilities or characterizations, or reducing that student's traits. He may then spend these points on new traits for the student, presumably to make them good at the thing the pedagogue is training them in. Pedagogues frequently forge Deep Connections in their students towards themselves.
    