---
title: The Path of Fate
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Celestium Sphere
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Path of Fate can be discovered spontaneously through the contemplation of the nature of Fate and the truth behind what should be. Any character that spends 100 hours in such contemplation and garners 10 or more successes on an Intellect + Philosophy (Taoism) roll can spontaneously discover this path.

*   [The Breath](/legends/the-breath)
*   [The Wind](/legends/the-wind)

The Path of Fate Master Jutsu
=============================
    