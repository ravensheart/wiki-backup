---
title: Actions and Rolling
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Whenever attempting a feat, the player will usually be called upon to roll an attribute plus a skill. He may always add the governing Talent to this roll as well… the sum of these three traits is the total number of 10 sided dice the player rolls. Each die that comes up a 7 or better is a success, and dice that come up 10 count as two. If there are no successes on the dice and at least one 1, then the character has critically failed the task, and suffers negative consequences.

In some cases, a Static Value is used instead of the full dice pool. A Static Value is just the “average” roll that a die pool will achieve, and is used primarily for defenses… for example, the Static Value of Dodge is calculated by adding the character’s Agility + Athletics and dividing by 2. This is the average number of successes if the character had rolled his defense, and functions as the difficulty of attack rolls against which the character dodges.

There are several things that a character can do to improve his die pools and/or total successes. Many Knacks and Jutsu add dice or successes to specific kinds of rolls, or reduce the difficulty or penalties associated with some rolls. Chi, Covenant, and Vice bonuses can almost always help; see the sections on Martial Arts and Rude Arts. Lastly, the player can aid his character by describing the action in an entertaining manner; the better the description, and the more entertaining it is, the more dice are rewarded. This is known as stunting, and rewards stunt dice. Any description past simply saying “I cast the spell” or “I hit him with my sword” should earn at least one stunt die. A good stunt should earn 2 dice, and an amazing stunt should earn 3. Stunting can also earn other rewards; see the section on stunting. On any given stunt, the player can only choose one of the available rewards.

When one of the above is used to supplement a Static Value, any automatic successes are simply added to the Static Value directly, while any dice earned are rolled and successes added to the Static Value. For example, a character with an Agility and an Alacrity of 4 and no levels in Natural Athlete will have a base Dodge of 4. However, if he has one applicable Pact, that goes up to 5. He could also stunt his Dodge by describing it well… if he does a good job with that, he might get 2 stunt dice. On a lucky roll, he might get another 3 successes on those two dice, which would put his Dodge up to 8 for that one attack!
    