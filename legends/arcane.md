---
title: Arcane
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  ethereal, Ethereal, magic arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Arcane Magic [opposes](/legends/opposing-magics) [Spirit Investiture](/legends/spirit-investiture)
    