---
title: Survival
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Explorer
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of living in the wilderness without help.
    