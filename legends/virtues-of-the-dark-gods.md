---
title: Virtues of the Dark Gods
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Dark Gods, Virtue System
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

**Appetite**

Appetite is the Hun virtue of the Dark Gods. It represents the character's desire for growth and power. It can also represent the character's actual hunger, especially for those who have [Nephendus](/legends/nephendus). Appetite resists Despair.

**Diligence**

Diligence is the Khat virtue of the Dark Gods. It represents the character's dedication of purpose and ability to set themselves to a task completely. It resists Temptation.

**Veracity**

Veracity is your desire for and adherence to truth, both in your words and in your actions. With a Veracity of 3 or higher, you must fail a Veracity roll in order to tell an outright lie, though omission is fine. Veracity resists Callousness.

**Wrath**

Wrath is the Po virtue of the Dark Gods. It represents the character's capacity for anger and hatred, and tends to represent a more long-term anger than similar virtues. Wrath resists Fear.
    