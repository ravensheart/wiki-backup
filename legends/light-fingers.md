---
title: Light Fingers
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Goblin Knacks, Kobald Knacks, Nomad Knacks, Racial Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

8 Experience

Prerequisites

none

> Some smaller races are adept at sleight-of-hand and mundane trickery. Each purchase of this Knack adds one to the difficulty of all Perception rolls to notice the Koobald performing an illegal or nefarious act in plain sight: slipping poison into someone’s drink, filching an apple from a fruit stall, etc. This Knack can be purchased more than once, but no more times than the character’s Po.
    