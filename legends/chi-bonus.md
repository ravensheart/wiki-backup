---
title: Chi Bonus
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Martial Arts Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

A Martial Artist Style Master gains a Chi bonus equal to the number of [Shido](/legends/shido) he has. On rolls where it applies, the Martial Artist gains a number of automatic successes on the roll equal to his Chi bonus. All Styles specify two attributes for which a Style Master of that Style gains his Chi bonus on all rolls once he becomes a Master. Further, the character's Chi Bonus applies to all [In-Style Rolls](/legends/in-style-rolls) the character makes for all Martial Arts. If the character would gain a Chi bonus to the same trait from multiple different styles, these bonuses stack. For example, if a character who was a Style Master for two different styles that add to Agility used a Jutsu from a third style that called for an Agility roll, he would add three times the number of Shido he had as automatic successes on that roll.
    