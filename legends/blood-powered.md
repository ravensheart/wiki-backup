---
title: Blood Powered
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Source of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Blood:_ Blood is a potent source of magic for those who use blood magic. Each hit point sacrificed is worth 2 energy, and most forms of blood magic allow you to use the hit points of others, so long as you are touching blood that is continuous with their heart. Many forms of magic also allow blood sacrifices, which can provide a LOT of energy for magic. Specifics vary per magic, but in general the amount of energy gained is twice the product of the sacrifice’s souls.
    