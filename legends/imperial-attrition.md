---
title: Imperial Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Attrition Deal Variant
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Imperial Attrition is a much longer game. Each deal the number of cards dealt to each player changes, starting at 3 and increasing until the hand size is such that the entire deck is dealt; this is known as the "peak". Then hand size decreases again until each player gets 3 cards again. Depending on the number of players and which suits are being used, this can be a very long game.
    