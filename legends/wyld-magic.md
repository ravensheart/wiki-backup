---
title: Wyld Magic
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Primordial Skill
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Wyld Magic is highly unpredictable, unstable and dangerous, to the character and to everyone around him. It is, however, also very powerful, and quite easy to master. It is a unique form of magic that cannot be mastered. Skill level is limited to 4x the character’s Hun.

When a Wyld Mage creates an effect (Wyld Magic has nothing as organized or predictable as spells) the GM determines the effects' level, using the guidelines below. Once the GM decides the effect level the player must decide to proceed or do something else. If the player proceeds, the GM rolls a number of dice equal to twice the effect level to gain a number called the Wyld Shear, which is equal to the number of successes on those dice. The player then rolls Wits + Wyld Magic + the SUM of his Hun talents at a Difficulty equal to this Wyld Shear. With 10 or more successes, the player can choose to produce more potent effects, including permanent magic, large areas of effect, and anything else the GM approves. This should be as spectacular as it is uncommon. He also has total and precise control over the effect of the spell (within the limits set by the GM), and accumulates no Wyld Shear. With 5 or more successes, the exact effect he was attempting to produce happens, and he accumulates an amount of Wyld Feedback equal to the Wyld Shear minus his number of successes. With less than 5 successes, the character accumulates Wyld Feedback as above and an effect that closely resembles the intended effect occurs (perhaps lightning instead of a fireball). If the character failed by less than 5 successes, the effect is twisted, as above, and he accumulates Wyld Feedback equal to the Wyld Shear plus his margin of failure. On a failure by 5 or more, an effect goes off that vaguely resembles the intended effect (perhaps the target of his fireball spell is instead attacked by a tiger), and the character accumulates Wyld Feedback as above. On a botch, the character suffers a Feedback Eruption, below. Effects cannot be trained as techniques, and no result can reduce the characters Wyld Feedback total other than a Wyld Eruption, even if his margin of success should be greater than the Wyld Shear. All Wyld Effects take one 3-Trice action to cast.

Every time the character casts a Wyld Magic effect, after he rolls and the Wyld Feedback has been determined, the GM should roll a single die. On a 4-6, nothing happens. On a 1-3, the character suffers a Wyld Eruption. The GM should take his current Wyld Feedback and interpret it as either a fatigue pool or divide it by 3.5 and treat it as a Wyld Effect of that tier. This effect should be bad for the character, and more maliciously so the higher the character's current Wyld Feedback total. It should be more vicious if the Wyld Eruption is a result of a Critical Failure than if it occurs randomly. The good new is that, if the character survives the Wyld Eruption, his new Wyld Feedback total is 0.

Greatly succeeding with Wyld Magic is dangerous in and of itself. With great success, the character's confidence grows. Each time the character gains ten or more threshold successes when producing a Wyld Magic effect, he grows more confident and reckless with his magic. The first time this happens, the character gains the Fatal Flaw: Use Wyld Magic Recklessly. A use of Wyld Magic is only considered Reckless if the Effect level is higher than the character’s Skill. Unlike most Fatal Flaws, the character can gain a mark on this as many times per session as he roleplays it. Experience gained from this can only be spent on Hun-related traits.

*   Changing one thing to another: +0
*   Creating something from nothing: +1
*   Destroying something utterly: +1
*   Per die of damage done per margin of success: +1\*
*   Thing created is totally unorganized (ball of fire, pile of dirt): +0\*
*   Thing created is partially organized (steel, a pile of potatoes): +1\*
*   Thing created is mostly organized (A meal, a sword): +2\*
*   Thing created is organized in great detail (a computer, a living being): +3\*
*   Effect reverses entropy (healing spells, repair spells, etc): +4\*
*   Effect changes a living being without damaging it: +2\*
*   Effect travels or involves travel through time, space or dimensions (teleport, timeport, etc): +2
*   Effect targets one person or a single hex: +0
*   Effect targets up to 10 people or an area with a radius up to 10 yards: +1
*   Up to 100 people or 100 yards: +2
*   Up to 1,000 people or 1,000 yards: +3
*   etc.
*   Per 100 points of summoned creature’s point value: +1
*   Effect has no lasting effects other than damage: +0
*   Effect lasts for less than a minute: +1
*   Effect lasts less than an hour: +2
*   Effect lasts less than a day: +3
*   Effect lasts less than a week: +4
*   Effect lasts less than a month: +5
*   Effect lasts less than a season: +6
*   Effect lasts less than a year: +7
*   Effect lasts less than a decade: +8
*   Effect lasts less than a century: +9
*   Effect lasts less than a millennium: +10
*   Effect is permanent: +11

Those modifiers marked with a \* are multiplied by both the area modifier and the duration modifier. For example, an effect that does 5 dice of damage times the margin of success to an area 10 yards across for 5 minutes would increase the Wyld Effect Level by 5\*1 (for the area) and 5\*2 (for the duration), with a total mod of +15.

Teleportation has NO range modifiers… but remember, only a success by 5 or more means you actually arrive where you wanted. Anything less than that means you arrive in the “general area”, and a failure could put you anywhere within the original distance of your starting point. For example, if you were teleporting 200 miles and failed by 3, it would be perfectly reasonable for the GM to put you ANYWHERE within 200 miles of your starting position.

Random Wyld Feedback! (optional: the GM can always opt to specify the effects of Wyld Feedback if he is feeling inspired/cruel)

First, determine what college manifests. Pick one you don’t want to deal with today (default: pick the one that caused the roll). Use a d6 to randomize amongst the others: Blasties, Summoning, Illusion, Headfuckery, Metamagic, Transformation, Conjuration. This is the college that manifests. Next, determine how many effect points you have. Divide the total Wyld Feedback of the Wyld mage in question by 3.5 and round up. Grab that many d6. These are your Eruption Dice. Do damage? Roll your Eruption Dice. Each one that rolls a 3 or under is going to go toward damage; set those aside.

MOAR TARGETS? Roll your remaining Eruption Dice. Each success is an extra target. Set those aside.

Make a thing? Roll your remaining Eruption Dice. Set the good ones aside.

Splode you long time? Roll remaining Eruption Dice. Yadda Yadda.

Make Big Boom? All your remaining Eruption Dice go to [AoE](/legends/aoe).

The Wyld Eruption does a number of dice of damage equal to the Eruption Dice you set aside in the Damage phase. If you have more than twice the number of [AoE](/legends/aoe) dice than Damage dice, divide [AoE](/legends/aoe) by Damage dice and look that up in the Area Effect, above. If not, each [AoE](/legends/aoe) die simply converts to a radius of one yard (so 3 [AoE](/legends/aoe) dice = 3 yard radius). Randomly determine a number of targets from those present equal to the number of dice you got on Targets. The number of dice you allocated to making things determines complexity of the thing, as per Wyld Shear table. Same with duration.
    