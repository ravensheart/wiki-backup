---
title: Empowering Magic Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Form of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Empowering Magic:_ Empowering Magics give the character new ways to earn experience, and frequently allow him to use that experience to purchase Knacks and traits that he would not normally have access to.
    