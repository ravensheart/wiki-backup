---
title: Soul
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Concepts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Ever person in Raven's Heart has four governing Souls. They are the Hun, Alma, Khat, and Po. Each of these souls represent the slices of the cardinal elements that the gods mix together in the arts of celestial alchemy to guide destiny, Although the gods are hardly the only force that guides the trajectory of a beings life. Kismet, Destiny, Will, and Coincidence are all cosmic forces that vie for control of the souls of all mortal and immortal beings, which are the currency of the [Destinies Gambit](/legends/the-destinies-gambit).
    