---
title: Opposing Magics
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Opposing Magics use opposed mana. Where one mana is high, the other is low.
    