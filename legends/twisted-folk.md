---
title: Twisted Folk
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Goblins
=======

Cost

8 Experience

Prerequisites

none

> Some smaller races are adept at sleight-of-hand and mundane trickery. Each purchase of this Knack adds one to the difficulty of all Perception rolls to notice the Koobald performing an illegal or nefarious act in plain sight: slipping poison into someone’s drink, filching an apple from a fruit stall, etc. This Knack can be purchased more than once, but no more times than the character’s Po.

Cost

4 experience

Prerequisites

none

> Some species have a sense of smell that a bloodhound would envy. Each level of this Knack grants the character an automatic success on all smell and taste related Perception checks, and can be taken up to as many times as the character’s Perception.

Cost

16 Experience

Prerequisites

> The smaller races have a knack for hiding in the shadows. Each level of this Knack gives the character an automatic success on all rolls to stay hidden or to prevent someone from tracking them. It can be taken no more times than the character’s Po.

Orcs
====

Orcs usually have the [Dark God Virtues](/legends/dark-god-virtues).

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.

Cost

4 experience

Prerequisites

> You are at home in dim light, such as that provided by the stars or a dim torch. You can see five times as far using a single light source as a normal human, and see by starlight just as well as a human does in full daylight. A character with both this Knack and Home in Darkness suffers no penalties is full daylight.

Cost

16 experience

Prerequisites

> Some races are tough to kill. Each level in this Knacks adds one to the character’s effective Health and Khat for the purposes of calculating his Hit Points, and only for that purpose. This Knack can be purchased multiple times, though no more times than the character’s Khat.

Trolls
======

Trolls usually have the [Dark God Virtues](/legends/dark-god-virtues).

**Biggun**  
Cost: 8 experience  
Some creatures just grow big. Each level of this knack reduces the training time needed to increase the character’s Khat by half. This Knack can be taken multiple times, up to once per level of the character’s Hun.

Cost

16 experience

Prerequisites

> Even the most serious injury, if it does not kill you, can be overcome. Any Crippling injury heals once all of the Hit Point loss associated with its infliction is healed. Amputations require more potent solutions.

General Twisted Folk Knacks
===========================

**Terrible Hunger**  
Cost: 16 experience  
Twisted Folk are notorious in many ways. One of those ways is that they are known is that they are known to eat the flesh of others. A character with this Knack may consume the flesh of a person, and that flesh will sustain them for a full day for every level that person had in Khat. Characters with this knack never get sick from eating the flesh of people. Freshness does not matter, though a corpse that has decayed to the point at which it has no flesh cannot be eaten.

Cost

8 Experience

Prerequisites

> Some creatures are just better at resisting the Void than others. Each purchase of this Knack gives the character an extra die for the purpose of resisting Void effects, including most poisons and toxins and all Rude Arts. It also subtracts one from the die pools of any Rude Artist attempting to target the character directly with his arts. This can be purchased multiple times, but no more than the character’s Khat. Its effects stack.
    