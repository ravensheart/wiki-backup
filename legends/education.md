---
title: Education
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Rude Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Agents of the Machine](/legends/agents-of-the-machine)
*   [Agents of the State](/legends/agents-of-the-state)
*   [Agents of the Void](/legends/agents-of-the-void)
*   [The Wracked](/legends/the-wracked)
    