---
title: Mathematics
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosopher
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The world obeys rules, and some of those rules are inviolate. Mathematics is the study of those rules.
    