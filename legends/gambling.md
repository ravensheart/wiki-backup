---
title: Gambling
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Shadow
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Everyone gambles, all the time. Gambling is the skill in gaming, being able to participate in games of skill and chance and, usually, coming out on top.

_A Game of Power (Convolution, -2 or more):_ This technique allows you to gamble with actual character points. You cannot gamble more character points than twice your Shadow talent level, and you are at a -2 penalty to your skill for every character point you are wagering. Only one person at the table need be skilled enough to seal the wager, and only that person needs to make a roll to do so. It is assumed that being caught cheating means you forfeit the wager, and the terms of the wager must be explicit to all parties involved, and all parties must consent to the wager.
    