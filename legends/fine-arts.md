---
title: Fine Arts
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The Primordial Fine Art is [Connoisseur](/legends/connoisseur).

*   [Balance](/legends/balance)
*   [Control](/legends/control)
*   [Creation](/legends/creation)
*   [Enlightenment](/legends/enlightenment)
    