---
title: Kenso
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Motgo
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Motgo Hermit, Motgo Monk
========================

placeholder

### Consecration or Dedication

spend 6mo in solitude, speak only to those who initiate the connection, vegetarian diet, chastity, no mild-altering substances (classic ascetic behaviors). Then, spend 6mo in the city with the same restrictions. Buy off all Temptation Fatal Flaws in this time

### Covenant

gain no further Fatal Flaws (Temptation), live life of modesty

### Attunement

holy bonus against Temptation Ordeals

### Gift

Oden Knacks

### Atonement

placeholder
    