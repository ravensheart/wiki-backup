---
title: Craft: Tapestry
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Artificier
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The Craft of Tapestry revolves around making the durable goods needed to live. It includes clothing and shelter, shoes and utensils, bags and hats. But it also includes making armor, and can be used to make weapons that are also tools, such as knives and axes. Craft: Tapestry is used to make all of the tools used by all of the other crafts, and so nearly all Artificiers have some skill in it.

_Craft Armor (Intellect, -2 to -6)_: This technique allows the character to make mundane armor. The difficulty of the technique is -2 per AR of the armor. One roll per hour can be made at a difficulty equal to the AR of the armor. Five times that in successes is required to make a suit, but successes after difficulty are multiplied by the characters Calling level in Artificier (or 1/2, if he has no levels in that calling).

_Improve AR (Intellect, -4 per AR)_: This technique allows you to improve the AR of armor beyond the normal limits. The penalty for this technique is equal to the difference between the desired final AR and the mundane AR of the armor (not counting enchantments). Improving the AR of a piece of armor has difficulties and times similar to that of making the armor in the first place.

_Reduce Mobility Penalty (Intellect, -4 per MP)_: This technique allows you to reduce the Mobility Penalty of a piece of armor. It is at -4 per difference between the natural MP and the desired MP, not counting enchantments. The difficulty of this roll is the difference in MP, and the number of successes needed is five times that. Each roll takes an hour, and the character's Artificier talent multiplies the successes before applying them to the number needed (no talent means the successes are multiplied by 1/2).

_Work With Adamant (Intellect, -8)_: This technique allows the Artificier to work with the hardest of metals, Adamant. A base roll on the Technique will allow him to forge normal Iron into Adamant, generating up to his Calling level in pounds per hour and consuming twice that in raw iron. This technique must also be employed whenever doing anything else with Adamant, potentially making many projects impossible until the character familiarises himself sufficiently with the difficult metal.

_Work With Mythril (Intellect, -6)_: This technique allows the Artificier to work with one of the most fantastic of metals, Mythril. A base roll on the Technique will allow him to forge normal Platnum into Mythril, generating up to his Calling level in pounds per hour and consuming twice that in raw iron. This technique must also be employed whenever doing anything else with Mythril, potentially making many projects impossible until the character familiarises himself sufficiently with the difficult metal.
    