---
title: Reservoir of Will
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Will Style Master Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Reservoir of Will
-----------------

Cost

\-

Type

Stackable Permanent

Requirements

Master of a Will Style

Prerequisites

none

> Your Will has greater depths than most men. Add one to your effective Hun and Will for the purposes of calculating Willpower, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Rating; Mana Reservoir and Reservoir of Will do not stack.
    