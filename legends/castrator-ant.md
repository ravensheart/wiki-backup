---
title: Castrator Ant
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Melee Style, Perception Style, Strength Style, The Black Spiral
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Default

Castrator Ant Core Cascade
==========================

Castrator Ant Master Jutsu
==========================

Spirited
--------

Cost

\-

Type

Stackable Permanent

Requirements

Style Master

Prerequisites

none

> You are more sure of yourself and your convictions than most. Add one to your effective Khat Virtue and your effective Alma for the purposes of calculating Certainty, and only for that purpose. This Jutsu can be purchased multiple times, though no more times than the character’s Path Talent.

Penetrating Strikes
-------------------

Cost

\-

Type

Stackable Permanent

Requirements

Strength Style Master

Prerequisites

none

> Your fists and style weapons bite deep into the flesh of your enemies. You ignore one level of Armor your opponent has per level in this Jutsu when making attacks with in-style weapons. You can have multiple levels of this Jutsu, but no more than your talent level in the Path the style belongs to.
    