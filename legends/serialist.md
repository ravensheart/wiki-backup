---
title: Serialist
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Primordial Skill
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

In order to perform a Murder, the Serialist must personally interact with a target, forcing them to bend to their will using some form of force. The Serialist can then make a Guile + Serialist roll to finish their murder.

The Serialist can gain up to his total Rude Arts Talent level times his stunt level per kill if the theme and victims are stunted well. The arch finishing reward can be the cumulation of the “unearned” experience from the early murders. I.E, one could do Earth, Metal, Fire, Water, Wood, and get 5, 10, 15, 20, and 25 exp for the murders themselves, and a bonus at the end of 50 for finishing the arc (total of 125, or 25 \*5). This is in addition to any normal experience the character would normally get for killing that particular victim. Note that earning more than 5 experience for a kill requires truly epic stunting and consideration. Each of the murders in Seven would have earned that Serialist only 5 experience, while Saw wavers between 3 and 5 per kill. 10 experience is a novel-length story where it is revealed in the end that every moment in the book was actually working toward the death of a single character.

Each murder must be personal and premeditated. One cannot simply poison the well of a town and claim to have murdered everyone that drinks from the well. Murders can be categorical; a Serialist might lock a bunch of policemen in a cell deep underground and let them starve to death as an ironic comment on how they spent their lives incarcerating others, but categorical murders grant no more than 15 experience per kill. To earn more than 15 experience, the Serialist must make each murder highly personal and specific.

As a side effect of their deadly prowess, Serialists can also make a Serialist roll to ignore the AR of a restrained target.

Experience gained in this way can be spent on attributes, skills that the character knows or his victim knew, Attributes, Souls, and of course any Rude Art. It can also be banked and spent like normal experience.
    