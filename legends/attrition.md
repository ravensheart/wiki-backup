---
title: Attrition
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Card Game
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Attrition is a common card game in Raven's Heart for 2-8 players (though some variants can have as many as 23 players in the early game). The standard, or elemental, variant is played with a 65 card deck. The deck has 5 suits, and 13 cards to a suit. The five suits correspond to the five elements, and the thirteen animals are the thirteen constallations. In the standard game, each suit has one trump suit: Wood trumps Earth, Earth trumps Water, Water trumps Fire, Fire trumps Metal, and Metal trumps Wood.

Dealer shuffles and deals six cards to each player, starting with the player on the left (this can be important for some variants). The player to the dealer's left initiates a Challenge by playing a card from his hand. Play passes to the left, with each player responding to the challenge with a card in their hand. When responding to a challenge, the player must follow suit if they have any cards of that suit in their hand. If the do not, they can respond with a card from any other suit. All players must respond to every Challenge; it is not optional.

If no trumps were played, the player who played the highest in-suit card wins the Challenge. If any trumps were played, the highest trump wins instead. In the standard variant, only the trumps of the original suit are considered trumps; all other suits are trash, and always lose.

The winner of the Challenge puts his winning card in his Bank and the rest of the cards from the Challenge into his hand. Anyone who has no cards left in their hand has been eliminated. The winner then initiates another Challenge, and play continues in this manner until all but one player has been eliminated. The winning player then gets one point for every card in his bank and in his hand, and deal passes to the right.

A number of minor variants of this game exist, from whether the cards in one's bank are kept face up or face down to who deals first to hand-size variants. The most common variant is a simple gambling variant where all players put some amount of money in the pot when the game begins, and the first player to reach a certain number of points (usually 100) wins the pot. Another basic variant has each point won in a game representing some amount of money from the pot, distributed when the deal ends. Play continues until all the money is won. Below are listed more major variations on the game.

**High Magic Animals**

13: Dragon  
12: Tiger  
11: Ox  
10: Allicorn  
9: Horse  
8: Wild Boar  
7: Monkey  
6: Crane  
5: Dog  
4: Rooster  
3: Ferret  
2: Cat  
1: Snake  

Suit Variants
=============

**Cardinality Attrition**

In this Attrition variant, there are four suits, not five. Crystal trumps Light, Light trumps Tapestry, Tapestry trumps Void, and Void trumps Light. It is less common, but still widely played.

**Cross-Trump Attrition**

Cross-Trump Attrition allows a player to Trump from a secondary suit. For example, if the Challenge is in the suit of Wood and someone has already played a Metal card, the player can play a Fire card to trump the Metal, but only if he has no Wood and no Metal cards in his hand. This can theoretically go all the way around to Earth being the final trump. Crystal Cross-Trump also exists, and functions in the same way.

**Double Trump Attrition**

In this variant, each suit has two trump suits. Metal trumps Wood and Earth, Earth trumps Fire and Water, Fire trumps Wood and Metal, Wood trumps Water and Earth, Water trumps Metal and Fire. If two trump suits are both played on the same play, one will trump the other, and that one wins the Challenge.

**Embodiment Attrition**

Some decks have four additional cards: The True Dragon, The Titan, The Immortal, and The Anathema. These serve as ultimate trump cards, always winning a Challenge except when facing one another. The True Dragon wins against the Titan, who wins against the Anathema, who wins against the Immortal, who wins against the True Dragon. Should a cross-pair show up in a single Challenge, it is known as a Reckoning, and all cards in the Challenge are discarded, and the winner of the previous Challenge initiates again.

Deal Variants
=============

**Imperial Attrition**

Imperial Attrition is a much longer game. Each deal the number of cards dealt to each player changes, starting at 3 and increasing until the hand size is such that the entire deck is dealt; this is known as the "peak". Then hand size decreases again until each player gets 3 cards again. Depending on the number of players and which suits are being used, this can be a very long game.

**Short Attrition**

A quicker version, or one for many players, Short Attrition only involves dealing three cards to each player.

Play Variants
=============

**Attrition Oroboros**

In this variant, the Snake isn't the weakest card all the time, and it is known as Oroboros. Within the same suit, Oroboros beats Ox, Tiger and Dragon (11, 12, and 13; Jack, Queen, and King). Unless one of those cards are in play, Oroboros is still beaten by all other cards. However, if one of those other cards (of the same suit) are in the Challenge, then The Round card beats them and can only be beaten by a Cat, Ferret, or Rooster. For example, if an Oroboros, a Monkey (7), and a Hores (9) were in play, the Horse would win. If the next player played an Ox (11), the Oroboros would be winning. If the next player played a Rooster, that would then be the winning card (assuming everything is in suit).  
When this variant is used with the Face Point Value, frequently the Oroboros is worth more than the 1 point that it would be normally. Common values are 10, 13 and 14 points. It is frequently called the "little dragon" in such games.

**Face Point Value Attrition**

In this Attrition Variant, cards are worth more or less depending on their face value, and sometimes suit. Usually, this is juts that each card is worth its face value, but sometimes there is a "gold suit" which is worth twice what the others are.

Gambling Variants
=================

**Fixed Point Value Attrition**

This Attrition Variant is usually only seen in casinos and other organized gambling halls. In it, each player agrees to pay the winner a fixed amount of money per point they win in any hand they win. Frequently, the "house" will have a skilled player participating on their behalf.

**Hand Betting Attrition**

In this Attrition variant, each player puts up and ante before the cards are dealt. Then another round of betting (similar to Poker) is done after the hands are dealt and everyone sees what cards they have. Players who fold simply discard their cards. The winner of the hand takes the pot.
    