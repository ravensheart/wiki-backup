---
title: Shocking Glance
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Breath of the Dragon Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Shocking Glance
---------------

Cost

\-

Type

Free Instant

Requirements

Breath 1, Scales 1

Prerequisites

none

> You project the power of the Dragon through your eyes at an adversary. Your opponent must immediately save against Fear or flee. You may activate this Jutsu at any time, even when it is not your action, but you may only use it against a given opponent once per month, and you may not activate it more than once per action.
    