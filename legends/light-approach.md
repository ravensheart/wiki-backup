---
title: Light Approach
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosophy Technique
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Light Approach (Intellect, -8)_: You have studied the Light approach to philosophy. You can use this technique to understand something from the perspective of growth, power, and glory. Or, for that matter, the perspective of Dragons and Mages.
    