---
title: Void Magic
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  Abyssal
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Void Magic [opposes](/legends/opposing-magics) [Platonic Magic](/legends/platonic)
    