---
title: Blade Seeks Blood Understanding
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Wind Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Blade Seeks Blood Understanding
-------------------------------

Cost

2+ w

Type

Quick Stackable Instant

Requirements

[Conspiracy of Meaningless Fates](/legends/conspiracy-of-meaningless-fates)

Prerequisites

Fate 1, Wind 2

> The blood of mortals calls to its ultimate fate, yearning to be freed from the fragile shell containing it. Your blade is merely the path to enable this fate. This Jutsu represents an attack with a normal thrown weapon. This attack does double damage after armor for 2 willpower. Every additional 2 willpower adds one to this multiplier, though the multiplier cannot be higher than the Martial Artist's Path of Fate Talent level + 1. This stacks multiplicitively with the Vital Strike Thrown technique.
    