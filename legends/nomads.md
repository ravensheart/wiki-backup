---
title: Nomads
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Humans
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Nomads are shorter than Planesmen, darker of skin and hair color, and usually have brown eyes. Nomads frequently have the [Silent Virtues](/legends/silent-virtues)

Cost

8 Experience

Prerequisites

none

> Some smaller races are adept at sleight-of-hand and mundane trickery. Each purchase of this Knack adds one to the difficulty of all Perception rolls to notice the Koobald performing an illegal or nefarious act in plain sight: slipping poison into someone’s drink, filching an apple from a fruit stall, etc. This Knack can be purchased more than once, but no more times than the character’s Po.
    