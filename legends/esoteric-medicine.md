---
title: Esoteric Medicine
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Mystic
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill that studies the flows of Chi through the seats of the souls, and how those interact with the Chakra of the body, and how interruptions of those flows can cause diseases and sickness.
    