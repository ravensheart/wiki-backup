---
title: Humans
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Species
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Humans are losers and you shouldn't play one.

Daikini
=======

Daikini frequently have the [Old God Virtues](/legends/old-god-virtues)

**Biggun**  
Cost: 8 experience  
Some creatures just grow big. Each level of this knack reduces the training time needed to increase the character’s Khat by half. This Knack can be taken multiple times, up to once per level of the character’s Hun.

Cost

16 experience

Prerequisites

> It is a harsh world, and sometimes one must be harsh back. Each level in this Knack adds one damage to all of the character’s Karate and Melee attacks. This Knack cannot be taken more times than the character’s Po.

Nomads
======

Nomads are shorter than Planesmen, darker of skin and hair color, and usually have brown eyes. Nomads frequently have the [Silent Virtues](/legends/silent-virtues)

Cost

8 Experience

Prerequisites

none

> Some smaller races are adept at sleight-of-hand and mundane trickery. Each purchase of this Knack adds one to the difficulty of all Perception rolls to notice the Koobald performing an illegal or nefarious act in plain sight: slipping poison into someone’s drink, filching an apple from a fruit stall, etc. This Knack can be purchased more than once, but no more times than the character’s Po.

Planesmen
=========

Planesmen are tall, usually light of skin and hair, with blue or green eyes. Planesmen frequently have the [Court of Heaven Virtues](/legends/court-of-heaven-virtues)

General Human Knacks
====================
    