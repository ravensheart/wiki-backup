---
title: The Chronicle of Sedulous Ember
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Diary of Embers
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Chronicle of Sedulous Ember
===============================

A powerful master of martial arts was among these siblings. He defended with ease his siblings when they faced physical danger. Rejoiced at the prospect of physical conflict and never shrunk from a chance to befriend or train under an opponent who he defeated or who defeated him. His excellency in the martial arts was forced upon him by times of conflict against the Whoevers. But like a diamond that is forged deep in the earth and cut to a brilliant diadem. So the skill of Tekua of Sedulous ember rose. He never claimed mastery and never pursued any form of recognition for his teachings or bravura, Save for the the pluck of his students whom he each cherished as a father does his own children. Tekua spent most of his days on the mats of his dojo. Teaching his craft with his own hands and leaving his unique fingerprint upon each of his students, each of whom bears this signature mark.

When thier master passed. each bearer of Tekua's Fingerprints created part of a kata that would serve as a bond to those who were true to their masters teachings. This kata is was a combative complex that required great skill to execute. The Fingerprint Kata and Mark to this day marks the authenticity of those who have learned from a true master.
    