---
title: The Chronicle of Scarlett Embers
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Diary of Embers
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Chronicle of Scarlett Ember
===============================

There Once was a sister to clockwork who never saw beauty in function. Never felt at home where anything was predictable. Who only loved the world when it vexed and amused, lied and shifted. Like a small child who has more confidence than a towering warrior, But less the knowledge of all things in the world. In this there is a wisdom only known to those who relinquish the deathgrip that we all have to coherence and security.

She never needed a reason to create anything, instead she relished in letting what beauty came to her in the act of creation take her to whatever meaning it might have to herself or others in the wider world. Her creations always led those who experienced them to have some higher understanding of purpose. The meaning behind the work, the message it told, to moral of the story, or the emotions the music evoked were wholly secondary to the miracle of the art itself.

Even on her deathbed she couldn't have told you what mystery her life had solved. Even though the entire length of it was spent unravelling a single mystery that she admitted she failed to comprehend its entirety, and those closest to her attested that she died more fulfilled than anyone they had ever known.

Such was the life of Kharmine of Scarlet Ember, so shrouded in the mystery of creation and beauty that she vanished into it.

She vowed to never take credit for the beauty she created and she credited any morsel of success she had to her love of the craft of creation. In her last breaths she uttered these words to her followers:

“There is beauty in pain and suffering that allow you to savor it like a fine wine. The dust of the field and the breath of the winds have as many succulent flavors as the finest wines. If only you learn to taste of them.”
    