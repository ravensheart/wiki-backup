---
title: Combat
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Control
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Karate](/legends/karate)
*   [Marksman](/legends/marksman)
*   [Melee](/legends/melee)
*   [Thrown](/legends/thrown)
    