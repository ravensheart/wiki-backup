---
title: Deep Lore
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Philosopher
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Deep Lore is the study of hidden secrets of the world. It is the skill one would need to roll in order to know the truth behind the myths of Creation, or why Fae fear Iron.
    