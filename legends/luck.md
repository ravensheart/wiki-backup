---
title: Luck
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  General Knacks
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Cost

4 experience

Prerequisites

Alma 2+

> Fortune seems to favor some people. For every level of this knack, the character can either reduce any die-pool penalty he is experiencing by 2 or reduce the difficulty of a roll by 1, to a minimum penalty of -0 and a minimum difficulty of 1. This applies to all rolls, but won’t help if there is no difficulty or penalties associated with the roll. This also does not function against active opposition; technically, the opponent’s Dodge, Armor, Parry, and SRD all act in a manner similar to a difficulty on a roll, but none of these can be reduced by Luck. Levels in Luck can help the character pull off hard Techniques that he has not yet learned completely, but he still cannot attempt techniques whose base penalties are higher than his skill, and Luck doesn’t help with avoiding Spellshear. It can help mitigate penalties that accrue due to the use of magic, however, such as those suffered by Linguists and Spellmasters. Luck cannot be taken more than once for every two points of the character’s Alma.
    