---
title: Divine and Infernal Magics
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Celestial
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

There are a number of magics that fall under the category of Divine or Infernal. Every pantheon and every independent god provides some power to their worshippers, and every one of these is categorized as Divine or Infernal, or sometimes both. The magic granted to priests is a Charisma Based Ritual Magic. Beyond this, any god may designate someone whose Fate he owns as a Prophet. This is, in itself, a form of Divine or Infernal magic, though it is Granted rather than Ritual. The distinction between Divine and Infernal is that Divine Magic is Warp-Powered, while Infernal Magic is Blood-Powered.

Each Pantheon and independent God gives their own form of Power Investiture, the Magery for their particular kind of magic. This is the same for Profits and Priests, but normally a character cannot have more than one kind of Power Investiture; a man can serve only one master. There are exceptions, but they are extraordinary.

Pantheons
=========

*   [Circle of Gifts](/legends/circle-of-gifts)
*   [Council of Embers](/legends/council-of-embers)
*   [The Cult of the Dark Gods](/legends/the-cult-of-the-dark-gods)

Independent Gods
================
    