---
title: Species
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  TableOfContents
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

There are a great number of Races, separated into Species, in Raven's Heart. Most Species have three Races. Mechanically, the main difference between the different Races are the Racial Knacks they have access to. Each Race has a number of story implications as well, as detailed in the description of each race. Such will also vary geographically, as Sky Elves from one garden are not always the same as Sky Elves from another.

*   [Browen](/legends/browen)
*   [Dragonkin](/legends/dragonkin)
*   [Dwarves](/legends/dwarves)
*   [Elves](/legends/elves)
*   [Fae](/legends/fae)
*   [Giants](/legends/giants)
*   [Greatfolk](/legends/greatfolk)
*   [Humans](/legends/humans)
*   [Twisted Folk](/legends/twisted-folk)
*   [Wildlings](/legends/wildlings)
    