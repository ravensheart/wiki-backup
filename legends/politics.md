---
title: Politics
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Rude Arts
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Memetics](/legends/memetics)
*   [Shadow](/legends/shadow)
*   [Statesman](/legends/statesman)
*   [Viromancy](/legends/viromancy)
    