---
title: Pu
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Alma Virtue, Taoist Virtue, Virtue Resists Callousness
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Pu is one's connection to the Tao, his understanding of Wu Wei. A character can always channel Pu when attempting to know when the right moment is to act, when attempting to accomplish an action with the minimal amount of effort, and when trying to discern the best course of action in a complex situation or follow their intuition to discern the best action for all. This includes all Prayer rolls for guidance. A high Pu character must fail a virtue roll in order to act rashly or against his better judgment. Pu is used to resist Callousness.
    