---
title: Performer
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Creation
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Acting](/legends/acting)
*   [Dancing](/legends/dancing)
*   [Musical Performance](/legends/musical-performance)
*   [Storytelling](/legends/storytelling)
    