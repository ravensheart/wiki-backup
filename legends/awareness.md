---
title: Awareness
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Natural Athlete
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the measure of how aware a character is of their surroundings. It is important for Join Battle, and to prevent sneak attacks.
    