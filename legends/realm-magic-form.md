---
title: Realm Magic Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Form of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Realm Magic:_ Realm Magics are similar to Word Magics, except that there are usually fewer divisions and a mage can usually create an effect with just one Realm. Additionally, the mage must buy levels in Realm Knacks, which allow him to create the magic effects necessary. Innate Magic: Innate Magics allow the character to grant himself and others Knacks at the expense of experience.
    