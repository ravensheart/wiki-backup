---
title: De
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Po Virtue, Taoist Virtue, Virtue Resists Fear
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

De represents the character's dedication to doing what is right and to not harming those he comes across without good reason. A character can always channel De when attempting to bring Creatures of Darkness to justice, or to re-integrate them into creation. He can also channel De when defending the weak or punishing those who prey on people weaker than themselves. A high De character must fail a De roll in order to take advantage of another for personal gain, to abuse or allow someone else to abuse the weak, to engage in mindless destruction or slaughter, or to stand by while injustice prevails. De resists Fear.
    