---
title: Cartography
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Explorer
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the skill of making maps of new, unexplored areas, as well as your ability to read maps others have made.
    