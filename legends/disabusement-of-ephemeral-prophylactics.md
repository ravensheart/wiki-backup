---
title: Disabusement of Ephemeral Prophylactics
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  The Wind Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Disabusement of Ephemeral Prophylactics
---------------------------------------

Cost

2+w

Type

Quick Stackable Instant

Requirements

[Conspiracy of Meaningless Fates](/legends/conspiracy-of-meaningless-fates)

Prerequisites

Fate 1, Wind 2

> The protection that armor provides is an illusion; no mere metal or skin can protect one from fate. This Jutsu allows the Martial Artist to ignore one level of his opponent's AR for every 2 willpower spent, up to his Path level in AR.
    