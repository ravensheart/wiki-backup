---
title: Craft: Light
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Artificier
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

The Craft of Light is the craft that governs all things to do with the keeping and dissemination of knowledge. It includes calligraphy, book binding, paper making, ink making, and the like.
    