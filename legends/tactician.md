---
title: Tactician
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Control
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

*   [Drill](/legends/drill)
*   [Intelligence](/legends/intelligence)
*   [Leadership](/legends/leadership)
*   [War](/legends/war)
    