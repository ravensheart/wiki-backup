---
title: ToDo
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

 [Update the Splash Page](/legends/update-the-splash-page) 

 [Backend server](/legends/backend-server) 

 [Front end](/legends/front-end) 

 [Add weapons to the wiki](/legends/add-weapons-to-the-wiki) 

 [race template](/legends/race-template) 

 [knack template](/legends/knack-template)
    