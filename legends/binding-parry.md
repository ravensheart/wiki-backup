---
title: Binding Parry
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Thousand Voiced Silence Core Jutsu
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Binding Parry
-------------

Cost

2 w

Type

Free Persistant

Requirements

Road 1, 1k Silence 1

Prerequisites

none

> As your opponent strikes, you move to the side and bind his weapon with rope. This jutsu can only be activated if you have rope in hand. Upon activating this Jutsu, roll Agility + Melee at a difficulty equal to your opponent's Agility + Alacrity static value. Success on this roll means you have Disabled the attacking limb or weapon; for weapons, this means you have disarmed your opponent, for limbs, you have bound them with rope. This lasts until the rope is cut loose or untied. Untying takes several minutes.
    