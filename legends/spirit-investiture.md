---
title: Spirit Investiture
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Terrestrial
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Spirit Investiture [opposes](/legends/opposing-magics) [Arcane Magic](/legends/arcane)
    