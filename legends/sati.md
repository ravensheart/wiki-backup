---
title: Sati
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Khat Virtue, Taoist Virtue, Virtue Resists Temptation
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

This is the character's freedom from distraction and separation from material desires. Sati can always be channeled to resist those who would bribe, cajole or threaten the character away from the way. A high Sati character must fail a virtue roll in order to spend time pursuing material gain, extremism or petty pursuits. A high Sati character must fail a virtue roll in order to totally abstain from sex as much as he must fail a virtue roll in order to spend a week doing nothing but. Balance is the key of Sati. Sati is used to resist Temptation.
    