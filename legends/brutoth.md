---
title: Brutoth
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Dark God
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Brutoth
=======

Brutal general of the Dark Gods, and lover to Lilith.

### Consecration or Dedication

The Dedication of Brutoth is to beat a person to death with your bare hands. You must kill them using no supernatural traits, and using only non-lethal damage. Those who dedicate themselves to Brutoth first gain dark red hair.

### Covenant

Brutoth does not have patience for those who cast themselves as victims. Any who lay down and die rather than fight back deserve their fate, and get no sympathy or aid from Brutoth or his followers. It is a violation of his Covenant to aid those who do nothing in their own defense.

### Attunement

Brutoth adds his Unholy Bonus to all combat actions and tactical evaluations. This only functions in actual combat; Brutoth does not give bonuses to training rolls.

### Gift

Brutoth gives the gift of Excision.

### Atonement

The oathbreaker must flay themselves. They must isolate themselves in some dark place underground with a sharp implement and do a total of 10\*Alma damage to themselves. Normally this requires multiple sessions, with rest for healing between. They cannot accept help or aid in any way during this Atonement.
    