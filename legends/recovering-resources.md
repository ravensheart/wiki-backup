---
title: Recovering Resources
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Basic Rules
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Beyond recovering resources from stunting, characters recover naturally as well. For every 10 minutes of rest and relaxation, the character can roll his Health and regain the successes in Fatigue. After each full night’s sleep, the character can roll his Hun Virtue and regain Willpower equal to the successes, and his Defiance to recover successes in Certainty. After a full day (24 hours) rest, a character may roll Will and, if the roll succeeds, recover 1 hit point. All of these recovery rates can be affected by supernatural abilities and various Knacks.
    