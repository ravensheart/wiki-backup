---
title: Acting
description:
published: true
date: 2021-06-09T19:26:33.378Z
tags: legends,  Performer
editor: markdown
dateCreated: 2021-06-09T19:26:33.378Z
---

Acting is your ability to perform a role on stage. This contrasts with your ability to actually fool people in real life... Regardless of the words of an old bard, life is not, in fact, a stage, and fooling people and acting are different skills.
    