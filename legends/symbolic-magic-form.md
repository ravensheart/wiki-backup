---
title: Symbolic Magic Form
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Form of Magic Power
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

_Symbolic Magic:_ Almost identical to Syntactic Magic, except that Symbolic Magic uses symbols rather than words. Many magics that are one are also frequently the other, but this is not always true, and as such the distinction can be important. Symbolic Magic must be written in order to be used, while Syntactic must be spoken. This means that what can prevent the character from casting can be different, but the magics function otherwise identically, except as specified for particular magics.
    