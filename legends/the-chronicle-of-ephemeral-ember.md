---
title: The Chronicle of Ephemeral Ember
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Diary of Embers
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

The Chronicle of Ephemeral Ember
================================

The eldest of the ember siblings was a philosopher who grounded and advised the lot of his siblings. At trying times in their lives each of them came to the eldest sibling for comfort and guidance, and through his life he was known only as Axumander.

Every follower of the Axumander has never written down his philosophies, preferring instead to have them passed down purely through oratorical means. But it is these itinerant scholars who hold the keys to many secrets that elude the most studious of magical scholars. I have elected to mark down what the monks of ephemeral embers I have met in my travels have called the greatest of Axumander’s secrets, the secret the now bears the name of the pantheon itself.

“Fire consumes the wood that it burns, Essential Metal destroys Wood, All the primal elements have related in the earliest days in only this way. Such is the way of things utterly bound to their power to change. Fire can burn bright and reach high at the expense of the wood, Fire is quenched by the Essential water completely. There is no room in their essence for anything but their consuming features. But there is something that only happens when the mixing of essential and mundane things occurs. In a world where there are things that are “non” essential, made of some other element beyond what these elements contain to consume one another.

As the world grows old, the fires bite dies and so rises a new order of things. A world where embers can rise from fire. Embers that rise higher and burn brighter than the Fire that consumed the wood. Embers of Wood in fire are made seemingly of both elements simultaneously. They embody the strengths of each of their parents.

In this world there is a natural order. A way of things that resembles this consuming relationship of these elements. But as time goes on, the greatest and most powerful of magics has either faded or sequestered itself away to parts of the world where its power can be undimmed.

The truth is that these five elements that we know as the bearers of nature are solely the domain of magic. Which is one of four elusive primordial elements. All the elements that we celebrate as the essence of the world and nature are borne of the sun as is all magic. This magic flows through the stars and into the land and our hearts. But people are not merely the domain of magic. And are in fact composed of four elements more fundamental than that, People are where all four of these primal essences mix and relate in the same consuming fashion that the 5 magical elements do, they each hold another in the palm of their hands. With complete dominion over one of them with the power to uplift or destroy what lies in their grasp, in the same way that fire can consume wood.

But an ember is something beyond all of these, For what is an ember but a beautiful analogy of the contradiction of this natural order. An Ember is what occurs when wood burns fire, When metal rusts the water. Such things are improbable but possible and precisely what characterizes some of the most intriguing and powerful people. Because people are the only ones capable of discovering and harnessing the power of the four primal cardinal elements. People are the brightest Embers.”
    