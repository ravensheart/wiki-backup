---
title: Kender
description:
published: true
date: 2021-06-09T19:26:33.379Z
tags: legends,  Browen
editor: markdown
dateCreated: 2021-06-09T19:26:33.379Z
---

Kender usually have very low virtues, and don’t pay them much mind in general. Nearly any virtue system is appropriate for a Kender, in that they are all equally inappropriate.

Kender Knacks
-------------

Foolhardy
---------

Cost

16 experience

Prerequisites

> Kender are either foolishly brave or bravely foolish. Either way, fear just doesn’t seem to be an idea that they really understand. Each level in this Knack gives the character a bonus success to resist Fear. It can be taken multiple times, up to the character’s Alma, and stacks.

Impressive Vocabulary
---------------------

Cost

8 experience

Prerequisites

> Kender are well known for their creative use of words, especially when angry. The Kender may roll their Wits + Fast Talk at a difficulty of their target’s SDR; this roll must be stunted, and the Knack does not function unless the player gets at least one stunt die. If they succeed, their target must spend a Certainty or attack them next round. Using this Knack in combat is a reflexive action, and if the target is already engaged in physical combat, he does not have the option to spend Certainty.

No invitation
-------------

Cost

8 experience

Prerequisites

> A locked door is an invitation, at least to a Kender. This Knack grants one automatic success on all rolls related to opening a locked portal, no matter the means. It can be purchased more than once, up to the character’s Po, and its effects stack.
    