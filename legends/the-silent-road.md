---
title: The Silent Road
description:
published: true
date: 2021-06-09T19:26:33.380Z
tags: legends,  The Celestium Sphere
editor: markdown
dateCreated: 2021-06-09T19:26:33.380Z
---

Those who follow the Silent Road have become instruments of fate, silent unseen assassins who remove those from the world who must not remain. They follow no law but the laws of the Gods, and answer to nobody but Iwo himself. Any who maintain the Higher Purpose to Slay all Enemies of the World for more than a month, unbroken, may spontaneously discover this path.

*   [Thousand Voiced Silence](/legends/thousand-voiced-silence)

The Silent Road Master Jutsu
============================
    