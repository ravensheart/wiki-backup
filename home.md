---
title: Welcome
description: Raven's Heart wiki
published: true
date: 2021-09-10T16:15:28.379Z
tags: welcome, home
editor: markdown
dateCreated: 2021-06-09T20:15:04.078Z
---

# Aaaaaand Welcome Back Everyone!!!
We are all so happy to be back working on *Raven's Heart*{.font-decadentia .text-lg} content, and we love the new platform! So please jump in!

> Please give our [Contributing](/Contributing) page a quick read.
{.is-info}

## News

**Attention all contributors**

Fairly soon we should all get together to take a look at the "Paths of Power" (P.o.P) which is the generic name of the 256 different skill trees that exist in Ravensheart and grade them based upon 4 different Criteria (#,#,#)

1. How aesthetically satisfying is this skill? 
- Does this skill have cool visual art or player RP opportunities.
- Unifed artistic identity is a huge component of RH's development future, Music and Art style help "sell" the importance of a characters skills and abilities


2. Does this skill have cool synergy opportunities with at least 3 other paths of power in other cardinalities?
- Synergy and unification is important to the mechanical identity of Raven's Heart. How likely is it that this P.o.P will supplement another PoP elsewhere. 
- Each of the paths of power needs a unique visual and mechanical identity that differentiates it from its partners. Most of the original PoP were created by D and were differentiated by what resource they used to "cast" thier effects and whether or not they were "threshold based, limit based, difficulty based, or what have you. While these are very important mechaincal distinctions there also needs to be an aesthetic distinction not only in visual style but in scope (Necromancers should probably be the only casters able to use blood as a resource) If all that visually distinguishies a Max level Excision master and a Max level Dragon Mage is how they pay for spell effects and what color the "sparks" are, there isnt any reason to do one over the other. PoP should be as much defined by the agency they provide as the resource mechanics that underpin them. 


3. Mechanistic Satisfaction? (tabletop and otherwise)
- How likely is this PoP to satisfy the type of player who would make it their main PoP. What does that player look like and can you describe and envision how they play in a sentance.
4. How important is this PoP to the identy of Raven's Heart as a unique fantasy world?


Please reach out to me whenever you can (or whenever I talk to you) when we can set aside time to compare notes on this. 



### Introducing Spoilers{.spoiler}
So when you have a spoiler paragraph on a page just add the `{.spoiler}` class after it in the markup, and now it is blurred until someone hovers over it with their mouse.{.spoiler}

![trans_aras_sigil.png](/trans_aras_sigil.png)It even works with pictures.
{.img .spoiler}

## Projects Currently are:  
- [ ] True Dragons and those whom it concerns go over old content and update it with new or include missing content.

- [x] Look into Embedding and updating complex tools (Plant/Weapon Gen'rtr, Maps/Detail Maps) ***this isn't going to work well - Gynn***

- [ ] Best practices article and conventions for filling out articles.
  - Standards for Lore/Places tags.
  - Standards for inclusion of First City/Farleague Campaign content. (this probably is a tag)
  - Certain articles are MUCH more important and precedent setting than others and will need to have a stricter code of construction (think Anakin Skywalker's Wookieepedia page), Sazafir/Callixtus/All Gods etc.
- [ ] Someone should take ownership of porting existing "Places" lore into the new wiki. 
  - This will involve a lot of digging through the old  Gdrive stuff. A good place to start with this is each of the towns in Valenthyne (or any other Duchy). Valenthyne's Cities have a bunch of really cool pictures for from the wonderdraft map. 
  - Verund's Garden Empires and cities also needs to be filled out.
- [ ] Someone should look into filling out the content for Valenthyne's cities with images from Wonderdraft map and other lore development.