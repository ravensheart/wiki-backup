---
title: Generic Page Template
description: Some small description of the page.
published: true
date: 2021-08-02T18:02:48.588Z
tags: template
editor: markdown
dateCreated: 2021-06-10T19:18:24.904Z
---

<audio src="/assets/mastering.mp3" controls autoplay></audio>

> **Gynn** - *WOW Jeremy, you are the coolest, and thank you for this awesome template!*  
> **Jeremy** - *Your welcome.*

![this doesn't show up unless the image is not there.][1]
This image is on the right!
{.img .right}

Sed egestas mi elit, viverra aliquet libero interdum a. Quisque ut nunc vel tellus pellentesque lacinia ac ac lectus. Cras lacinia est a orci iaculis, eu cursus mi imperdiet. Proin sit amet aliquam felis. Praesent finibus dui id efficitur tempor. Praesent vitae nibh ac urna interdum pulvinar. Phasellus eu congue nisl. Donec quis massa ut nunc vulputate iaculis at vitae mi. Integer laoreet arcu diam, blandit porttitor dolor facilisis vel.

# Bio
Lorem ipsum dolor sit amet, consectetur [adipiscing][2] elit. Sed ut ex quis ligula ornare finibus quis eget magna. Quisque vitae nisi vitae ante vehicula porta. Nulla posuere justo sit amet tristique auctor. Mauris laoreet sapien sodales ullamcorper mollis. Curabitur vel pulvinar nunc. Proin porttitor purus vitae urna hendrerit vestibulum. Sed in magna at eros placerat vestibulum ut quis eros. Nam nec erat libero. Aenean pretium eleifend turpis non ultrices. Etiam eu ligula eget urna aliquam cursus. Proin vitae tortor sit amet ligula semper porta sit amet id tellus. Donec nec lectus ut ante rhoncus scelerisque quis a lectus.

Sed egestas mi elit, viverra aliquet libero interdum a. Quisque ut nunc vel tellus pellentesque lacinia ac ac lectus. Cras lacinia est a orci iaculis, eu cursus mi imperdiet. Proin sit amet aliquam felis. Praesent finibus dui id efficitur tempor. Praesent vitae nibh ac urna interdum pulvinar. Phasellus eu congue nisl. Donec quis massa ut nunc vulputate iaculis at vitae mi. Integer laoreet arcu diam, blandit porttitor dolor facilisis vel.
{.spoiler}

![percy.jpg](/percy.jpg)
This image is on the left!
{.img .left}

Etiam quis fringilla ex. Praesent volutpat mattis aliquet. Vivamus sit amet enim sed diam fermentum maximus eget vitae mi. Nunc dictum, felis id cursus sagittis, felis tortor aliquam nisl, quis tristique mi augue sagittis massa. Pellentesque sed quam lorem. Vestibulum at dignissim lorem, ac lacinia neque. Fusce ullamcorper ultricies purus non sodales. Duis turpis metus, pretium mollis lacinia laoreet, tincidunt finibus erat. Etiam rutrum gravida leo vitae gravida. Pellentesque lobortis vulputate tellus, vel euismod ex rhoncus sed.
{.something}

## An a subsection
Lorem ipsum dolor sit amet, consectetur [adipiscing][2] elit. Sed ut ex quis ligula ornare finibus quis eget magna. Quisque vitae nisi vitae ante vehicula porta. Nulla posuere justo sit amet tristique auctor. Mauris laoreet sapien sodales ullamcorper mollis. Curabitur vel pulvinar nunc. Proin porttitor purus vitae urna hendrerit vestibulum. Sed in magna at eros placerat vestibulum ut quis eros. Nam nec erat libero. Aenean pretium eleifend turpis non ultrices. Etiam eu ligula eget urna aliquam cursus. Proin vitae tortor sit amet ligula semper porta sit amet id tellus. Donec nec lectus ut ante rhoncus scelerisque quis a lectus.

[1]: /saladinsazafir.png
[2]: /
