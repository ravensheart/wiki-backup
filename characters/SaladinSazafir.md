---
title: Saladin Sazafir
description: The Editor in Chief of the Veritable Trust Chronicle
published: true
date: 2021-07-27T21:02:24.011Z
tags: lore, character, canon
editor: markdown
dateCreated: 2021-06-09T22:24:30.890Z
---


> **Callixtus** - *You've filled all of us with your lies...*  
> **Sazafir** -  *Lies! They are not lies my boy, they are perspectives!! Think of all the hearts you would break if you told the people of this world that their lives sat in the belly of a monster... they're not ready for that, I am their father, holding their hand. And if you attack my children.... a river of ink shall wash you from history. Not one machine you've constructed will be remembered as more than a mountain in one generation.*  
> **Callixtus** - *They deserve better.*  
> **Sazafir** - *I shall be the judge of that.*

![][1]Sazafir in his signature Veritean Ceremonial Robes - Circa 1030 AoM.
{.img .right}

# Bio
Saladin Sazafir is the trueborn name of an [Anathema][2] who is often associated with the long-running unary Arch Vizier caste of the Veritiean Empire. Auspiciously enigmatic and taking a leading role in the instigation of SkyStream Corporations participation in the [Dark Draconic War][3]. He is also the sole-proprietor of a garden-wide newspaper whose readership encompasses most of the Garden's sentient inhabitants under the pseudonymn Dix Dartan. While seldom few creatures know of his true existence, Civilized organizations of Magical Authority like the Wizencourt or the Forbidden Master's of the First City of Men largely consider his existence to be a mythological concept similar to the influence of the Aspects of Iwo on the Destinies Gambit.

[1]: /saladinsazafir.png
[2]: /Anathema
[3]: /Dark_Draconic_War
