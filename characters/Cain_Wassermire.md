---
title: Cain Wassermire the Greater
description: Eldest Prince-Duke of Wassermire
published: true
date: 2021-06-11T01:32:00.626Z
tags: character, canon
editor: markdown
dateCreated: 2021-06-10T22:16:00.359Z
---

Cain Wassermire the Greater is the Eldest Prince-Duke of the Duchy of Wassermire in the First City of Men. Born the eldest of three triplets. Cain is known to be the rashest and most firebrand of his three brothers with whom he shares the title of Duke. 

![this doesn't show up unless the image is not there.][1]
Cain Wassermire circa. 990 AoM
{.img .right}

# History

Ascending to his position in an auxiliary capacity in 969 AoM. Cain was crowned as the [Prince-Elder](/society/Prince-Elder) at the young age of 9 during a time of stability in the First City. Only years later he would wrest control of the SkyStream Trade Corporation from his brother Callixtus the Lesser months after the corporation instigated the Dark Draconic War circa. 1001 AoM.   
  
By virtue of the Arasian dispensation granted to the three brothers during the coronation of the three princes as “joint-Duke's", Cain would publicly conspire to see to his second eldest brother's assassination at the hand of Skystream    
Scholars of the First city would come to call the reign of the Brother's Wassermire as a time of thrift and strife. As he would lead the forces of Wassermire into conflict against the Dragon Concordat and break the non agression pact between Vilansce-Wassermire-and Kellarn known as the Triple Concordat. 

## Assassination of Corwyn the Middle

Cain would conspire with agents within the [SkyStream Corporation](/hidden-lore) as well as Priests of [Coin](/gods/Coin) in (~980-990 AoM) in a plot to see that his middle siblings permanent removal from the line of succession. Scholars debate as to weather this measure was taken to ensure that Cain could not be deposed or challenged by his middle sibling whose connections within the Church of Coin gave Cain reason to believe his Eldest position within the Ducal hierarchy might be challenged.  


[1]: /cain.png
[2]: /
