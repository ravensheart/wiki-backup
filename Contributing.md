---
title: Contributing
description: Guidelines for wiki contributions
published: true
date: 2021-06-23T18:11:56.741Z
tags: admin
editor: markdown
dateCreated: 2021-06-11T02:34:36.561Z
---

# Page Guidelines
General guidance to follow when creating or editing content on the wiki.

## markdown
Please, prefer the [markdown] editor option when creating pages. It is a lot easier to create a consistant look and feel, and you can even add CSS classes in the document. Check out the generic page [template] source code and look at how the images are marked up.

## Templates
Please use any relevant template for the page you are creating or editing. You can find a list of available [templates] here. This helps keep a consistant look and feel across all pages. If you find yourself writing a page and you are not using a template, ask yourself:

> **Q**: *Is there already a template for this kind of page?*
> **A**: Use that template.

> **Q**: *Is there an existing template I can adapt?*
> **A**: Do it! make a new template, just please *document* it.

> **Q**: *How do I make a template?*
> **A**: Ask Gynn he will help you out. You can also check out a good markdown [tutorial][markdown].

## Tags
Tag it up! but please a few simple rules
- **One** word tags, please. *maybe* 2.
- No symbols please (*\*-+#^&...*)
- There are a few reserved tags that are used for system admin. Go ahead and use them, but please follow the guidance.

Tag          | Descrition
:----------  | :----------
`character`  | adds page to *Characters*{.v-icon .mdi .mdi-account}
`place`      | adds page to *Places*{.v-icon .mdi .mdi-compass}
`literature` | adds page to *Literature*{.v-icon .mdi .mdi-script-text}
`org`        | adds page to *Organizations*{.v-icon .mdi .mdi-bank}
`glossary`   | adds page to *Glossary*{.v-icon .mdi .mdi-book-open}
`legends`    | adds page to *Legends*{.v-icon .mdi .mdi-book}
`canon`      | only used for official lore
`admin`      | **NOT FOR LORE** only used to tag administrative pages.
`hidden`     | restricts access to the page to *True Dragon* and up
`template`   | **NOT FOR LORE** only used for [page templates][templates]

- `legends` tag is only used for content brought over from the old wiki platform. The goal is to migrate or delete all articles with the `legends` tag. So...
  - do not tag new content with `legends`
  - If migration of a `legends` page is complete, remove the tag.

## Assets
Assests is refering mainly to pictures, but it can also mean videos, or fonts. While assets can be great to bring a little life to a page. Please remember we are storing all of them on a platform that we pay for so, maybe leave out the photo's of [Cain]'s fishing trip last fall.

Also, please put them in the `/assets/images` folder for my own sanity.

*[CSS]: Cascading Style Sheet

[Cain]: /characters/Cain_Wassermire
[templates]: /t/template?sort=title
[markdown]: https://guides.github.com/features/mastering-markdown/
[template]: /template/general