---
title: Cairn
description: Ducal Capital of the House of Mormire
published: true
date: 2021-06-22T16:42:49.578Z
tags: place
editor: markdown
dateCreated: 2021-06-10T21:59:22.784Z
---

![](/cairn.png)
{.img .right .w-30}

Cairn is the Capital city of the Duchy of Mormire in the [First City of Men.](/places/FirstCity)

Home to the [Duke Boltar](/characters/Boltar) and his three sons, The city is the main port city of The First City of Men and a major trade hub for Eastern Nissea, which sees tens of thousands of commuting travelers from the outlying [Wanderlands](/places/Wanderlands). The city is also the most well known hub for the garden-wide slave trade. As the Duke Boltar is a known and prolific slave trader. 

## History
Duke Boltar bore his three children around 977 AoM  after securing a stranglehold grip upon the import of slaves from the overseas continent of [Mormiria](/places/Mormiria_continent), and the Nearby Mormirian Exclave tens of miles north of the First City. Mormiria's join to the First City as a Royal Household under the [First Alliance of Men](/FirstAllianceofMen) at the behest house Vilansce, who hoped to make thier exclusive slaving contracts a matter of national law by granting the Duchy to their most lucrative slave-lord. [Mormirian](/races/Human/Mormirian) men who worked the fields of the [Wanderlands](/places/Wanderlands) and the mid/inner Vilanscian agrarian plantations are known to be large and "docile". Perfect for field work, breeding, and battle.

Despite the Duchy's historical origins supporting the slave trade. Mormire's populace was among the most pious and religiously active in the First City around the time of 990 AoM. [Vilanscian Scholars](/organizations/TheFarmersofVilansce) Would speculate that the origin for this behaviour lie in the natural Mormirian predeliction for ancient occult practices native to thier homeland and the worship of [The Old Gods](/gods/OldGods).Because of this tendancy the Duchy was able to create and maintain close religious relationships with the nearby Duchy of [Valenthyne](/places/Valenthyne_duchy). Eventually culminating in the friendship of Essiol of Valenthyne and Nathaneil Mormire in the Winter War.