---
title: On the First City of Men
description: Setting Overview
published: true
date: 2021-07-14T05:57:46.788Z
tags: lore, literature, meta
editor: markdown
dateCreated: 2021-07-14T05:57:46.788Z
---

## The First City of Men


There is an Ancient city, deep in the Nissean Stillwater Valley. Forged by the dying breath of a part of dragon lovers. Overrun by the men of the valley. The Two lover dragons gathered their children in their arms and cast a final spell to turn themselves, their murderers and the 70 miles around them into impenetrable essential stone. The Men of the Valley claimed the dragons roost as a home for themselves and as the eons passed. The Men of the Valley - Valenthynians they were called, claimed to rightfully rule an empire that they forged on the graves of the dragon's ancestors. 

First came the plainsmen, Vilanscians they were called - Men of the fields and the horse, Obsessed with their legacy they crafted the most beautiful townships out of the pristine essential stone. Men of stature and poise they were. They would come to rule over provinces far more powerful than they,  simple by the guile and craft of their gifted political leaders.

Then came the Wassermirians. Laborers from the far north who smelled of the timber they broke their backs to gather. They brought their slaves, their money and their technology. They were the Vilanscians. and Their Ducal household would reign until the Winter Wars at the end of the first Millenia of Man. 

Then for those of the cloth. House Palanine they were called, Heartland and Home to all the wizarding and worshipping members of clergy, The Heights of the Nine would become a bastion of ancient spiritual leadership in the Empire of the Stillwater Valley until it took its true name as the Arasian Empire of Men.

Next for the Duchy of Kellarn. Legions of well trained men with thick armor and shields whose ranks would swell those of the Arasian Empire. From a far south land of tradition and war they came.

Then came the Sectumites, Strange and weird denizens of the Balefire North where the Void tears at the edges of the world causing even the Ice of the swelling artics to burn green and bright. They would come at a time of need. When the dubious magicks of a needy city would tear a hole in creation. The wisest of the Forbidden masters would see that the spirits of the Sectumites would keep the evil at bay, In turn - they would be allowed a Duchy of their own. 

After these came the Barabians, Nomads of the wilds, barely fitting the description of men by all accounts of the pious members of the Empire, Their land would be purchased by Wassermire, A Blood debt to never be repaid, and three families of competing gangsters would rule the city from the great tents of the Pillarhouse. Slumlords for the vices and wayward appetites of the city forever.

Finally there were the Mormirians, Themselves made up of slavers and exiles in a far away land. Towering bearded men and women, who adorned themselves with heretical tattoos of their ancient land. A dispensation the Aras Vilansce II gave freely to them, so long as their backs broke in their fields… So their Duchy was made.

And in secret - In the Duchy of Pan, Lived the Fire-blooded descendants of the dragon whose stone visage lie frozen in the heart of the city. To most, A trivial artistic masterpiece that sat in the Aras’s Palace.The truth of this sculpture however, was only known to those of this duchy, That on the Thousandth year of the City’s existence.. The Dragon would rise again in fire.  The year is 990 in the First age of Man as the season of metal comes, And The Fire... is Rising.

