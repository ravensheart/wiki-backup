---
title: A Sword that Fell iin the West
description: An Ancient Legend of the Coming of Utumno
published: true
date: 2021-06-22T17:11:44.294Z
tags: literature
editor: markdown
dateCreated: 2021-06-22T17:04:57.997Z
---

> *There are some gods who revel at the fall of night,*  
> *...some gods who only stir, when all else has ceased to stir.*

!![tome.png](/tome.png)
A Copy of "A Sword that Fell in the West" Text residing in Fairfax Vilansce
{.img .right}

*The Sword that Fell in the West* is a Primordial Text (circa. 50,000 bAoM) that records the coming of Utumno, Dark Elf and the Making of Giant. Its Full text is disputed and few copies outside those housed inside of the secret warhouses of the Arcane Panopticum and the Wizencourt exist. Some members of the Ephemeral Embers Caste can recite its full unadulterated content that describe in full detail the horrors wrought against the Garden of Verund in those ancient days. Most other versions are subject to some [Interpretation](/powers/Necrolinguistics) or other.

# Text
*-Full Text Transcribed by Lord Ession of Valenthyne 901 AoM -*

*In the Time of the falling sword. A shadow was cast over all of the peoples of the garden, It was a dark time remembered in the minds of each civilization, remembered by all and feared by all.*

*Over the course of a year, A great monstrosity was looming over the garden all the great wizards and warriors of the world were held fast by its looming presence. All the great priests and holy people of the world prayed together to every god in the heavens. Each of the great civilizations worshipped different gods and yet they knew that they would perish if they did not band together.  The Noble Gods fled the land with their people, Leaving their cities to be ruined and Fled into the Bitter east of mormiria never to be heard of again. The Civilized Gods and the Courts of Heaven took to the ground and the seas. Fleeing eastward on the heels of the men of noble gods and found sanctuary on a island in the southeast. Men of every character and size were terrified by the silence of their gods, and even more by the mass immigrations of large civilizations. The great men of faith wrote in those days that the stars and sun were blotted out and there were naught but the blackness of the night to cloak the earth.*


*...but there are some gods who revel at the fall of night,* 
*...some gods who only stir, when all else has ceased to stir.*

*Enter Ethod the faceless who was a slave and servant of his dark master whose voice could still be heard even amongst the terrors.* *Niktoul’s voice had not been heard by his followers in eons. So when he spoke, the faithful wept with joy and leapt into action.* 

*All the loyal servants of the “low” constellations had to be called. The greatest warriors of the garden were needed, and quickly. They must be found, and they must pledge themselves to the dark pantheon if there was to be any hope. And it was up to Ethod the Watcher to see that this was done.* 

*In the time of Ethod the great warriors of the day were thus:*

Beautiful Elita Everbind Headmistress of the Nightblades of Gnostitia - In her native land she was a loyal servant of Lilith, and thusly she would represent the great godess in her hour of need.

Great Qo’ab Loremaster and Arch-Protector of the Tentacular - A creature whose lust for knowledge and its preservation were matched only by Ohgmar himself. The god noticed him and called for his service

[Mighty Zaxus](/characters/Zaxus) - The Primodial Hammer, Slayer of the Great Demon Drake of Reyawinn and Justice of the Gods, His deeds in battle were unsurpassed amongst the annals of men, a personal servant of the god in countless battles in lands long away and long forgotten, Brutoth would call him to his service once more.


Mother Vassfra Mawnight - The Womb of the West, Mother of Wildlings and Westmen, A great spirit of a westerly forest bourne here by the great tentacular seed. She would be the bearer of the seeds of a great civilization, her likeness was like that of a great monsterous form, fitting for Aranea, who called her to her side.

[Ethod of Niktoul](/characters/Ethod) - The Sleepless Watcher himself would need to lead the company west where the great sword would land and stay the forces that would threaten the world. 

Yet even with Ethod’s covenant forged after several tides of the seas, There were still not enough to brave the forces that would come. Others had to be found….

But others could not be found...

So Ethod prayed for 40 nights in the darkness waiting for his god’s reply.

*After 2 thirtnights, The silence came as a stark reminder, that Niktoul might have very well gone silent. Ethods work had been completed, and now his task fullfilled he would simply be alone waiting for the enemy to come, and through the expenditure of thier bodies and those of his fellows they might stymie the terrors to come….*

… The god remained silent….

*As the sword struck the earth. The Warriors rose from thier post near the edge of the world. The worlds most able warriors and servants of darkness, would serve the light and protect the great kingdoms behind them that viewed them as terrors.* 

*As night fell, they saw a glistening in the sky from the west that heralded the coming of great terrors, and as the terrors crept over the horizon the champions of the dark gods saw not a terror but a wind of scales. A Company of scaled warriors glistening in colored jade came forth seemingly from the sky like a shooting star. They were from the celestial gods of dragons who were an answer to the prayers of the great dragons of the garden who even now were seeing the people of the garden to safety in the east.* 

*In that day they were:

Rathgar the Black
Dry’theer the Green
Obsys the White
Ksutil the Purple
Xarmund the Blue
Pashtu the Yellow

*Then, When all the races of men had been ushered to the East away from the scouring taint of wyld magic, came the untold terrors of a long night.*





[1]: /saladinsazafir.png
[2]: /
