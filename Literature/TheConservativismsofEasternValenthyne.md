---
title: Conservativisms of East Valenthyne
description: Novella Written by the Princess Arien Valenthyne circa. 982 AoM
published: true
date: 2021-07-15T03:43:55.311Z
tags: literature
editor: markdown
dateCreated: 2021-06-25T21:32:25.386Z
---

>  *The Ancients Roots of Valenthyne's Origins lie not within the rivers of Card's Peaks or the Hollow Hieghts, but in the Far North of the First City, Beyond the pale of the Wanderlands, into the Essian Foothills north of the Riddlewoods*  


![this doesn't show up unless the image is not there.][1]
Eastern Valenthyne beyond the outer wall of the First City!
{.img .right}

A Novella Written by the 1st Princess Arien Valenthyne circa. 982 AoM documenting the various sects of anti-Arasian sentiments and largely considered to be heresy amongst the established clergy of west Valenthyne.  

# Content
*...The People of East Valenthyne have been in our Duchy the most troublesome to contain by virtue of the obdurance of legendary stature. The people themselves root themselves in the ancient traditions of an almost asuredly long since dead line of Emperors that the Paladins of Valenthyne's inner court can only scratch upon. While the Western provinces of Valenthyne can rightly lay claim to all of the legal and even ritualistic firmament of the Religion of the Voice of Kul in the form of the Ancestors Garden within Harmony. A look at the histories of the house valenthyne written by a ancestor of the Nexian Scholar known as Cruor, indicate that records from the Wizard War of 400 AoM saw the garden relocated to Harmony within the second ring of the First City of Men at the hands of the Palanine after thier victory...*

*-Excerpt from the Original Text Housed in Shurendo-*


[1]: /eastvalen.png
[2]: /
