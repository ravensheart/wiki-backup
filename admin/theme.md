---
title: Wiki CSS Theme
description: 
published: true
date: 2021-06-15T13:25:25.605Z
tags: hidden, admin
editor: markdown
dateCreated: 2021-06-10T14:28:43.139Z
---

```css
@font-face {
  font-family: Decadentia;
  src: url(/assets/font/decadentia.ttf)
}
@font-face {
  font-family: "Aligot De Mirabelle";
  src: url(/assets/font/aligot_de_mirabelle.ttf)
}
@font-face {
  font-family: "Francisco Lucas Briosa";
  src: url(/assets/font/flbrsa1.ttf)
}
@font-face {
  font-family: Duality;
  src: url(/assets/font/the_duality.ttf)
}
.font-decadentia {
  font-family: Decadentia!important;
  font-weight: 100!important
}
.font-aligot {
  font-family: "Aligot De Mirabelle"!important
}
.font-briosa {
  font-family: "Francisco Lucas Briosa"!important
}
.font-duality {
  font-family: Duality!important
}
.v-toolbar__title {
  overflow: visible
}
.v-toolbar__title .subheading {
  position: relative;
  top: 8px;
  font-family: Duality;
  font-size: 2.5rem
}
.v-application {
  background: rgba(0,0,0,.7)!important
}
.v-application .headline {
  font-family: "Francisco Lucas Briosa"!important
}
.v-main__wrap {
  position: relative;
  z-index: 0
}
.v-main__wrap::after {
  content: "";
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: url(/delketh.jpg);
  background-size: cover;
  background-position: center
}
.v-main__wrap .theme--dark.v-divider {
  border-color: #000
}
.v-main__wrap .container.grid-list-xl {
  background: linear-gradient(-90deg,#000,rgba(0,0,0,.8));
  height: 100%
}
.contents {
  margin-bottom: 3rem;
  overflow: auto
}
.contents .img {
  position: relative;
  z-index: 1;
  width: 20rem;
  max-width: 50%;
  margin-bottom: 1.5rem!important;
  font-style: italic;
  font-size: .85rem;
  text-align: center
}
.contents .img.right {
  float: right;
  margin-left: 1.5rem!important
}
.contents .img.left {
  float: left;
  margin-right: 1.5rem!important
}
.contents .center {
  margin: auto!important
}
.contents .w-50 {
  width: 50rem
}
.contents .w-30 {
  width: 30rem
}
.contents .spoiler {
  filter: blur(5px);
  transition: filter 0.75s ease;
  transition-delay: 0.5s;
}
.contents .img.spoiler img {
  filter: blur(16px);
  transition: filter 0.75s ease;
  transition-delay: 0.25s;
}
.contents .spoiler:hover {
  filter: initial;
}
.contents .img.spoiler img:hover {
  filter: initial;
}
.theme--dark .v-main .contents h1,
.theme--dark .v-main .contents h2,
.theme--dark .v-main .contents h3,
.theme--dark .v-main .contents h4,
.theme--dark .v-main .contents h5,
.theme--dark .v-main .contents h6 {
  clear: left
}
.theme--dark .v-main .contents h1::after {
  background: linear-gradient(90deg,#64b5f6,#2196f3 10%,rgba(13,71,161,0) 40%)
}
.theme--dark .v-main .contents h2::after {
  background: linear-gradient(90deg,#e0e0e0,rgba(97,97,97,0) 30%)
}
.v-main .contents h3:after,
.v-main .contents h4:after,
.v-main .contents h5:after,
.v-main .contents h6:after {
  background: linear-gradient(90deg,#9e9e9e,hsla(0,0%,62%,0) 20%)
}
.contents .text-lg {
  font-size: 2rem
}
.v-footer {
  display: none!important
}
.contents .no-clear {
  clear: none!important
}
@media only screen and (max-width:600px) {
  .contents .img.left,
  .contents .img.right {
    float: none;
    margin: 0 auto 0 auto!important
  }
  .contents .img {
    width: 95%;
    max-width: 95%
  }
}
```